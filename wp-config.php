<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*V`d|dO&M(n6gQOIr16WK^1@ND{wFt3G!??z}ge&f&+IVH@G{MKc.~1fpPR3@j~P');
define('SECURE_AUTH_KEY',  'GCzRm~T,Q~qsw<fPO0|4vjBNcY-,$JOiSf%-|HAwXqSY,T~y:n+i:m*vQRf/QNw@');
define('LOGGED_IN_KEY',    '*.{!P`F-2$eA+@vpq?[!%M->yjQ@ycjU;~4^G|1:9G}41}5|eMBZ3;  O2KQySs3');
define('NONCE_KEY',        '|cr!$ITMy!C_O!4t~.2 KvEav%o*gYo-,0>4yT*]#o%zu{&HM&D10~~M~NaKk0CF');
define('AUTH_SALT',        '&z=%O2)8T`4z3Z&Pg*YPLw%dYvem`;c$;IJgyc4:rI~xn-LO@]O=dL#t>9MUk;o-');
define('SECURE_AUTH_SALT', 'oD?RO9H9PcXmBICS#f^omB*. Kqc1#+*M%7|8Q1Zp)f-_NNZN%S[FZ1`Mv~vrN-Y');
define('LOGGED_IN_SALT',   ';kUzxDm+8DvA|TOf;(g&^Wdbt-Z{NDpQY#f-jzNhJL-p^2H?%y-@VaA0e]A9R_FL');
define('NONCE_SALT',       'c{~G3RnN|MCV%i=T_+SbWV3OUYB~RNa_7#50+n}j[<(;l@tE/a)l<|9f+9v~zd^n');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define( 'WP_DEBUG_LOG', true );
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
