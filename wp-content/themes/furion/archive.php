<?php
/**
 * The main template file.
 *
 * @package Furion
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get theme options
global $smof_data;

$classes = array();

// Get blog layout
$blog_layout = $smof_data['blog-layout'];

// Get blog style
$blog_style = $smof_data['blog-style'];

if ( 'right_sidebar' == $blog_layout ) {
	$classes[] = 'right-sidebar';
} elseif ( 'left_sidebar' == $blog_layout ) {
	$classes[] = 'left-sidebar';
} else {
	$classes[] = 'no-sidebar';
}
if ( $blog_style ) {
	$classes[] = 'b-' . $blog_style;
}
// Blog masonry full width
$fullwidth = ( isset ( $smof_data['blog-masonry-full-width'] ) && $smof_data['blog-masonry-full-width'] ) ? ' fullwidth' : '';

get_header(); ?>

	<div class="k2t-content <?php echo esc_attr( implode( ' ', $classes ) ) . $fullwidth ?>">

		<div class="k2t-wrap">

			<main class="k2t-blog" role="main">
				
				<?php
					if ( 'masonry' == $blog_style ) {
						echo '<div class="masonry-layout ' . esc_attr( $smof_data['blog-masonry-column'] ) . ' ">';
						echo 	'<div class="grid-sizer"></div>';
					}
					if ( 'grid' == $blog_style ) {
						echo '<div class="grid-layout clearfix ' . esc_attr( $smof_data['blog-grid-column'] ) . ' ">';
					}
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							if ( 'large' == $blog_style ) {
								include K2T_TEMPLATE_PATH . 'blog/content-large.php';
							} elseif ( 'medium' == $blog_style ) {
								include K2T_TEMPLATE_PATH . 'blog/content-medium.php';
							} elseif ( $blog_style == 'grid'  || $blog_style == 'masonry' ) {
								include K2T_TEMPLATE_PATH . 'blog/content-grid.php';
							} 
						endwhile;
					else :
						get_template_part( 'content', 'none' );
					endif;

					if ( 'masonry' == $blog_style || 'grid' == $blog_style ) {
						echo '</div>';
					}

					include_once K2T_TEMPLATE_PATH . 'navigation.php';

				?>

			</main><!-- .k2t-main -->

			<?php
				if ( 'no_sidebar' != $blog_layout ) {
					get_sidebar();
				}
			?>

		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-content -->

<?php get_footer(); ?>
