<?php
/**
 * The template for displaying single post content.
 *
 * @package Furion
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get theme options
global $smof_data;
// GET LAYOUT;
$layout = ( function_exists( 'get_field' ) ) ? get_field( 'post_layout', get_the_ID(), true ) : '';

if ( $layout == 'default' )
	$layout = $smof_data['single-layout'];

// Get post format
$post_format = get_post_format();
$link        = ( function_exists( 'get_field' ) ) ? get_field( 'link_format_url', get_the_ID() ) : '';

// Display categories
$display_categories = ( function_exists( 'get_field' ) ) ? get_field( 'display_categories', get_the_ID() ) : '';

// Display posted author
$display_posted_author = ( function_exists( 'get_field' ) ) ? get_field( 'display_posted_author', get_the_ID() ) : '';

// Display post time
$display_post_date = ( function_exists( 'get_field' ) ) ? get_field( 'display_post_date', get_the_ID() ) : '';

// Display tags
$display_tags = ( function_exists( 'get_field' ) ) ? get_field( 'display_tags', get_the_ID() ) : '';

// Display author post bio
$display_authorbox = ( function_exists( 'get_field' ) ) ? get_field( 'display_authorbox', get_the_ID() ) : '';

// Display related post
$display_related_post = ( function_exists( 'get_field' ) ) ? get_field( 'display_related_post', get_the_ID() ) : '';

//get list of category follow codex 

$cats = array();
foreach(wp_get_post_categories( get_the_ID() ) as $c)
{
	$cat = get_category($c);
	array_push($cats,$cat->name);
}

if(sizeOf($cats)>0)
{
	$post_categories = implode(', ',$cats);
}

?>

<div id="main-col" <?php post_class(); ?>>

	<section class="entry-box">
		<h2><a href="#"> <?php echo get_the_title();?> </a></h2>
		<div class="top-meta">
			<?php if ( $display_posted_author != 'hide' ) : ?>
				<div class="author-meta">
				<?php
					echo '<span>' . esc_html__( 'POSTED BY', 'furion' ) . ' &nbsp;</span>';
					echo '<a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '"> ' . get_the_author() . '</a>';
				?>
				</div>
			<?php endif;?>
			<?php if ( $display_post_date != 'hide' ) : ?>
				<div class="date-meta">
					<?php echo '<span>' . get_the_date() . '</span>';?>
				</div>
			<?php endif;?>

			<?php if ( $display_categories != 'hide' ) : ?>
				<div class="categories-list">
					<?php $categories_list = get_the_category_list( esc_html__( ', ', 'furion' ) );
						if ( $categories_list ) :
							printf( wp_kses(__( '%1$s', 'furion' ), array( 
																		'a' => array() ,
																	) 
									),
									$categories_list ) ;
						endif;
					?>
				</div>
			<?php endif;?>
		</div>

		<?php include get_template_directory() . '/templates/blog/post-format.php'; ?>

		<div class="post-entry clearfix">
			<?php the_content(); ?>
			<div class="clearfix">
			<?php if ( $smof_data['single-comments'] ) : ?>
				<span class="entry-comment">
					<a href="<?php comments_link(); ?>"><span aria-hidden="true" class="icon_comment_alt"></span><?php comments_number( '0', '1', '%' ); ?></a>
				</span>
			<?php endif; ?>
		</div>
		</div><!-- .post-entry -->

		<div class='bottom-meta'>
			<div class="tag-meta">
				<?php if ( $smof_data['single-tags'] )
					$tags = get_the_tags();
					if( $tags ) {
						echo '<div class="widget_tag_cloud"><div class="tagcloud"><span>' . esc_html__('Tags:', 'k2t') . ' &nbsp;</span>';
						foreach ( $tags as $key => $tag) {
							echo '<a href="'. esc_url( get_tag_link( $tag->term_id ) ) .'" title="'. esc_attr( $tag->name ) .'">'. esc_attr( $tag->name ) .'</a>';
						}
						echo '</div></div>';
					}	
				?>
			</div>
			<div class="share-meta">
				<?php 
					if (function_exists( 'k2t_furion_social_share' ) ) : 
						echo '<span class="share-title">' . esc_html__( 'Share : ', 'furion' ) . '</span>';
						k2t_furion_social_share();
					endif;
				?>
			</div>
		</div>

	</section><!--end:entry-box-->

	<?php if ( $display_authorbox != 'hide' || 'default' == $display_authorbox && $smof_data['single-authorbox'] ) : ?>
		<article class="about-author clearfix">      
			<?php echo get_avatar( get_the_author_meta( 'user_email' ), '202' , false );
			?> 
			<div class="author-info">
				<h4><?php echo get_the_author_link();?></h4>
				<p class="description"><?php echo get_the_author_meta( 'description' );?></p>
			</div>
		</article><!--about-author-->
	<?php endif;?>

	<?php if( $smof_data['single-related-post'] ) : ?>
		<?php 
			$num_related = ( $layout ==  'no_sidebar' ) ? '3' : '2';
			wp_localize_script( 'k2t-main-script', 'related_num_slider', $num_related );
		?>
		<div class="related-post">
			<h3><?php echo esc_html( $smof_data['single-related-post-title'] );?></h3>
			<div class="related-post-wrap">
				<?php
					$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => $smof_data['single-related-post-number'], 'post__not_in' => array($post->ID) ) );
					if( $related ) foreach( $related as $post ) {
					setup_postdata($post); ?>
			            <div class="post">
			            	<a rel="external" href="<?php echo the_permalink()?>">
			            		<?php 
			            		if ( has_post_thumbnail() ) :
									echo get_the_post_thumbnail( get_the_ID(), 'k2t_furion_370x192' );
								else :
									echo '<img src="' . get_template_directory_uri() . '/assets/img/placeholder/k2t_furion_370x192.jpg" alt="' . get_the_title() . '" />';
								endif;
			            		?>
			            	</a>
			        		<h5><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			        		<div class="top-meta">
								<?php if ( $display_posted_author != 'hide' ) : ?>
									<div class="author-meta">
									<?php
										echo '<a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '"> ' . get_the_author() . '</a>';
									?>
									</div>
								<?php endif;?>
								<?php if ( $display_post_date != 'hide' ) : ?>
									<div class="date-meta">
										<?php echo '<span>' . get_the_date() . '</span>';?>
									</div>
								<?php endif;?>

								<?php if ( $display_categories != 'hide' ) : ?>
									<?php $categories_list = get_the_category_list( esc_html__( ', ', 'furion' ) );
										if ( $categories_list ) :
											printf( esc_html__( '%1$s', 'furion' ), $categories_list ) ;
										endif;
									?>
								<?php endif;?>
							</div>
			 			</div>
					<?php }
				wp_reset_postdata(); ?>
			</div>
		</div>
	<?php endif; ?>

</div>