<?php

/**
 * Required action filters
 *
 * @uses add_action()
 *
 * @since 1.0.0
 */

/**
 * AJAX Saving Options
 *
 * @since 1.0.0
 */
add_action( 'wp_ajax_of_ajax_post_action', 'of_ajax_callback' );

/*
	Remove script version
*/
function k2t_furion_remove_version( $src ) {
    if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'k2t_furion_remove_version', 9999 );
add_filter( 'script_loader_src', 'k2t_furion_remove_version', 9999 );

// Hooks on Back-end
if( is_admin() ){

	//Enqueue Script and Css in Backend
	if ( ! function_exists ( 'k2t_furion_backend_scripts' ) ){
		function k2t_furion_backend_scripts() {
			wp_enqueue_style( 'k-backend-style', K2T_THEME_URL . 'assets/css/k2t-backend.css' );
		}
		add_action( 'admin_enqueue_scripts', 'k2t_furion_backend_scripts' );
	}
	 
	/*--------------------------------------------------------------
	Var for Script Backup
	--------------------------------------------------------------*/
	if ( ! function_exists( 'k2t_furion_sample_import_add_admin_head' ) ) {
		function k2t_furion_sample_import_add_admin_head() {
			echo '<scr' . 'ipt>';
			echo 'var home_url = "' . esc_url( site_url() ) . '";';
			echo 'var installing_proccess  = 0;';
			echo 'var cache_installing_url = "' . K2T_FRAMEWORK_URL . 'inc/k2timporter/tmp_backup/cache_proccess";';
			echo '</scr' . 'ipt>';
		}
		add_action( 'admin_head', 'k2t_furion_sample_import_add_admin_head');
	}
}

// Enqueue css login admin
function k2t_furion_themeslug_enqueue_style() {
	wp_enqueue_style( 'admin-style', K2T_FRAMEWORK_URL . 'assets/css/admin-style.css' );
}
add_action( 'login_enqueue_scripts', 'k2t_furion_themeslug_enqueue_style', 10 );

// Move comment textarea to bottom

function wpb_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}
 
add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

// Hooks on Front-end
if( ! is_admin() ){
	
	/*--------------------------------------------------------------
	Enqueue front-end script
	--------------------------------------------------------------*/
	if ( ! function_exists( 'k2t_furion_front_end_enqueue_script' ) ) :
		function k2t_furion_front_end_enqueue_script() {
			global $smof_data;

			// Load zoom effect for title bar.
			if ( function_exists( 'get_field' ) && get_field( 'background_zoom', get_the_ID() ) ) {
				wp_enqueue_script( 'zoomeffects-script', K2T_THEME_URL . 'assets/js/vendor/zoom-effect.js', array(), '', true );
			}

			// Load jquery easing.
			wp_enqueue_script( 'jquery-easing-script', K2T_THEME_URL . 'assets/js/vendor/jquery-easing.js', array(), '', true );
			// Load infinite scroll library.
			wp_enqueue_script( 'infinitescroll-script', K2T_THEME_URL . 'assets/js/vendor/jquery.infinitescroll.min.js', array(), '', true );
			// Enqueue jquery isotope
			wp_enqueue_script( 'jquery-isotope', K2T_THEME_URL . 'assets/js/vendor/isotope.pkgd.min.js', array(), '', true );
			// Jquery Library: Imagesloaded
			wp_enqueue_script( 'jquery-imagesloaded', K2T_THEME_URL . 'assets/js/vendor/imagesloaded.pkgd.min.js', array( 'jquery' ), '3.1.6', true );
			// Enqueue jquery carousel
			wp_enqueue_script( 'owlcarousel', K2T_THEME_URL . 'assets/js/vendor/owl.carousel.min.js', array(), '', true);

			// Load our custom javascript.
			$mainParams = array();
			wp_enqueue_script( 'jquery-mousewheel', K2T_THEME_URL . 'assets/js/vendor/jquery.mousewheel.min.js', array( 'jquery' ), '', true );
			wp_enqueue_script( 'k2t-main-script', K2T_THEME_URL . '/assets/js/main.js', array( 'jquery' ), '', true );

			// enqueue script 

			if ( is_singular() && get_option( 'thread_comments' ) )
				wp_enqueue_script( 'comment-reply' );

			// set variable for javascript 

			$header_style    = ( function_exists( 'get_field' ) ) ? get_field( 'page_header_style', get_the_ID() ) : '';
			if ( !isset($header_style) )
				$header_style = $smof_data['header-style'];
			if ( isset( $smof_data['offcanvas-swipe'] ) && $smof_data['offcanvas-swipe'] ) {
				$mainParams['offcanvas_turnon'] = $smof_data['offcanvas-turnon'];
			}
			if ( isset( $smof_data['fixed-header'] ) ) {
				$mainParams['fixed_header'] = $smof_data[ $header_style . 'fixed-header'];
			}
			if ( isset( $smof_data[ $header_style . 'sticky-menu' ] ) ) {
				$mainParams['sticky_menu'] = $smof_data[ $header_style . 'sticky-menu'];
			}
			if ( isset( $smof_data[ $header_style . 'smart-sticky'] ) ) {
				$mainParams['smart_sticky'] = $smof_data[ $header_style . 'smart-sticky'];
			}
			if ( 'masonry' == $smof_data['blog-style'] ) {
				$mainParams['blog_style'] = $smof_data['blog-style'];
			}

			wp_localize_script( 'k2t-main-script', 'mainParams', $mainParams );
		}
		add_action( 'wp_enqueue_scripts', 'k2t_furion_front_end_enqueue_script' );
	endif;
	
	/*--------------------------------------------------------------
		Enqueue front-end style
	--------------------------------------------------------------*/
	if ( ! function_exists( 'k2t_furion_front_end_enqueue_style' ) ) :
		function k2t_furion_front_end_enqueue_style() {
			global $smof_data;
			
			wp_enqueue_style( 'owlcarousel', K2T_THEME_URL . 'assets/css/vendor/owl.carousel.css' );

			// Load elegant icon
			wp_enqueue_style( 'elegant-icon', K2T_THEME_URL . 'assets/css/vendor/elegant-icon.min.css' );	

			// Load font awesome for first active theme
			wp_enqueue_style( 'k2t-font-awesome-style', K2T_THEME_URL . 'assets/css/vendor/font-awesome.min.css' );	
	
			// Style for mega menu
			wp_enqueue_style( 'k2t-megamenu-style', K2T_THEME_URL . 'assets/css/megamenu.css' );
	
			// Load our main stylesheet.
			wp_enqueue_style( 'k2t-main-style', K2T_THEME_URL . 'assets/css/main.css' );
	
			// Load responsive stylesheet.
			wp_enqueue_style( 'k2t-reponsive-style', K2T_THEME_URL . 'assets/css/responsive.css' );

		}
		add_action( 'wp_enqueue_scripts', 'k2t_furion_front_end_enqueue_style' );
	endif;
		
	
	/**
	 * Enqueue stylesheet.
	 *
	 * @package Furion
	 * @author  LunarTheme
	 * @link    http://www.lunartheme.com
	 */
	
	/*--------------------------------------------------------------
		Custom CSS
	--------------------------------------------------------------*/
	if ( ! function_exists( 'k2t_furion_adjustBrightness' ) ) {
		function k2t_furion_adjustBrightness($hex, $steps) {
		    // Steps should be between -255 and 255. Negative = darker, positive = lighter
		    $steps = max(-255, min(255, $steps));

		    // Normalize into a six character long hex string
		    $hex = str_replace('#', '', $hex);
		    if (strlen($hex) == 3) {
		        $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
		    }

		    // Split into three parts: R, G and B
		    $color_parts = str_split($hex, 2);
		    $return = '#';

		    foreach ($color_parts as $color) {
		        $color   = hexdec($color); // Convert to decimal
		        $color   = max(0,min(255,$color + $steps)); // Adjust color
		        $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
		    }

		    return $return;
		}
	}
	

	if ( ! function_exists( 'k2t_furion_front_end_enqueue_inline_css' ) ) {
		function k2t_furion_front_end_enqueue_inline_css() {
			global $smof_data; 
			ob_start();
			?>
			<style>
				
				<?php
				/* Content width
				------------------------------------------------- */
				if ( ! empty( $smof_data['boxed-layout'] ) ) {
					echo '
						.boxed .k2t-container { max-width: ' . $smof_data['use-content-width'] . 'px; }
					';
				} else {
					if ( isset ( $smof_data['use-content-width'] ) ) {
						echo '
							.k2t-wrap, .container { max-width: ' . $smof_data['use-content-width'] . 'px; }
						';
					}
				}
	
				/* Sidebar width
				------------------------------------------------- */
				$sidebar_width = $smof_data['sidebar_width'];
				if ( ! empty( $page_sidebar_width ) ) {
					echo '
						.k2t-sidebar, .k2t-sidebar-sub { width:' . $page_sidebar_width . '%; }
						.k2t-blog, .k2t-main { width:' . ( 100 - $page_sidebar_width ) . '%; }
					';
				}
				/* set up layout container */
				$container = function_exists( 'get_field' )	 ? get_field( 'page_container_max_width', get_the_ID() ) : '';

				if ( ! empty( $container ) ) :
					echo '
					.k2t-title-bar,
					.k2t-body,
					.page div.no-sidebar .container,
					.page div.no-sidebar .k2t-wrap,
					.page div.no-sidebar div[data-vc-full-width-init="true"],
					.page div.no-sidebar div[data-vc-full-width-init="true"] .vc_inner,
					.page div.no-sidebar div[data-vc-stretch-content="true"] .vc_column_container,
					.page div.no-sidebar div[data-vc-stretch-content="true"] .vc_column_container .vc_inner {
						max-width:' . $container .  'px !important;
					}

					';
				endif;


				// PRIMARY COLOR 
				if ( $smof_data['theme-primary-color'] == 'custom' ) :?>

					a:hover, 
					a:focus,
					.k2t-header-mid .k2t-menu > li.current-menu-item > a, 
					.k2t-header-mid .k2t-menu > li.current-menu-parent > a, 
					.k2t-header-mid .k2t-menu > li.current-menu-ancestor > a,
					.k2t-heading.has-border.two_dots .h:before, .k2t-heading.has-border.two_dots .h:after,
					.k2t-iconbox.layout-1:hover .iconbox-icon,
					.k2t-btt,
					.widget-title:after,
					.k2t-info .widget_nav_menu ul li a:hover,
					.k2t-footer .k2t-wrap .k2t-row > div a:hover,
					.k2t-gallery-heading .filter-list li.active, .k2t-gallery-heading .filter-list li:hover,
					.k2t-iconbox.layout-4 .iconbox-icon i,
					.event-isotope-filter li.active,
					.widget_categories > ul > .cat-item .children li a:hover,
					.contact-info a:hover,
					.vc_toggle.vc_toggle_default .vc_toggle_title h4:hover,
					.k2t-project-heading h2:before,
					.k2t-project-heading h2:after,
					.k2t-page-topnav ul.menu > li.active > a, 
					.k2t-gallery-heading .gallery-title:after,
					.widget ul li.current-cat a,
					.entry-box .entry-comment a:hover,
					.k2t-header-mid .search-box:hover,
					.entry-box .post-entry a,
					.course-isotope-filter li.active,
					.k2t-blog .post-item.sticky .entry-content::before,
					.k2t-blog .post-item header .entry-author a:hover, 
					.k2t-blog .post-item header .entry-comment a:hover {
						color: <?php echo  esc_html( $smof_data['primary-color'] ) ;?>;
					}

					button:hover,
					input[type="button"]:hover,
					input[type="reset"]:hover,
					input[type="submit"]:hover ,
					.h-element .shop-cart .cart-control span,
					#close-canvas,
					.k2t-header-top,
					.tp-caption.primarybutton a,
					.tp-caption.primarybutton a:hover,
					.k2t-btt:hover,
					article[class*="course-"] .more-link,
					article[class*="course-"] .more-link:hover,
					.event-listing-masonry .masonry-item .read-more,
					.k2t-blog .post-item .more-link,
					.k2t-blog .post-item .more-link:hover,
					.owl-controls .owl-dots > div.active,
					.mc4wp-form input[type=submit],
					.mc4wp-form input[type=submit]:hover,
					.k2t-footer .widget.social-widget ul li a:hover,
					.owl-controls .owl-nav > div > i:hover,
					.k2t-member .team-socials-link li:hover,
					.k2t-gallery-heading .filter-list li:after,
					.wpcf7 #commentform input[type="submit"],
					.event-classic-item .more-link,
					.event-classic-item .more-link:hover,
					.k2t-pagination-lite,
					.k2t-pagination-lite a:hover,
					.k2t_widget_recent_event .join-event,
					.k2t_widget_recent_event .join-event:hover,
					.event-isotope-filter li:after,
					.single-post-k-event .event-link,
					.single-post-k-event .event-link:hover,
					.widget_categories > ul > .cat-item > a:before,
					.vc_tta.vc_tta-style-outline.vc_tta-tabs .vc_tta-tabs-list .vc_tta-tab.vc_active a,
					.error404, .error404 .k2t-body,
					.k2t-page-topnav ul.menu > li > a:before,
					.project-fields .project-link,
					.project-fields .project-link:hover,
					.teacher-listing article .social a:hover,
					.teacher-connect table th,
					.widget #wp-calendar caption,
					.widget #wp-calendar td#today,
					.k2t-blog .cat-icon,
					.k2t-navigation ul li span.current,
					.widget .tagcloud a:hover,
					.about-author,
					.form-submit #submit,
					.form-submit #submit:hover,
					.k2t-button a,
					.single-post-k-course .course-link,
					.single-post-k-course .course-link:hover,
					.k2t-searchbox .mark,
					body.search .k2t-main .searchform .form-group button,
					.loader-inner,
					.course-isotope-filter li:after,
					.cd-dropdown > span,
					.cd-dropdown ul li span,
					.k2t-related-course .related-thumb a i,
					blockquote::before, 
					q::before,
					.entry-box .widget_tag_cloud .tagcloud a:hover,
					.single-footer-nav,
					.woocommerce #respond input#submit,
					.woocommerce a.button,
					.woocommerce button.button,
					.woocommerce input.button, 
					.woocommerce-page #payment #place_order, 
					.shop-cart .shop-item .buttons .button,
					*::-moz-selection {
						background-color: <?php echo esc_html( $smof_data['primary-color'] );?>;
						border-color: <?php echo esc_html( $smof_data['primary-color'] );?>;
					}
					.k2t-header-mid .k2t-menu > li:hover a,
					.k2t-header-mid .k2t-menu > li > a:hover {
						border-bottom-color: <?php echo esc_html( $smof_data['primary-color'] );?>;
					}
					#commentform > p.focus input[type="text"],
					#commentform > p.focus input[type="email"],
					#commentform > p.focus textarea {
						border-bottom: 3px solid <?php echo esc_html( $smof_data['primary-color'] );?>;
					}
					.woocommerce #respond input#submit:hover, .woocommerce a.button:hover,
					.woocommerce button.button:hover, 
					.woocommerce input.button:hover, 
					.woocommerce-page #payment #place_order:hover, 
					.shop-cart .shop-item .buttons .button:hover,
					.k2t-pagination-lite a:hover {
						background-color: <?php echo adjustBrightness( $smof_data['primary-color'] , -20 );?>;
					}
					*::-moz-selection,
					::selection, 
					.k2t-header-mid .k2t-menu > li.current-menu-item > a, 
					.k2t-header-mid .k2t-menu > li.current-menu-parent > a, 
					.k2t-header-mid .k2t-menu > li.current-menu-ancestor > a,
					.tp-caption.primarybutton a,
					.tp-caption.primarybutton a:hover,
					.k2t-footer .widget.social-widget ul li a:hover,
					.owl-controls .owl-nav > div > i:hover,
					#commentform > p.focus input[type="text"],
					#commentform > p.focus input[type="email"],
					#commentform > p.focus textarea,
					.widget .tagcloud a:hover,
					.loader,
					.comment-list .comment-body footer .action-link > a:hover {
						border-color: <?php echo esc_html( $smof_data['primary-color'] );?>;
					}

					.k2t-gallery-shortcode .view .mask {
						background: rgba(33, 150, 243, 0.95);
					}
					.contact-info .vc_icon_element.vc_icon_element-outer .vc_icon_element-inner:hover .vc_icon_element-icon:before,
					.woocommerce .b-action a.button.added:before, 
					.b-action .yith-wcwl-wishlistexistsbrowse a:before, 
					.b-action .yith-wcwl-wishlistaddedbrowse a:before,
					.widget_product_categories > ul > .cat-item .children li a:hover,
					.woocommerce ul.products li.product h3 a:hover,
					.woocommerce .cart-collaterals .cart_totals table tr.order-total td, 
					.woocommerce-page .cart-collaterals .cart_totals table tr.order-total td{
						color: <?php echo esc_html( $smof_data['primary-color'] );?>!important;
					}

					.widget #wp-calendar tr th:last-child, 
					.widget #wp-calendar tr th:nth-last-child(2), 
					.widget #wp-calendar tr td:last-child, 
					.widget #wp-calendar tr td:nth-last-child(2), 
					.widget #wp-calendar tr td:last-child a, 
					.widget #wp-calendar tr td:nth-last-child(2) a{
						color: <?php echo esc_html( $smof_data['primary-color'] );?>;
					}

					@media only screen and (max-width: 1199px) {
						.widget #wp-calendar td#today {
							color: <?php echo esc_html( $smof_data['primary-color'] );?>!important;
						}
					}
					@media only screen and (min-width: 768px) {
						.event-isotope-filter li:hover {
							color: <?php echo esc_html( $smof_data['primary-color'] );?>;
						}
						.course-isotope-filter li:hover,
						.k2t-page-topnav ul.menu > li:hover > a{
							color: <?php echo esc_html( $smof_data['primary-color'] );?>;
						}
					}
					@media only screen and (max-width: 767px) {
						.vc_tta.vc_general .vc_tta-panel.vc_active div.vc_tta-panel-heading {
							background: <?php echo esc_html( $smof_data['primary-color'] );?>!important;
						}
					}
				
				<?php endif;

				/* Logo margin
				------------------------------------------------- */
				$header_style    = ( function_exists( 'get_field' ) ) ? get_field( 'page_header_style', get_the_ID() ) : '';
				if ( empty( $header_style ) )
					$header_style = $smof_data['header-style'];
				if ( isset ( $smof_data[ $header_style . 'logo-margin-top'] ) || isset ( $smof_data[ $header_style . 'logo-margin-left'] ) || isset ( $smof_data[ $header_style . 'logo-margin-right'] ) || isset ( $smof_data[ $header_style . 'logo-margin-bottom'] ) ) :
					echo '
						.k2t-logo { margin-top: ' . esc_html( $smof_data[ $header_style . 'logo-margin-top'] ) . 'px;margin-left: ' . esc_html( $smof_data[ $header_style . 'logo-margin-left'] ) . 'px;margin-right: ' . esc_html( $smof_data[ $header_style . 'logo-margin-right'] ) . 'px;margin-bottom: ' . esc_html( $smof_data[ $header_style . 'logo-margin-bottom'] ) . 'px; }
					';
				endif;
	
				/* Global color scheme
				------------------------------------------------- */
				if ( $smof_data['heading-color'] || $smof_data['heading-font'] ) :
					echo '
						h1, h2, h3, h4, h1 *, h2 *, h3 *, h4 * { color: ' . esc_html( $smof_data['heading-color'] ) . '; font-family: ' . esc_html( $smof_data['heading-font'] ) . '; }
					';
				endif;

				if ( $smof_data['text-color'] ) :
					echo '
						body, button, input, select, textarea { color: ' . esc_html( $smof_data['text-color'] ) . '; }
					';
				endif;

				if ( $smof_data['footer-link-color'] ) :
					echo '
						.k2t-footer a { color: ' . esc_html( $smof_data['footer-link-color'] ) . '; }
					';
				endif;
	
				if ( $smof_data['link-color'] ) :
					echo '
						a { color: ' . esc_html( $smof_data['link-color'] ) . '; }
					';
				endif;

				if ( $smof_data['link-hover-color'] ) :
					echo '
						a:hover, a:focus { color: ' . esc_html( $smof_data['link-hover-color'] ) . '!important; }
					';
				endif;
	
				if ( $smof_data['main-menu-color'] ) :
					echo '
						.k2t-header-mid .k2t-menu li a,
						.k2t-header-mid .k2t-menu > li:hover a, .k2t-header-mid .k2t-menu > li > a:hover, .k2t-header-mid .k2t-menu > li.current-menu-item > a, .k2t-header-mid .k2t-menu > li.current-menu-parent > a, .k2t-header-mid .k2t-menu > li.current-menu-ancestor > a { color: ' . esc_html( $smof_data['main-menu-color'] ) . '!important; }
					';
				endif;

				if ( $smof_data['sub-menu-color'] ) :
					echo '
						.k2t-header-mid .k2t-menu li ul li a span::before,
						.k2t-header-mid .k2t-menu > li:hover a{ color: ' . esc_html( $smof_data['sub-menu-color'] ) . ' !important; }
					';
				endif;
				
				/* Typography
				------------------------------------------------- */
				if ( $smof_data['body-font'] || $smof_data['body-size'] ) :
					echo '
						body { font-family: ' . esc_html( $smof_data['body-font'] ) . '; font-size: ' . esc_html( $smof_data['body-size'] ) . 'px; }
					';
				endif;

				if ( $smof_data['mainnav-font'] || $smof_data['mainnav-size'] ) :
					echo '
						.k2t-header-mid .k2t-menu, .k2t-header .k2t-menu .mega-container ul, .vertical-menu .k2t-header-mid .k2t-menu { font-family: ' . esc_html( $smof_data['mainnav-font'] ) . '; font-size: ' . esc_html( $smof_data['mainnav-size'] ) . 'px; }
					';
				endif;

				if ( $smof_data['mainnav-text-transform'] ) :
					echo '
						.k2t-header-mid .k2t-menu > li > a { text-transform: ' . esc_html( $smof_data['mainnav-text-transform'] ) . '; }
					';
				endif;

				if ( $smof_data['mainnav-font-weight'] ) :
					echo '
						.k2t-header-mid .k2t-menu > li > a { font-weight: ' . esc_html( $smof_data['mainnav-font-weight'] ) . '; }
					';
				endif;

				if ( $smof_data['h1-size'] || $smof_data['h2-size'] || $smof_data['h3-size'] || $smof_data['h4-size'] || $smof_data['h5-size'] || $smof_data['h6-size'] ) :
					echo '
						h1 { font-size: ' . esc_html( $smof_data['h1-size'] ) . 'px; }
						h2 { font-size: ' . esc_html( $smof_data['h2-size'] ) . 'px; }
						h3 { font-size: ' . esc_html( $smof_data['h3-size'] ) . 'px; }
						h4 { font-size: ' . esc_html( $smof_data['h4-size'] ) . 'px; }
						h5 { font-size: ' . esc_html( $smof_data['h5-size'] ) . 'px; }
						h6 { font-size: ' . esc_html( $smof_data['h6-size'] ) . 'px; }
					';
				endif;

				if ( $smof_data['submenu-mainnav-size'] ) :
					echo '
						.k2t-header-mid .k2t-menu .sub-menu { font-size: ' . esc_html( $smof_data['submenu-mainnav-size'] ) . 'px; }
					';
				endif;
			
				/* Custom CSS
				------------------------------------------------- */
				if ( isset ( $smof_data['custom_css'] ) ) :
					$custom_css = $smof_data['custom_css'];
					echo ( $custom_css );
				endif;
				
				/* Header custom css */
				if ( !empty( $smof_data['max-width-header'] ) ) : ?>
					.k2t-header.full-width .k2t-wrap,
					.k2t-header-top.full-width .k2t-wrap,
					.k2t-header-mid.full-width .k2t-wrap,
					.k2t-header-bot.full-width .k2t-wrap {
						max-width: <?php echo esc_html( $smof_data['max-width-header'] ); ?>;
					}
				<?php endif;
				for ( $h = 1; $h < 4; $h++ ) :
					$opacity_only_sticky = isset( $smof_data['opacity_only_sticky_header_section_' . $h ] ) ? $smof_data['opacity_only_sticky_header_section_' . $h ] : '' ;
					$css    			 = isset( $smof_data['custom_css_setting_header_section_' . $h ] ) ? $smof_data['custom_css_setting_header_section_' . $h ] : '' ;
					$opacity		 	 = isset( $smof_data['opacity_setting_header_section_' . $h ] ) ?  $smof_data['opacity_setting_header_section_' . $h ] : '' ;
					$hex 		         = isset( $smof_data['bg_color_setting_header_section_' . $h ] ) ? $smof_data['bg_color_setting_header_section_' . $h  ] : '';
					$rgb      		     = k2t_furion_hex2rgb( $hex );
					switch ( $h ) {
						case '1':
							$header_pos = 'top';
							break;
						case '2':
							$header_pos = 'mid';
							break;
						case '3':
							$header_pos = 'bot';
							break;
						default:
							# code...
							break;
					}
					if ( $opacity_only_sticky == 'yes' ) {
						if ( $opacity < 100 ) {
							$a = ', 0.' . $opacity;
						} else {
							$a = ', 1';
						}
						$css .= '  .k2t-header-' . $header_pos . '.opacity-only-sticky.sticky .k2t-wrap { ' . 'background-color: rgba(' . $rgb['0'] . ',' . $rgb['1'] . ',' . $rgb['2'] . $a .') !important;'  .  ' } ';
					}
					if ( ! empty( $css )  ) {
						echo esc_html( $css );
					}
				endfor;

				?>
	
				/* Primary color
				------------------------------------------------- */
				
	
			</style>	
		<?php 
			$output = ob_get_clean();
			$output = k2t_furion_mini_output( $output );

			echo ( $output );
		} //end function
		add_action( 'wp_head','k2t_furion_front_end_enqueue_inline_css' );
	}
	
	
	/*--------------------------------------------------------------
		Enqueue google fonts
	--------------------------------------------------------------*/
	if ( ! function_exists( 'k2t_furion_enqueue_google_fonts' ) ) {
		function k2t_furion_enqueue_google_fonts() {
			global $smof_data;
			
			$protocol = is_ssl() ? 'https' : 'http';
			if ( isset ( $smof_data['body-font'] ) && in_array ( $smof_data['body-font'], k2t_furion_google_fonts() ) ) {
				$body_font = $smof_data['body-font'];
				wp_enqueue_style( 'k2t-google-font-' . str_replace( ' ','-',$body_font ), "$protocol://fonts.googleapis.com/css?family=" . str_replace(' ','+', $body_font ) . ":100,200,300,400,500,600,700,800,900&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese" );
			}
			
			if ( isset ( $smof_data['heading-font'] ) && in_array ( $smof_data['heading-font'], k2t_furion_google_fonts() ) ) {
				$heading_font = $smof_data['heading-font'];		
				wp_enqueue_style( 'k2t-google-font-' . str_replace( ' ','-',$heading_font ), "$protocol://fonts.googleapis.com/css?family=" . str_replace(' ','+', $heading_font ) . ":100,200,300,400,500,600,700,800,900&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese" );
			}
			
			if ( isset ( $smof_data['mainnav-font'] ) && in_array ( $smof_data['mainnav-font'], k2t_furion_google_fonts() ) ) {
				$mainnav_font = $smof_data['mainnav-font'];		
				wp_enqueue_style( 'k2t-google-font-' . str_replace( ' ','-',$mainnav_font ), "$protocol://fonts.googleapis.com/css?family=" . str_replace(' ','+', $mainnav_font ) . ":100,200,300,400,500,600,700,800,900&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese" );
			}
		}
		add_action( 'wp_enqueue_scripts', 'k2t_furion_enqueue_google_fonts' );
	}

}

// Hooks on Back-end
else{
	
	/**
	 * Removes tabs such as the "Design Options" from the Visual Composer Settings
	 *
	 * @package Furion
	 */
	if ( class_exists( 'Vc_Manager' ) ) :
		vc_set_as_theme( true );
	endif;
	
	/*-------------------------------------------------------------------
		Map for Visual Composer Shortcode.
	--------------------------------------------------------------------*/
	if ( class_exists( 'Vc_Manager' ) ) :
		if ( ! function_exists( 'k2t_furion_vc_map_shortcodes' ) ) :
	
			function k2t_furion_vc_map_shortcodes() {

				// Include plugin.php
				include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
				$k2t_icon = array( '', 'fa fa-glass', 'fa fa-music', 'fa fa-search', 'fa fa-envelope-o', 'fa fa-heart', 'fa fa-star', 'fa fa-star-o', 'fa fa-user', 'fa fa-film', 'fa fa-th-large', 'fa fa-th', 'fa fa-th-list', 'fa fa-check', 'fa fa-remove', 'fa fa-close', 'fa fa-times', 'fa fa-search-plus', 'fa fa-search-minus', 'fa fa-power-off', 'fa fa-signal', 'fa fa-gear', 'fa fa-cog', 'fa fa-trash-o', 'fa fa-home', 'fa fa-file-o', 'fa fa-clock-o', 'fa fa-road', 'fa fa-download', 'fa fa-arrow-circle-o-down', 'fa fa-arrow-circle-o-up', 'fa fa-inbox', 'fa fa-play-circle-o', 'fa fa-rotate-right', 'fa fa-repeat', 'fa fa-refresh', 'fa fa-list-alt', 'fa fa-lock', 'fa fa-flag', 'fa fa-headphones', 'fa fa-volume-off', 'fa fa-volume-down', 'fa fa-volume-up', 'fa fa-qrcode', 'fa fa-barcode', 'fa fa-tag', 'fa fa-tags', 'fa fa-book', 'fa fa-bookmark', 'fa fa-print', 'fa fa-camera', 'fa fa-font', 'fa fa-bold', 'fa fa-italic', 'fa fa-text-height', 'fa fa-text-width', 'fa fa-align-left', 'fa fa-align-center', 'fa fa-align-right', 'fa fa-align-justify', 'fa fa-list', 'fa fa-dedent', 'fa fa-outdent', 'fa fa-indent', 'fa fa-video-camera', 'fa fa-photo', 'fa fa-image', 'fa fa-picture-o', 'fa fa-pencil', 'fa fa-map-marker', 'fa fa-adjust', 'fa fa-tint', 'fa fa-edit', 'fa fa-pencil-square-o', 'fa fa-share-square-o', 'fa fa-check-square-o', 'fa fa-arrows', 'fa fa-step-backward', 'fa fa-fast-backward', 'fa fa-backward', 'fa fa-play', 'fa fa-pause', 'fa fa-stop', 'fa fa-forward', 'fa fa-fast-forward', 'fa fa-step-forward', 'fa fa-eject', 'fa fa-chevron-left', 'fa fa-chevron-right', 'fa fa-plus-circle', 'fa fa-minus-circle', 'fa fa-times-circle', 'fa fa-check-circle', 'fa fa-question-circle', 'fa fa-info-circle', 'fa fa-crosshairs', 'fa fa-times-circle-o', 'fa fa-check-circle-o', 'fa fa-ban', 'fa fa-arrow-left', 'fa fa-arrow-right', 'fa fa-arrow-up', 'fa fa-arrow-down', 'fa fa-mail-forward', 'fa fa-share', 'fa fa-expand', 'fa fa-compress', 'fa fa-plus', 'fa fa-minus', 'fa fa-asterisk', 'fa fa-exclamation-circle', 'fa fa-gift', 'fa fa-leaf', 'fa fa-fire', 'fa fa-eye', 'fa fa-eye-slash', 'fa fa-warning', 'fa fa-exclamation-triangle', 'fa fa-plane', 'fa fa-calendar', 'fa fa-random', 'fa fa-comment', 'fa fa-magnet', 'fa fa-chevron-up', 'fa fa-chevron-down', 'fa fa-retweet', 'fa fa-shopping-cart', 'fa fa-folder', 'fa fa-folder-open', 'fa fa-arrows-v', 'fa fa-arrows-h', 'fa fa-bar-chart-o', 'fa fa-bar-chart', 'fa fa-twitter-square', 'fa fa-facebook-square', 'fa fa-camera-retro', 'fa fa-key', 'fa fa-gears', 'fa fa-cogs', 'fa fa-comments', 'fa fa-thumbs-o-up', 'fa fa-thumbs-o-down', 'fa fa-star-half', 'fa fa-heart-o', 'fa fa-sign-out', 'fa fa-linkedin-square', 'fa fa-thumb-tack', 'fa fa-external-link', 'fa fa-sign-in', 'fa fa-trophy', 'fa fa-github-square', 'fa fa-upload', 'fa fa-lemon-o', 'fa fa-phone', 'fa fa-square-o', 'fa fa-bookmark-o', 'fa fa-phone-square', 'fa fa-twitter', 'fa fa-facebook', 'fa fa-github', 'fa fa-unlock', 'fa fa-credit-card', 'fa fa-rss', 'fa fa-hdd-o', 'fa fa-bullhorn', 'fa fa-bell', 'fa fa-certificate', 'fa fa-hand-o-right', 'fa fa-hand-o-left', 'fa fa-hand-o-up', 'fa fa-hand-o-down', 'fa fa-arrow-circle-left', 'fa fa-arrow-circle-right', 'fa fa-arrow-circle-up', 'fa fa-arrow-circle-down', 'fa fa-globe', 'fa fa-wrench', 'fa fa-tasks', 'fa fa-filter', 'fa fa-briefcase', 'fa fa-arrows-alt', 'fa fa-group', 'fa fa-users', 'fa fa-chain', 'fa fa-link', 'fa fa-cloud', 'fa fa-flask', 'fa fa-cut', 'fa fa-scissors', 'fa fa-copy', 'fa fa-files-o', 'fa fa-paperclip', 'fa fa-save', 'fa fa-floppy-o', 'fa fa-square', 'fa fa-navicon', 'fa fa-reorder', 'fa fa-bars', 'fa fa-list-ul', 'fa fa-list-ol', 'fa fa-strikethrough', 'fa fa-underline', 'fa fa-table', 'fa fa-magic', 'fa fa-truck', 'fa fa-pinterest', 'fa fa-pinterest-square', 'fa fa-google-plus-square', 'fa fa-google-plus', 'fa fa-money', 'fa fa-caret-down', 'fa fa-caret-up', 'fa fa-caret-left', 'fa fa-caret-right', 'fa fa-columns', 'fa fa-unsorted', 'fa fa-sort', 'fa fa-sort-down', 'fa fa-sort-desc', 'fa fa-sort-up', 'fa fa-sort-asc', 'fa fa-envelope', 'fa fa-linkedin', 'fa fa-rotate-left', 'fa fa-undo', 'fa fa-legal', 'fa fa-gavel', 'fa fa-dashboard', 'fa fa-tachometer', 'fa fa-comment-o', 'fa fa-comments-o', 'fa fa-flash', 'fa fa-bolt', 'fa fa-sitemap', 'fa fa-umbrella', 'fa fa-paste', 'fa fa-clipboard', 'fa fa-lightbulb-o', 'fa fa-exchange', 'fa fa-cloud-download', 'fa fa-cloud-upload', 'fa fa-user-md', 'fa fa-stethoscope', 'fa fa-suitcase', 'fa fa-bell-o', 'fa fa-coffee', 'fa fa-cutlery', 'fa fa-file-text-o', 'fa fa-building-o', 'fa fa-hospital-o', 'fa fa-ambulance', 'fa fa-medkit', 'fa fa-fighter-jet', 'fa fa-beer', 'fa fa-h-square', 'fa fa-plus-square', 'fa fa-angle-double-left', 'fa fa-angle-double-right', 'fa fa-angle-double-up', 'fa fa-angle-double-down', 'fa fa-angle-left', 'fa fa-angle-right', 'fa fa-angle-up', 'fa fa-angle-down', 'fa fa-desktop', 'fa fa-laptop', 'fa fa-tablet', 'fa fa-mobile-phone', 'fa fa-mobile', 'fa fa-circle-o', 'fa fa-quote-left', 'fa fa-quote-right', 'fa fa-spinner', 'fa fa-circle', 'fa fa-mail-reply', 'fa fa-reply', 'fa fa-github-alt', 'fa fa-folder-o', 'fa fa-folder-open-o', 'fa fa-smile-o', 'fa fa-frown-o', 'fa fa-meh-o', 'fa fa-gamepad', 'fa fa-keyboard-o', 'fa fa-flag-o', 'fa fa-flag-checkered', 'fa fa-terminal', 'fa fa-code', 'fa fa-mail-reply-all', 'fa fa-reply-all', 'fa fa-star-half-empty', 'fa fa-star-half-full', 'fa fa-star-half-o', 'fa fa-location-arrow', 'fa fa-crop', 'fa fa-code-fork', 'fa fa-unlink', 'fa fa-chain-broken', 'fa fa-question', 'fa fa-info', 'fa fa-exclamation', 'fa fa-superscript', 'fa fa-subscript', 'fa fa-eraser', 'fa fa-puzzle-piece', 'fa fa-microphone', 'fa fa-microphone-slash', 'fa fa-shield', 'fa fa-calendar-o', 'fa fa-fire-extinguisher', 'fa fa-rocket', 'fa fa-maxcdn', 'fa fa-chevron-circle-left', 'fa fa-chevron-circle-right', 'fa fa-chevron-circle-up', 'fa fa-chevron-circle-down', 'fa fa-html5', 'fa fa-css3', 'fa fa-anchor', 'fa fa-unlock-alt', 'fa fa-bullseye', 'fa fa-ellipsis-h', 'fa fa-ellipsis-v', 'fa fa-rss-square', 'fa fa-play-circle', 'fa fa-ticket', 'fa fa-minus-square', 'fa fa-minus-square-o', 'fa fa-level-up', 'fa fa-level-down', 'fa fa-check-square', 'fa fa-pencil-square', 'fa fa-external-link-square', 'fa fa-share-square', 'fa fa-compass', 'fa fa-toggle-down', 'fa fa-caret-square-o-down', 'fa fa-toggle-up', 'fa fa-caret-square-o-up', 'fa fa-toggle-right', 'fa fa-caret-square-o-right', 'fa fa-euro', 'fa fa-eur', 'fa fa-gbp', 'fa fa-dollar', 'fa fa-usd', 'fa fa-rupee', 'fa fa-inr', 'fa fa-cny', 'fa fa-rmb', 'fa fa-yen', 'fa fa-jpy', 'fa fa-ruble', 'fa fa-rouble', 'fa fa-rub', 'fa fa-won', 'fa fa-krw', 'fa fa-bitcoin', 'fa fa-btc', 'fa fa-file', 'fa fa-file-text', 'fa fa-sort-alpha-asc', 'fa fa-sort-alpha-desc', 'fa fa-sort-amount-asc', 'fa fa-sort-amount-desc', 'fa fa-sort-numeric-asc', 'fa fa-sort-numeric-desc', 'fa fa-thumbs-up', 'fa fa-thumbs-down', 'fa fa-youtube-square', 'fa fa-youtube', 'fa fa-xing', 'fa fa-xing-square', 'fa fa-youtube-play', 'fa fa-dropbox', 'fa fa-stack-overflow', 'fa fa-instagram', 'fa fa-flickr', 'fa fa-adn', 'fa fa-bitbucket', 'fa fa-bitbucket-square', 'fa fa-tumblr', 'fa fa-tumblr-square', 'fa fa-long-arrow-down', 'fa fa-long-arrow-up', 'fa fa-long-arrow-left', 'fa fa-long-arrow-right', 'fa fa-apple', 'fa fa-windows', 'fa fa-android', 'fa fa-linux', 'fa fa-dribbble', 'fa fa-skype', 'fa fa-foursquare', 'fa fa-trello', 'fa fa-female', 'fa fa-male', 'fa fa-gittip', 'fa fa-sun-o', 'fa fa-moon-o', 'fa fa-archive', 'fa fa-bug', 'fa fa-vk', 'fa fa-weibo', 'fa fa-renren', 'fa fa-pagelines', 'fa fa-stack-exchange', 'fa fa-arrow-circle-o-right', 'fa fa-arrow-circle-o-left', 'fa fa-toggle-left', 'fa fa-caret-square-o-left', 'fa fa-dot-circle-o', 'fa fa-wheelchair', 'fa fa-vimeo-square', 'fa fa-turkish-lira', 'fa fa-try', 'fa fa-plus-square-o', 'fa fa-space-shuttle', 'fa fa-slack', 'fa fa-envelope-square', 'fa fa-wordpress', 'fa fa-openid', 'fa fa-institution', 'fa fa-bank', 'fa fa-university', 'fa fa-mortar-board', 'fa fa-graduation-cap', 'fa fa-yahoo', 'fa fa-google', 'fa fa-reddit', 'fa fa-reddit-square', 'fa fa-stumbleupon-circle', 'fa fa-stumbleupon', 'fa fa-delicious', 'fa fa-digg', 'fa fa-pied-piper', 'fa fa-pied-piper-alt', 'fa fa-drupal', 'fa fa-joomla', 'fa fa-language', 'fa fa-fax', 'fa fa-building', 'fa fa-child', 'fa fa-paw', 'fa fa-spoon', 'fa fa-cube', 'fa fa-cubes', 'fa fa-behance', 'fa fa-behance-square', 'fa fa-steam', 'fa fa-steam-square', 'fa fa-recycle', 'fa fa-automobile', 'fa fa-car', 'fa fa-cab', 'fa fa-taxi', 'fa fa-tree', 'fa fa-spotify', 'fa fa-deviantart', 'fa fa-soundcloud', 'fa fa-database', 'fa fa-file-pdf-o', 'fa fa-file-word-o', 'fa fa-file-excel-o', 'fa fa-file-powerpoint-o', 'fa fa-file-photo-o', 'fa fa-file-picture-o', 'fa fa-file-image-o', 'fa fa-file-zip-o', 'fa fa-file-archive-o', 'fa fa-file-sound-o', 'fa fa-file-audio-o', 'fa fa-file-movie-o', 'fa fa-file-video-o', 'fa fa-file-code-o', 'fa fa-vine', 'fa fa-codepen', 'fa fa-jsfiddle', 'fa fa-life-bouy', 'fa fa-life-buoy', 'fa fa-life-saver', 'fa fa-support', 'fa fa-life-ring', 'fa fa-circle-o-notch', 'fa fa-ra', 'fa fa-rebel', 'fa fa-ge', 'fa fa-empire', 'fa fa-git-square', 'fa fa-git', 'fa fa-hacker-news', 'fa fa-tencent-weibo', 'fa fa-qq', 'fa fa-wechat', 'fa fa-weixin', 'fa fa-send', 'fa fa-paper-plane', 'fa fa-send-o', 'fa fa-paper-plane-o', 'fa fa-history', 'fa fa-circle-thin', 'fa fa-header', 'fa fa-paragraph', 'fa fa-sliders', 'fa fa-share-alt', 'fa fa-share-alt-square', 'fa fa-bomb', 'fa fa-soccer-ball-o', 'fa fa-futbol-o', 'fa fa-tty', 'fa fa-binoculars', 'fa fa-plug', 'fa fa-slideshare', 'fa fa-twitch', 'fa fa-yelp', 'fa fa-newspaper-o', 'fa fa-wifi', 'fa fa-calculator', 'fa fa-paypal', 'fa fa-google-wallet', 'fa fa-cc-visa', 'fa fa-cc-mastercard', 'fa fa-cc-discover', 'fa fa-cc-amex', 'fa fa-cc-paypal', 'fa fa-cc-stripe', 'fa fa-bell-slash', 'fa fa-bell-slash-o', 'fa fa-trash', 'fa fa-copyright', 'fa fa-at', 'fa fa-eyedropper', 'fa fa-paint-brush', 'fa fa-birthday-cake', 'fa fa-area-chart', 'fa fa-pie-chart', 'fa fa-line-chart', 'fa fa-lastfm', 'fa fa-lastfm-square', 'fa fa-toggle-off', 'fa fa-toggle-on', 'fa fa-bicycle', 'fa fa-bus', 'fa fa-ioxhost', 'fa fa-angellist', 'fa fa-cc', 'fa fa-shekel', 'fa fa-sheqel', 'fa fa-ils', 'fa fa-meanpath' );
				sort( $k2t_icon );
				trim( join( 'fa ', $k2t_icon ) );
	
				$k2t_margin_top = array(
					'param_name'  => 'mgt',
					'heading'     => esc_html__( 'Margin Top', 'furion' ),
					'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
					'type'        => 'textfield',
				);
				$k2t_margin_right = array(
					'param_name'  => 'mgr',
					'heading'     => esc_html__( 'Margin Right', 'furion' ),
					'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
					'type'        => 'textfield',
				);
				$k2t_margin_bottom = array(
					'param_name'  => 'mgb',
					'heading'     => esc_html__( 'Margin Bottom', 'furion' ),
					'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
					'type'        => 'textfield',
				);
				$k2t_margin_left = array(
					'param_name'  => 'mgl',
					'heading'     => esc_html__( 'Margin Left', 'furion' ),
					'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
					'type'        => 'textfield',
				);
				$k2t_id = array(
					'param_name'  => 'id',
					'heading'     => esc_html__( 'ID', 'furion' ),
					'description' => esc_html__( '(Optional) Enter a unique ID.', 'furion' ),
					'type'        => 'textfield',
				);
				$k2t_class = array(
					'param_name'  => 'class',
					'heading'     => esc_html__( 'Class', 'furion' ),
					'description' => esc_html__( '(Optional) Enter a unique class name.', 'furion' ),
					'type'        => 'textfield',
				);
				$k2t_animation = array(
					'param_name' => 'anm',
					'heading' 	 => esc_html__( 'Enable Animation', 'furion' ),
					'type' 		 => 'checkbox',
					'value'      => array(
						'' => true
					)
				);
				$k2t_animation_name = array(
					'param_name' => 'anm_name',
					'heading' 	 => esc_html__( 'Animation', 'furion' ),
					'type' 		 => 'dropdown',
					'dependency' => array(
						'element' => 'anm',
						'value'   => array( '1' ),
						'not_empty' => false,
					),
					'value'      => array( 'bounce', 'flash', 'pulse', 'rubberBand', 'shake', 'swing', 'tada', 'wobble', 'bounceIn', 'bounceInDown', 'bounceInLeft', 'bounceInRight', 'bounceInUp', 'fadeIn', 'fadeInDown', 'fadeInDownBig', 'fadeInLeft', 'fadeInLeftBig', 'fadeInRight', 'fadeInRightBig', 'fadeInUp', 'fadeInUpBig', 'flip', 'flipInX', 'flipInY', 'lightSpeedIn', 'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft', 'rotateInUpRight', 'rollIn', 'zoomIn', 'zoomInDown', 'zoomInLeft', 'zoomInRight', 'zoomInUp' ),
				);
				$k2t_animation_delay = array(
					'param_name'  => 'anm_delay',
					'heading'     => esc_html__( 'Animation Delay', 'furion' ),
					'description' => esc_html__( 'Numeric value only, 1000 = 1second.', 'furion' ),
					'type'        => 'textfield',
					'std'		  => '2000',
					'dependency' => array(
						'element' => 'anm',
						'value'   => array( '1' ),
						'not_empty' => false,
					),
				);
	
				/*  [ Row ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_row', array(
						'name'        => esc_html__( 'Row', 'furion' ),
						'icon'        => 'fa fa-tasks',
						'category'    => esc_html__( 'Structure', 'furion' ),
						'description' => '',
					)
				);	

				/*  [ Posts Slider ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_posts_slider', array(
						'name'        => esc_html__( 'Posts Slider', 'furion' ),
						'icon'        => 'fa fa-refresh',
						'category'    => esc_html__( 'Content', 'furion' ),
						'description' => '',
					)
				);

				/*  [ Flickr Widget ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_flickr', array(
						'name'        => esc_html__( 'Flickr Widget', 'furion' ),
						'icon'        => 'fa fa-flickr',
						'category'    => esc_html__( 'Content', 'furion' ),
						'description' => '',
					)
				);


				/*  [ Masonry Media Grid ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_masonry_media_grid', array(
						'name'        => esc_html__( 'Masonry Media Grid', 'furion' ),
						'icon'        => 'fa fa-th',
						'category'    => esc_html__( 'Content', 'furion' ),
						'description' => '',
					)
				);


				/*  [ Revolution Slider ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'rev_slider_vc', array(
						'name'        => esc_html__( 'Revolution Slider', 'furion' ),
						'icon'        => 'fa fa-refresh',
						'category'    => esc_html__( 'Content', 'furion' ),
						'description' => '',
					)
				);
				
				/*  [ Featured products ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'featured_products', array(
						'name'        => esc_html__( 'Featured products', 'furion' ),
						'icon'        => 'fa fa-star',
						'category'    => esc_html__( 'WooCommerce', 'furion' ),
						'description' => '',
					)
				);

				/*  [ Recent products ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'recent_products', array(
						'name'        => esc_html__( 'Recent products', 'furion' ),
						'icon'        => 'fa fa-clock-o',
						'category'    => esc_html__( 'WooCommerce', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ Column ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_column', array(
						'name'        => esc_html__( 'Row', 'furion' ),
						'icon'        => 'fa fa-tasks',
						'category'    => esc_html__( 'Structure', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC Tabs]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_tabs', array(
						'name'        => esc_html__( 'Tabs', 'furion' ),
						'icon'        => 'fa fa-folder',
						'category'    => esc_html__( 'Content', 'furion' ),
						'description' => '',
					)
				);
				
				/*  [ VC Vertical tab ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_tour', array(
						'name'        => esc_html__( 'Vertical tabs', 'furion' ),
						'icon'        => 'fa fa-list-ul',
						'category'    => esc_html__( 'Structure', 'furion' ),
						'description' => '',
					)
				);
				
	
				/*  [ VC Tab]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_tab', array(
						'name'        => esc_html__( 'Text Block', 'furion' ),
						'icon'        => 'fa fa-text-height',
						'category'    => esc_html__( 'Content', 'furion' ),
						'description' => '',
					)
				);
				
	
				/*  [ VC Column Text ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_column_text', array(
						'name'        => esc_html__( 'Text Block', 'furion' ),
						'icon'        => 'fa fa-text-height',
						'category'    => esc_html__( 'Content', 'furion' ),
						'description' => '',
					)
				);
				
	
				/*  [ VC Separator ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_separator', array(
						'name'        => esc_html__( 'Separator', 'furion' ),
						'icon'        => 'fa fa-minus',
						'category'    => esc_html__( 'Structure', 'furion' ),
						'description' => '',
					)
				);
				
				/*  [ VC Separator With Text ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_text_separator', array(
						'name'        => esc_html__( 'Separator with text', 'furion' ),
						'icon'        => 'fa fa-text-width',
						'category'    => esc_html__( 'Structure', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC Message Box ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_message', array(
						'name'        => esc_html__( 'Message box', 'furion' ),
						'icon'        => 'fa fa-file-text-o',
						'category'    => esc_html__( 'Content', 'furion' ),
						'description' => '',
					)
				);
				
				/*  [ VC Facebook ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_facebook', array(
						'name'        => esc_html__( 'Facebook like', 'furion' ),
						'icon'        => 'fa fa-facebook',
						'category'    => esc_html__( 'Socials', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC Tweetmeme ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_tweetmeme', array(
						'name'        => esc_html__( 'Tweetmeme', 'furion' ),
						'icon'        => 'fa fa-twitter',
						'category'    => esc_html__( 'Socials', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC Google Plus ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_googleplus', array(
						'name'        => esc_html__( 'Google Plus', 'furion' ),
						'icon'        => 'fa fa-google-plus',
						'category'    => esc_html__( 'Socials', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC Pinterest ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_pinterest', array(
						'name'        => esc_html__( 'Pinterest', 'furion' ),
						'icon'        => 'fa fa-pinterest',
						'category'    => esc_html__( 'Socials', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC Single Image ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_single_image', array(
						'name'        => esc_html__( 'Single Image', 'furion' ),
						'icon'        => 'fa fa-image',
						'category'    => esc_html__( 'Content', 'furion' ),
						'description' => '',
					)
				);
				
	
				/*  [ VC Gallery ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_gallery', array(
						'name'        => esc_html__( 'Gallery', 'furion' ),
						'icon'        => 'fa fa-caret-square-o-right',
						'category'    => esc_html__( 'Media', 'furion' ),
						'description' => '',
					)
				);
				
	
				/*  [ VC Carousel ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_images_carousel', array(
						'name'        => esc_html__( 'Carousel', 'furion' ),
						'icon'        => 'fa fa-exchange',
						'category'    => esc_html__( 'Media', 'furion' ),
						'description' => '',
					)
				);
				
	
				/*  [ VC Toggle ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_toggle', array(
						'name'        => esc_html__( 'Toggles', 'furion' ),
						'icon'        => 'fa fa-indent',
						'category'    => esc_html__( 'Structure', 'furion' ),
						'description' => '',
					)
				);
				
	
				/*  [ VC Video ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_video', array(
						'name'        => esc_html__( 'Video', 'furion' ),
						'icon'        => 'fa fa-video-camera',
						'category'    => esc_html__( 'Media', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC Raw HTML ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_raw_html', array(
						'name'        => esc_html__( 'Raw HTML code', 'furion' ),
						'icon'        => 'fa fa-code',
						'category'    => esc_html__( 'Structure', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC Raw JS ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_raw_js', array(
						'name'        => esc_html__( 'Raw JS code', 'furion' ),
						'icon'        => 'fa fa-code',
						'category'    => esc_html__( 'Structure', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC Empty Space ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_empty_space', array(
						'name'        => esc_html__( 'Empty Space', 'furion' ),
						'icon'        => 'fa fa-arrows-v',
						'category'    => esc_html__( 'Structure', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC Custom Heading ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_custom_heading', array(
						'name'        => esc_html__( 'Custom Heading', 'furion' ),
						'icon'        => 'fa fa-header',
						'category'    => esc_html__( 'Typography', 'furion' ),
						'description' => '',
					)
				);
				
				/*  [ VC WP Search ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_wp_search', array(
						'name'        => esc_html__( 'WP Search', 'furion' ),
						'icon'        => 'fa fa-wordpress',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC WP Meta ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_wp_meta', array(
						'name'        => esc_html__( 'WP Meta', 'furion' ),
						'icon'        => 'fa fa-wordpress',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC WP recent comments ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_wp_recentcomments', array(
						'name'        => esc_html__( 'WP Recent Comments', 'furion' ),
						'icon'        => 'fa fa-wordpress',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC WP calendar ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_wp_calendar', array(
						'name'        => esc_html__( 'WP Calendar', 'furion' ),
						'icon'        => 'fa fa-wordpress',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC WP pages ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_wp_pages', array(
						'name'        => esc_html__( 'WP Pages', 'furion' ),
						'icon'        => 'fa fa-wordpress',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC WP Tagcloud ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_wp_tagcloud', array(
						'name'        => esc_html__( 'WP Tagcloud', 'furion' ),
						'icon'        => 'fa fa-wordpress',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC WP custom menu ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_wp_custommenu', array(
						'name'        => esc_html__( 'WP Custom Menu', 'furion' ),
						'icon'        => 'fa fa-wordpress',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC WP text ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_wp_text', array(
						'name'        => esc_html__( 'WP Text', 'furion' ),
						'icon'        => 'fa fa-wordpress',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC WP posts ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_wp_posts', array(
						'name'        => esc_html__( 'WP Posts', 'furion' ),
						'icon'        => 'fa fa-wordpress',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC WP categories ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_wp_categories', array(
						'name'        => esc_html__( 'WP Categories', 'furion' ),
						'icon'        => 'fa fa-wordpress',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC WP archives ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_wp_archives', array(
						'name'        => esc_html__( 'WP Archives', 'furion' ),
						'icon'        => 'fa fa-wordpress',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ VC WP rss ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'vc_wp_rss', array(
						'name'        => esc_html__( 'WP RSS', 'furion' ),
						'icon'        => 'fa fa-wordpress',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);
	
				/*  [ Contact form 7 ]
				- - - - - - - - - - - - - - - - - - - */
				vc_map_update(
					'contact-form-7', array(
						'name'        => esc_html__( 'Contact Form 7', 'furion' ),
						'icon'        => 'fa fa-envelope',
						'category'    => esc_html__( 'WordPress', 'furion' ),
						'description' => '',
					)
				);


				/*  [ K2T PORTFOLIO  ]
				- - - - - - - - - - - - - - - - - - - */

				$k2t_portfolio_shortcode = array(
					'base'            => 'portfolio',
					'name'            => esc_html__( 'K2T Portfolio', 'furion' ),
					'icon'            => 'fa fa-photo',
					'category'        => esc_html__( 'K2T FURION SHORTCODE', 'furion' ),
					'params'          => array(
						array(
							'param_name'  => 'layout_style',
							'heading' 	  => esc_html__( 'Layout Style', 'furion' ),
							'description' => esc_html__( 'select layout to display:', 'k2t'),
							'type' 		  => 'dropdown',
							'value'       => array( 
								esc_html__( 'Grid Style', 'furion' )    => 'grid',
								esc_html__( 'Masonry Style', 'furion' ) => 'masonry',
								esc_html__( 'Free Style', 'furion' )    => 'free',
								esc_html__( 'Slider', 'furion' )    => 'slider',
								esc_html__( 'time-line', 'furion' )    => 'time-line',
							),
						),
						array(
							'param_name'  => 'column',
							'heading'     => esc_html__( 'Number of column:', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value'       => array(
								esc_html__( '2 Column', 'furion' ) => '2',
								esc_html__( '3 Column', 'furion' ) => '3',
								esc_html__( '4 Column', 'furion' ) => '4',
							),
							'dependency' => array(
								'element' => 'layout_style',
								'value'   => array( 'grid', 'masonry', 'free' ),
							),
						),
						array(
							'param_name'  => 'hover_style',
							'heading'     => esc_html__( 'Hover Style', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value'       => array(
								esc_html__( 'Default', 'furion' ) => 'h-default',
								esc_html__( 'parallax', 'furion' ) => 'h-parallax',
								esc_html__( 'Right to left', 'furion' ) => 'h-rtl',
								esc_html__( 'Zoom', 'furion' ) => 'h-z',
								esc_html__( 'Open up', 'furion' ) => 'h-ou',
								esc_html__( 'Spread out', 'furion' ) => 'h-spo',
								esc_html__( 'Spread out 2', 'furion' ) => 'h-spo2',
							)
						),
						array(
							'param_name'  => 'categories',
							'heading'     => esc_html__( 'Filter by Categories :', 'furion' ),
							'description' => esc_html__( 'Categories seperate by "," like : category1, category2 ', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'post_per_page',
							'heading'     => esc_html__( 'Posts per page', 'furion' ),
							'description' => esc_html__( 'Please enter number, default is 10', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'navigation',
							'heading'     => esc_html__( 'Navigation', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'layout_style',
								'value'   => array( 'slider' ),
							),
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'pagination',
							'heading'     => esc_html__( 'Pagination', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'layout_style',
								'value'   => array( 'slider' ),
							),
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'auto_play',
							'heading'     => esc_html__( 'Auto Play', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'layout_style',
								'value'   => array( 'slider' ),
							),
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'filter',
							'heading'     => esc_html__( 'Display Filter bar', 'furion' ),
							'description' => esc_html__( 'Filter bar by categories', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								esc_html__( 'Show Filter bar', 'k2t')   => true,
							),
							'dependency' => array(
								'element' => 'layout_style',
								'value'   => array( 'grid', 'masonry','free' ),
							),
						),
						array(
							'param_name'  => 'filter_align',
							'heading'     => esc_html__( 'align filter bar', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value'       => array(
								esc_html__( 'Center', 'furion' ) => 'align-center',
								esc_html__( 'Right', 'furion' )  =>  'align-right' ,
								esc_html__( 'Left', 'furion' )  => 'align-left',
							),
							'dependency' => array(
								'element' => 'filter',
								'value'   => array( '1' ),
							),
						),
						array(
							'param_name'  => 'toggle_filter',
							'heading'     => esc_html__( 'Toggle filter', 'furion' ),
							'description' => esc_html__( 'Click a button to hidden or display filter bar', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								esc_html__( '', 'furion' )  => true
							),
							'dependency' => array(
								'element' => 'filter',
								'value'   => array( '1' ),
							),
						),
						array(
							'param_name'  => 'no_padding',
							'heading'     => esc_html__( 'No padding', 'furion' ),
							'description' => esc_html__( 'No space between items', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								esc_html__( '', 'furion' )  => true
							),
						),
						$k2t_id, 
						$k2t_class,
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
					)
				);
				vc_map( $k2t_portfolio_shortcode );

				/*  [ Furion Register ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_register_shortcodes = array(
					'base'            => 'register',
					'name'            => esc_html__( 'Furion register', 'furion' ),
					'icon'            => 'fa fa-photo',
					'category'        => esc_html__( 'Furion Shortcode', 'furion' ),
					'params'          => array(
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Register Title', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'website',
							'heading'     => esc_html__( 'website', 'furion' ),
							'type'        => 'checkbox',
							'value'		  => array(
								'' 	=> 'true',
								),
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'first_name',
							'heading'     => esc_html__( 'First name', 'furion' ),
							'type'        => 'checkbox',
							'value'		  => array(
								'' 	=> 'true',
								),
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'last_name',
							'heading'     => esc_html__( 'Last name', 'furion' ),
							'type'        => 'checkbox',
							'value'		  => array(
								'' 	=> 'true',
								),
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'bio',
							'heading'     => esc_html__( 'bio', 'furion' ),
							'type'        => 'checkbox',
							'value'		  => array(
								'' 	=> 'true',
								),
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'submit',
							'heading'     => esc_html__( 'Text on submit button', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),

						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
					)
				);
				vc_map( $k2t_register_shortcodes );
				
					/*  [ Counter ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_counter = array(
					'base'            => 'counter',
					'name'            => esc_html__( 'Counter', 'furion' ),
					'icon'            => 'fa fa-list-ol',
					'category'        => esc_html__( 'Content', 'furion' ),
					'as_child'		  => array( 'isotope' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name' => 'style_type',
							'heading' 	 => esc_html__( 'Style', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Icon Center', 'furion' )  => '1',
								esc_html__( 'Icon Left', 'furion' ) => '2',
							),
						),
						array(
							'param_name'  => 'border_width',
							'heading'     => esc_html__( 'Border Width', 'furion' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name' => 'border_style',
							'heading'    => esc_html__( 'Border Style', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Solid', 'furion' )  => 'solid',
								esc_html__( 'Dashed', 'furion' ) => 'dashed'
							),
						),
						array(
							'param_name'  => 'border_color',
							'heading'     => esc_html__( 'Border', 'furion' ),
							'type'        => 'colorpicker'
						),
						array(
							'param_name' => 'icon_type',
							'heading'    => esc_html__( 'Icon Type', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Icon font', 'furion' )    => 'icon_font',
								esc_html__( 'Icon Graphic', 'furion' ) => 'icon_graphic'
							),
						),
						array(
							'param_name'  => 'icon_font',
							'heading'     => esc_html__( 'Choose Icon', 'furion' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name'  => 'icon_size',
							'heading'     => esc_html__( 'Icon size', 'furion' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name'  => 'icon_color',
							'heading'     => esc_html__( 'Icon Color', 'furion' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name'  => 'icon_background',
							'heading'     => esc_html__( 'Icon Background', 'furion' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name'  => 'icon_border_color',
							'heading'     => esc_html__( 'Icon Border', 'furion' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name' => 'icon_border_style',
							'heading'    => esc_html__( 'Icon Border Style', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Solid', 'furion' )  => 'solid',
								esc_html__( 'Dashed', 'furion' ) => 'dashed'
							),
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							)
						),
						array(
							'param_name'  => 'icon_border_width',
							'heading'     => esc_html__( 'Icon Border Width', 'furion' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name'  => 'icon_border_radius',
							'heading'     => esc_html__( 'Icon Border Radius', 'furion' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name'  => 'icon_graphic',
							'heading'     => esc_html__( 'Upload icon graphic', 'furion' ),
							'type'        => 'attach_image',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_graphic' ),
							),
						),
						array(
							'param_name'  => 'number',
							'heading'     => esc_html__( 'Counter to number', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'number_font_size',
							'heading'     => esc_html__( 'Number font size', 'furion' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'number_color',
							'heading'     => esc_html__( 'Number Color', 'furion' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Counter Title', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'title_font_size',
							'heading'     => esc_html__( 'Title font size', 'furion' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'title_color',
							'heading'     => esc_html__( 'Title Color', 'furion' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'speed',
							'heading'     => esc_html__( 'Animation Speed', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'delay',
							'heading'     => esc_html__( 'Animation Delay', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_counter );
				
				/*  [ Brands ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_brands = array(
					'base'            => 'brands',
					'name'            => esc_html__( 'Brands', 'furion' ),
					'icon'            => 'fa fa-photo',
					'category'        => array( esc_html__( 'Content', 'furion' ), esc_html__( 'K2T SHORTCODE FURION', 'furion' ) ),
					'as_parent'       => array( 'only' => 'brand' ),
					'content_element' => true,
					'js_view'         => 'VcColumnView',
					'params'          => array(
						array(
							'param_name'  => 'column',
							'heading' 	  => esc_html__( 'Column', 'furion' ),
							'description' => esc_html__( 'Select column display brand', 'k2t'),
							'type' 		  => 'dropdown',
							'value'       => array( '0', '1', '2', '3', '4', '5', '6', '7', '8' ),
						),
						array(
							'param_name'  => 'align',
							'heading'     => esc_html__( 'align', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'value' 	  => array(
								esc_html__( 'Left', 'furion' ) => 'align-left',
								esc_html__( 'Right', 'furion' ) => 'align-right',
								esc_html__( 'Center', 'furion' ) => 'align-center',
							),
							'holder'      => 'div',
							'dependency'  => array(
								'element' => 'column',
								'value'  => '0',
							),
						),
						array(
							'param_name'  => 'padding',
							'heading'     => esc_html__( 'Padding', 'furion' ),
							'description' => esc_html__( 'space beetwen items like : 20px ', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency'  => array(
								'element' => 'align',
								'value'	  => array( 'align-left', 'align-right', 'align-center' )
							),
						),
						array(
							'param_name'  => 'opacity_hover',
							'heading'     => esc_html__( 'Opacity hover', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'opacity-hover'
							)
						),
						array(
							'param_name'  => 'grayscale',
							'heading'     => esc_html__( 'Grayscale', 'furion' ),
							'description' => esc_html__( 'Display grayscale.', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						),
						$k2t_id, $k2t_class
					)
				);
				vc_map( $k2t_brands );
	
				/*  [ Brand Items ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_brands_item = array(
					'base'            => 'brand',
					'name'            => esc_html__( 'Brands Item', 'furion' ),
					'icon'            => 'fa fa-photo',
					'category'        => esc_html__( 'Content', 'furion' ),
					'as_child'        => array( 'only' => 'brands' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Brand Title', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'tooltip',
							'heading'     => esc_html__( 'Tooltip', 'furion' ),
							'description' => esc_html__( 'Enable tooltip.', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'link',
							'heading'     => esc_html__( 'Upload Brand', 'furion' ),
							'type'        => 'attach_image',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'size',
							'heading'     => esc_html__( 'Size', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value'		  => get_intermediate_image_sizes(),
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
					)
				);
				vc_map( $k2t_brands_item );
	
				/*  [ Button ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_button = array(
					'base'            => 'button',
					'name'            => esc_html__( 'Button', 'furion' ),
					'icon'            => 'fa fa-dot-circle-o',
					'category'        => esc_html__( 'Typography', 'furion' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Button Text', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'link',
							'heading'     => esc_html__( 'Link', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'target',
							'heading' 	 => esc_html__( 'Link Target', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Open in a new window', 'furion' )                      => '_blank',
								esc_html__( 'Open in the same frame as it was clicked', 'furion' )  => '_self'
							),
							'dependency' => array(
								'element' 		=> 'link',
								'not_empty'   	=> true,
							),
						),
						array(
							'param_name' => 'icon',
							'heading' 	 => esc_html__( 'Choose Icon', 'furion' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
						),
						array(
							'param_name' => 'icon_position',
							'heading' 	 => esc_html__( 'Icon Position', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Right', 'furion' ) 				=> 'right',
								esc_html__( 'Left', 'furion' )  				=> 'left'
							),
							'dependency' => array(
								'element' 		=> 'icon',
								'not_empty'   	=> true,
							),
						),
						array(
							'param_name' => 'button_style',
							'heading' 	 => esc_html__( 'Button style', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Default button', 'furion' ) 		=> 'default',
								esc_html__( 'Outline button', 'furion' ) 		=> 'outline',
								esc_html__( 'Around button', 'furion' ) 		=> 'around',
								esc_html__( 'Shadow button', 'furion' ) 		=> 'shadow',
							),
						),
						array(
							'param_name' => 'button_color',
							'heading' 	 => esc_html__( 'Button color', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Default', 'furion' )   			=> 'default',
								esc_html__( 'Red', 'furion' )   				=> 'red',
								esc_html__( 'Deep Purple', 'furion' ) 			=> 'deep_purple',
								esc_html__( 'Blue', 'furion' )  				=> 'blue',
								esc_html__( 'Green', 'furion' )  				=> 'green',
								esc_html__( 'Amber', 'furion' )  				=> 'amber',
								esc_html__( 'Deep Orange', 'furion' )  		=> 'deep_orange',
								esc_html__( 'Black', 'furion' )  				=> 'black',
								esc_html__( 'White', 'furion' )  				=> 'white',
								esc_html__( 'Custom', 'furion' )  				=> 'custom',
							),
							'dependency' => array(
								'element' => 'button_style',
								'value'   => array( 'default', 'around', 'shadow' )
							),
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Button Background Color', 'furion' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name'  => 'text_color',
							'heading'     => esc_html__( 'Button Text Color', 'furion' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name'  => 'hover_bg_color',
							'heading'     => esc_html__( 'Background Hover Color', 'furion' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name'  => 'hover_text_color',
							'heading'     => esc_html__( 'Text Hover Color', 'furion' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name'  => 'border_color',
							'heading'     => esc_html__( 'Button border Color', 'furion' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name'  => 'border_width',
							'heading'     => esc_html__( 'Button border width', 'furion' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name'  => 'hover_border_color',
							'heading'     => esc_html__( 'Border Hover Color', 'furion' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name' => 'size',
							'heading' 	 => esc_html__( 'Size', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Small', 'furion' ) 				=> 'small',
								esc_html__( 'Medium', 'furion' ) 				=> 'medium',
								esc_html__( 'Large', 'furion' )  				=> 'large'
							),
						),
						array(
							'param_name' => 'align',
							'heading' 	 => esc_html__( 'Align', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Left', 'furion' )   				=> 'left',
								esc_html__( 'Center', 'furion' ) 				=> 'center',
								esc_html__( 'Right', 'furion' )  				=> 'right'
							),
						),
						array(
							'param_name'  => 'fullwidth',
							'heading'     => esc_html__( 'Button Full Width', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'pill',
							'heading'     => esc_html__( 'Pill', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'radius',
							'heading'     => esc_html__( 'Button radius', 'furion' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'pill',
								'value'   => 'true'
							),
						),
						array(
							'param_name'  => 'd3',
							'heading'     => esc_html__( '3D', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						),
						$k2t_margin_top,
						$k2t_margin_right,
						$k2t_margin_bottom,
						$k2t_margin_left,
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_button );
	
				/*  [ Circle button ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_circle_button = array(
					'base'            => 'circle_button',
					'name'            => esc_html__( 'Circle Button', 'furion' ),
					'icon'            => 'fa fa-circle',
					'category'        => esc_html__( 'Typography', 'furion' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'name',
							'heading'     => esc_html__( 'Button Name', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'link',
							'heading'     => esc_html__( 'Link To', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'icon_hover',
							'heading' 	 => esc_html__( 'Icon Hover', 'furion' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
						),
						array(
							'param_name'  => 'background_color',
							'heading'     => esc_html__( 'Button Background Color', 'furion' ),
							'type'        => 'colorpicker',
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_circle_button );
	
				/*  [ Google Map ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_google_map = array(
					'base'            => 'google_map',
					'name'            => esc_html__( 'K2t Google Maps', 'furion' ),
					'icon'            => 'fa fa-map-marker',
					'category'        => esc_html__( 'Marketing', 'furion' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'z',
							'heading'     => esc_html__( 'Zoom Level', 'furion' ),
							'description' => esc_html__( 'Between 0-20', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'lat',
							'heading'     => esc_html__( 'Latitude', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'lon',
							'heading'     => esc_html__( 'Longitude', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'w',
							'heading'     => esc_html__( 'Width', 'furion' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'h',
							'heading'     => esc_html__( 'Height', 'furion' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'address',
							'heading'     => esc_html__( 'Address', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'marker',
							'heading' 	 => esc_html__( 'Marker', 'furion' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							),
						),
						array(
							'param_name'  => 'markerimage',
							'heading'     => esc_html__( 'Marker Image', 'furion' ),
							'description' => esc_html__( 'Change default Marker.', 'furion' ),
							'type'        => 'attach_image',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'marker',
								'value'   => array( 'true' ),
							),
						),
						array(
							'param_name' => 'traffic',
							'heading' 	 => esc_html__( 'Show Traffic', 'furion' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							)
						),
						array(
							'param_name' => 'draggable',
							'heading' 	 => esc_html__( 'Draggable', 'furion' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							)
						),
						array(
							'param_name' => 'infowindowdefault',
							'heading' 	 => esc_html__( 'Show Info Map', 'furion' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'infowindow',
							'heading'     => esc_html__( 'Content Info Map', 'furion' ),
							'description' => esc_html__( 'Strong, br are accepted.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'hidecontrols',
							'heading' 	 => esc_html__( 'Hide Control', 'furion' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							)
						),
						array(
							'param_name' => 'scrollwheel',
							'heading' 	 => esc_html__( 'Scroll wheel zooming', 'furion' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							)
						),
						array(
							'param_name' => 'maptype',
							'heading' 	 => esc_html__( 'Map Type', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'ROADMAP', 'furion' )   => 'ROADMAP',
								esc_html__( 'SATELLITE', 'furion' ) => 'SATELLITE',
								esc_html__( 'HYBRID', 'furion' )    => 'HYBRID',
								esc_html__( 'TERRAIN', 'furion' )   => 'TERRAIN'
							),
						),
						array(
							'param_name' => 'mapstype',
							'heading' 	 => esc_html__( 'Map style', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'None', 'furion' )   => '',
								esc_html__( 'Subtle Grayscale', 'furion' )   => 'grayscale',
								esc_html__( 'Blue water', 'furion' ) => 'blue_water',
								esc_html__( 'Pale Dawn', 'furion' ) => 'pale_dawn',
								esc_html__( 'Shades of Grey', 'furion' ) => 'shades_of_grey',
							),
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Background Color', 'furion' ),
							'type'        => 'colorpicker',
						),
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_google_map );
	
				/*  [ Heading ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_heading = array(
					'base'            => 'heading',
					'name'            => esc_html__( 'K2T Heading', 'furion' ),
					'icon'            => 'fa fa-header',
					'category'        => array( esc_html__( 'Typography', 'furion' ), esc_html__( 'K2T FURION SHORTCODE', 'furion' ) ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'Title', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'value'       => ''
						),
						array(
							'param_name' => 'h',
							'heading' 	 => esc_html__( 'Heading Tag', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array( 
								esc_html__( 'H1', 'furion' ) => 'h1', 
								esc_html__( 'H2', 'furion' ) => 'h2', 
								esc_html__( 'H3', 'furion' ) => 'h3', 
								esc_html__( 'H4', 'furion' ) => 'h4', 
								esc_html__( 'H5', 'furion' ) => 'h5', 
								esc_html__( 'H6', 'furion' ) => 'h6', 
								esc_html__( 'Custom', 'furion' ) => 'custom',
							),
						),
						array(
							'param_name'  => 'font_size',
							'heading'     => esc_html__( 'Custom Font Size', 'furion' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'h',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name' => 'align',
							'heading' 	 => esc_html__( 'Align', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Left', 'furion' )   => 'left',
								esc_html__( 'Center', 'furion' ) => 'center',
								esc_html__( 'Right', 'furion' )  => 'right'
							),
						),
						array(
							'param_name'  => 'font',
							'heading'     => esc_html__( 'Title Font', 'furion' ),
							'description' => esc_html__( 'Use Google Font', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Title Color', 'furion' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'excerpt',
							'heading'     => esc_html__( 'Excertp', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'value'       => ''
						),
						array(
							'param_name'  => 'ex_font_size',
							'heading'     => esc_html__( 'Custom Font Size', 'furion' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'ex_color',
							'heading'     => esc_html__( 'Excerpt Color', 'furion' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name' => 'border',
							'heading' 	 => esc_html__( 'Has border', 'furion' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							)
						),
						array(
							'param_name' => 'border_style',
							'heading' 	 => esc_html__( 'Border Style', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Two Dots', 'furion' )   			=> 'two_dots',
								esc_html__( 'Short Line', 'furion' )   		=> 'short_line',
								esc_html__( 'Bottom Icon', 'furion' ) 		=> 'bottom_icon',
								esc_html__( 'Heading', 'furion' )  			=> 'heading',
								esc_html__( 'Boxed Heading', 'furion' )  		=> 'boxed_heading',
								esc_html__( 'Bottom Border', 'furion' )  		=> 'bottom_border',
								esc_html__( 'Line Through', 'furion' )  		=> 'line_through',
								esc_html__( 'Double Line', 'furion' )  		=> 'double_line',
								esc_html__( 'Dotted Line', 'furion' )  		=> 'three_dotted',
								esc_html__( 'Fat Line', 'furion' )  			=> 'fat_line',
								esc_html__( 'Custom Line', 'furion' )  		=> 'custom_line',
							),
							'dependency' => array(
								'element' => 'border',
								'value'   => array( 'true' )
							),
						),
						array(
							'param_name'  => 'cb_width',
							'heading'     => esc_html__( 'Width', 'furion' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'border_style',
								'value'   => array( 'custom_line' )
							),
						),
						array(
							'param_name'  => 'cb_height',
							'heading'     => esc_html__( 'Height', 'furion' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'border_style',
								'value'   => array( 'custom_line' )
							),
						),
						array(
							'param_name'  => 'cb_position',
							'heading'     => esc_html__( 'position', 'furion' ),
							'description' => esc_html__( 'postion of border', 'furion' ),
							'type'        => 'dropdown',
							'value' 	  => array(
								esc_html__( 'Top', 'furion' )		  => 'top',
								esc_html__( 'Right', 'furion' )	  => 'right',
								esc_html__( 'Bottom', 'furion' )     => 'bottom',
								esc_html__( 'Left', 'furion' )	  	  => 'left',
							),
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'border_style',
								'value'   => array( 'custom_line' )
							),
						),
						array(
							'param_name'  => 'cb_lr_top',
							'heading'     => esc_html__( 'Padding top', 'furion' ),
							'description' => esc_html__( 'spacing from top . Ex: 25px', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'cb_position',
								'value'   => array( 'right', 'left' ),
							),
						),
						array(
							'param_name'  => 'cb_lr_padding',
							'heading'     => esc_html__( 'Padding beetwen', 'furion' ),
							'description' => esc_html__( 'spacing beetwen border and heading text . Ex: 26px', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'cb_position',
								'value'   => array( 'right', 'left' ),
							),
						),
						array(
							'param_name'  => 'cb_tb_top',
							'heading'     => esc_html__( 'Padding top', 'furion' ),
							'description' => esc_html__( 'spacing top . Ex: 25px', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'cb_position',
								'value'   => array( 'top', 'bottom' ),
							),
						),
						array(
							'param_name'  => 'cb_tb_bottom',
							'heading'     => esc_html__( 'Padding bottm', 'furion' ),
							'description' => esc_html__( 'spacing bottom . Ex: 25px', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'cb_position',
								'value'   => array( 'top', 'bottom' ),
							),
						),
						array(
							'param_name'  => 'border_color',
							'heading'     => esc_html__( 'Border Color', 'furion' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'border',
								'value'   => array( 'true' )
							),
						),
						array(
							'param_name' => 'icon',
							'heading' 	 => esc_html__( 'Choose Icon', 'furion' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
							'dependency' => array(
								'element' => 'border_style',
								'value'   => array( 'bottom_icon', 'boxed_heading' )
							),
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_heading );
	
				/*  [ Icon Box ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_icon_box = array(
					'base'            => 'iconbox',
					'name'            => esc_html__( 'Icon Box', 'furion' ),
					'icon'            => 'fa fa-th',
					'category'        => esc_html__( 'Marketing', 'furion' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name' => 'layout',
							'heading' 	 => esc_html__( 'Layout', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array( '1', '2', '3', '4' , '5', '6' ),
						),
						array(
							'param_name'  => 'bgcolor',
							'heading'     => esc_html__( 'Background Color', 'furion' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'layout',
								'value'   => array( '1' )
							),
						),
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'title_link',
							'heading'     => esc_html__( 'Title link to', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'fontsize',
							'heading'     => esc_html__( 'Title Font Size', 'furion' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'text_transform',
							'heading' 	 => esc_html__( 'Text Transform', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Inherit', 'furion' )    => 'inherit',
								esc_html__( 'Uppercase', 'furion' )  => 'uppercase',
								esc_html__( 'Lowercase', 'furion' )  => 'lowercase',
								esc_html__( 'Initial', 'furion' )    => 'initial',
								esc_html__( 'Capitalize', 'furion' ) => 'capitalize',
							),
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Title Color', 'furion' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'title_margin_bottom',
							'heading'     => esc_html__( 'Title margin bottom', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
						),
						array(
							'param_name' => 'icon_type',
							'heading' 	 => esc_html__( 'Icon Type', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								'Icon Fonts' => 'icon_fonts',
								'Graphics'   => 'graphics',
							)
						),
						array(
							'param_name' => 'graphic',
							'heading' 	 => esc_html__( 'Choose Images', 'furion' ),
							'type' 		 => 'attach_image',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'graphics' )
							),
						),
						array(
							'param_name' => 'icon',
							'heading' 	 => esc_html__( 'Choose Icon', 'furion' ),
							'type' 		 => 'k2t_icon',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_fonts' )
							),
							'value'      => '',
						),
						array(
							'param_name'  => 'icon_font_size',
							'heading'     => esc_html__( 'Icon size', 'furion' ),
							'type'        => 'textfield',
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_fonts' )
							),
						),
						array(
							'param_name' => 'icon_color',
							'heading' 	 => esc_html__( 'Icon Color', 'furion' ),
							'type' 		 => 'colorpicker',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_fonts' )
							),
						),
						array(
							'param_name'  => 'link',
							'heading'     => esc_html__( 'Link to', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'link_text',
							'heading'     => esc_html__( 'Link text', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'Content', 'furion' ),
							'type'        => 'textarea_html',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_margin_top,
						$k2t_margin_right,
						$k2t_margin_bottom,
						$k2t_margin_left,
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_icon_box );
	
				/*  [ Icon List ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_icon_list = array(
					'base'            => 'iconlist',
					'name'            => esc_html__( 'Icon List', 'furion' ),
					'icon'            => 'fa fa-list',
					'category'        => esc_html__( 'Typography', 'furion' ),
					'as_parent'       => array( 'only' => 'li' ),
					'content_element' => true,
					'js_view'         => 'VcColumnView',
					'params'          => array(
						array(
							'param_name' => 'icon',
							'heading' 	 => esc_html__( 'Choose Icon', 'furion' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Icon Color', 'furion' ),
							'type'        => 'colorpicker',
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				$k2t_icon_list_item = array(
					'base'            => 'li',
					'name'            => esc_html__( 'Icon List', 'furion' ),
					'icon'            => 'fa fa-ellipsis-v',
					'category'        => esc_html__( 'Typography', 'furion' ),
					'as_child'        => array( 'only' => 'iconlist' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'icon',
							'heading' 	 => esc_html__( 'Choose Icon', 'furion' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
					)
				);
				vc_map( $k2t_icon_list );
				vc_map( $k2t_icon_list_item );
					
				/*  wrap member */

				$k2t_isotope = array(
					'base'            => 'isotope',
					'name'            => esc_html__( 'K2T Grid Member', 'furion' ),
					'icon'            => 'fa fa-exchange',
					'category'        => esc_html__( 'K2T FURION SHORTCODE', 'furion' ),
					'as_parent'       => array( 'only' => 'member,counter' ),
					'js_view'         => 'VcColumnView',
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'column',
							'heading'     => esc_html__( 'Number of columns', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array(
								esc_html__('2 column','k2t') => 'column-2',
								esc_html__('3 column','k2t') => 'column-3',
								esc_html__('4 column','k2t') => 'column-4',
								esc_html__('5 column','k2t') => 'column-5',
							),
						),
						array(
							'param_name'  => 'column_md',
							'heading'     => esc_html__( 'Number of columns on Medium device >= 992px', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array(
								esc_html__('2 column','k2t') => 'column-md-2',
								esc_html__('3 column','k2t') => 'column-md-3',
								esc_html__('4 column','k2t') => 'column-md-4',
								esc_html__('5 column','k2t') => 'column-md-5',
							),
						),
						array(
							'param_name'  => 'column_sm',
							'heading'     => esc_html__( 'Number of columns on Small device >= 768px', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array(
								esc_html__('2 column','k2t') => 'column-sm-2',
								esc_html__('3 column','k2t') => 'column-sm-3',
								esc_html__('4 column','k2t') => 'column-sm-4',
								esc_html__('5 column','k2t') => 'column-sm-5',
							),
						),
						array(
							'param_name'  => 'column_xs',
							'heading'     => esc_html__( 'Number of columns on Small device < 768px', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array(
								esc_html__('2 column','k2t') => 'column-xs-2',
								esc_html__('3 column','k2t') => 'column-xs-3',
								esc_html__('4 column','k2t') => 'column-xs-4',
								esc_html__('5 column','k2t') => 'column-xs-5',
							),
						),
					),
				);

				vc_map( $k2t_isotope );

				/*  [ Member ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_member = array(
					'base'            => 'member',
					'name'            => esc_html__( 'k2T Member Item', 'furion' ),
					'icon'            => 'fa fa-user',
					'as_child' 		  => 'isotope',
					'category'        => esc_html__( 'K2T FURION SHORTCODE', 'furion' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name' => 'style',
							'heading' 	 => esc_html__( 'Style Member', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Default', 'furion' ) => 'default',
								esc_html__( 'Boxed', 'furion' ) => 'boxed',
								esc_html__( 'Zoom', 'furion' )  => 'zoom',
								esc_html__( 'slice', 'furion' )  => 'slice',
							)
						),
						array(
							'param_name'  => 'image',
							'heading'     => esc_html__( 'Member Avatar', 'furion' ),
							'type'        => 'attach_image',
							'holder'      => 'div',
						),
						array(
							'param_name' => 'size',
							'heading' 	 => esc_html__( 'Image size', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => get_intermediate_image_sizes(),
						),
						array(
							'param_name'  => 'name',
							'heading'     => esc_html__( 'Member Name', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'role',
							'heading'     => esc_html__( 'Role', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'facebook',
							'heading'     => esc_html__( 'Facebook URL', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'twitter',
							'heading'     => esc_html__( 'Twitter URL', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'skype',
							'heading'     => esc_html__( 'Skype', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'pinterest',
							'heading'     => esc_html__( 'Pinterest URL', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'instagram',
							'heading'     => esc_html__( 'Instagram', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'dribbble',
							'heading'     => esc_html__( 'Dribbble URL', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'google_plus',
							'heading'     => esc_html__( 'Google Plus URL', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'Member Info', 'furion' ),
							'type'        => 'textarea_html',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_member );
	
				/*  [ Pricing Table ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_pricing = array(
					'base'            => 'pricing',
					'name'            => esc_html__( 'Pricing Table', 'furion' ),
					'icon'            => 'fa fa-table',
					'category'        => esc_html__( 'Marketing', 'furion' ),
					'as_parent'       => array( 'only' => 'pricing_column' ),
					'content_element' => true,
					'js_view'         => 'VcColumnView',
					'params'          => array(
						array(
							'param_name' => 'separated',
							'heading' 	 => esc_html__( 'Separated', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'True', 'furion' )  => 'true',
								esc_html__( 'False', 'furion' ) => 'false',
							)
						),
						$k2t_id,
						$k2t_class
					)
				);
				$k2t_pricing_item = array(
					'base'            => 'pricing_column',
					'name'            => esc_html__( 'Pricing Columns', 'furion' ),
					'icon'            => 'fa fa-table',
					'category'        => esc_html__( 'Marketing', 'furion' ),
					'as_child'        => array( 'only' => 'pricing' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'sub_title',
							'heading'     => esc_html__( 'Sub Title', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'image',
							'heading'     => esc_html__( 'Pricing Image', 'furion' ),
							'type'        => 'attach_image',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'price',
							'heading'     => esc_html__( 'Price', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'price_per',
							'heading'     => esc_html__( 'Price Per', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'unit',
							'heading'     => esc_html__( 'Unit', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'link',
							'heading'     => esc_html__( 'Link to', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'link_text',
							'heading'     => esc_html__( 'Link Text', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'target',
							'heading' 	 => esc_html__( 'Link Target', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Open in a new window', 'furion' )                      => '_blank',
								esc_html__( 'Open in the same frame as it was clicked', 'furion' )  => '_self'
							),
						),
						array(
							'param_name' => 'featured',
							'heading' 	 => esc_html__( 'Featured', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'False', 'furion' ) => 'false',
								esc_html__( 'True', 'furion' )  => 'true',
							)
						),
						array(
							'param_name'  => 'pricing_content',
							'heading'     => esc_html__( 'List Item', 'furion' ),
							'description' => esc_html__( 'Using ul li tag.', 'furion' ),
							'type'        => 'textarea_html',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_pricing );
				vc_map( $k2t_pricing_item );
	
				/*  [ Progress ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_progress = array(
					'base'            => 'progress',
					'name'            => esc_html__( 'Progress', 'furion' ),
					'icon'            => 'fa fa-sliders',
					'category'        => esc_html__( 'Common', 'furion' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'percent',
							'heading'     => esc_html__( 'Percent', 'furion' ),
							'description' => esc_html__( 'Numeric value only, between 1-100.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Color', 'furion' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'background_color',
							'heading'     => esc_html__( 'Background Color', 'furion' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'text_color',
							'heading'     => esc_html__( 'Text Color', 'furion' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'height',
							'heading'     => esc_html__( 'Height', 'furion' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'striped',
							'heading'     => esc_html__( 'Striped', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						),
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_progress );
	
				/*  [ Responsive Text ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_responsive_text = array(
					'base'            => 'responsive_text',
					'name'            => esc_html__( 'Responsive text', 'furion' ),
					'icon'            => 'fa fa-arrows-alt',
					'category'        => esc_html__( 'Typography', 'furion' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'compression',
							'heading'     => esc_html__( 'Compression', 'furion' ),
							'description' => esc_html__( 'Numeric value only.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'min_size',
							'heading'     => esc_html__( 'Min Font Size', 'furion' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'max_size',
							'heading'     => esc_html__( 'Max Font Size', 'furion' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_responsive_text );
	
				/*  [ Testimonial ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_textimonial = array(
					'base'            => 'testimonial',
					'name'            => esc_html__( 'testimonials', 'furion' ),
					'icon'            => 'fa fa-exchange',
					'category'        => esc_html__( 'Content', 'furion' ),
					'as_parent'       => array( 'only' => 'testi' ),
					'js_view'         => 'VcColumnView',
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'align',
							'heading'     => esc_html__( 'Name', 'furion' ),
							'type'        => 'dropdown',
							'value'		  => array(
								esc_html__( 'Center', 'furion' )	=>	'align-center',
								esc_html__( 'Left', 'furion' )		=>	'align-left',
								esc_html__( 'Right', 'furion' )		=>	'align-right',
							),
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Text Color', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'colorpicker',
							'holder'      => 'div'
						),
						$k2t_class
					)
				);
				vc_map( $k2t_textimonial );
				$k2t_testi = array(
					'base'            => 'testi',
					'name'            => esc_html__( 'Testimonial', 'furion' ),
					'icon'            => 'fa fa-comments-o',
					'category'        => esc_html__( 'Marketing', 'furion' ),
					'as_child'        => array( 'only' => 'testimonial' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'image',
							'heading'     => esc_html__( 'Avatar', 'furion' ),
							'description' => esc_html__( 'Choose avatar for testimonial author.', 'furion' ),
							'type'        => 'attach_image',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'name',
							'heading'     => esc_html__( 'Name', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						)
						,array(
							'param_name'  => 'job',
							'heading'     => esc_html__( 'Job', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'Text', 'furion' ),
							'description' => esc_html__( 'Enter your testimonial.', 'furion' ),
							'type'        => 'textarea_html',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_testi );
	
				/*  [ Blockquote ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_blockquote = array(
					'base'            => 'blockquote',
					'name'            => esc_html__( 'Blockquote', 'furion' ),
					'icon'            => 'fa fa-quote-left',
					'category'        => esc_html__( 'Typography', 'furion' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name' => 'style',
							'heading' 	 => esc_html__( 'Style', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Style 1', 'furion' )   => '1',
								esc_html__( 'Style 2', 'furion' )   => '2',
							),
						),
						array(
							'param_name' => 'align',
							'heading' 	 => esc_html__( 'Align', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Left', 'furion' )   => 'left',
								esc_html__( 'Center', 'furion' ) => 'center',
								esc_html__( 'Right', 'furion' )  => 'right'
							),
						),
						array(
							'param_name'  => 'author',
							'heading'     => esc_html__( 'Author', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'link_author',
							'heading'     => esc_html__( 'Link to', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'Content', 'furion' ),
							'type'        => 'textarea_html',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_blockquote );
	
				/*  [ Countdown ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_countdown = array(
					'base'            => 'countdown',
					'name'            => esc_html__( 'Countdown', 'furion' ),
					'icon'            => 'fa fa-sort-numeric-desc',
					'category'        => esc_html__( 'Common', 'furion' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'time',
							'heading'     => esc_html__( 'Time', 'furion' ),
							'description' => esc_html__( 'The time in this format: m/d/y h:mm tt', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'align',
							'heading' 	 => esc_html__( 'Align', 'furion' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Left', 'furion' )   => 'left',
								esc_html__( 'Center', 'furion' ) => 'center',
								esc_html__( 'Right', 'furion' )  => 'right'
							),
						),
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_countdown );
	
				/*  [ Embed ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_embed = array(
					'base'            => 'k2t_embed',
					'name'            => esc_html__( 'Embed', 'furion' ),
					'icon'            => 'fa fa-terminal',
					'category'        => esc_html__( 'Media', 'furion' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'width',
							'heading'     => esc_html__( 'Width', 'furion' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'URL or embed code', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_embed );

				/*  [ page navigation ]
				- - - - - - - - - - - - - - - - - - - */

				$k2t_page_nav = array(
					'base'            => 'page_nav',
					'name'            => esc_html__( 'Page navigation', 'furion' ),
					'icon'            => 'fa fa-terminal',
					'category'        => esc_html__( 'K2T FURION SHORTCODE', 'furion' ),
					'content_element' => true,
					'is_container' => true,
					'as_parent'       => array( 'only' => 'page_nav_item' ),
					'js_view'         => 'VcColumnView',
					'params'          => array(
						array(
							'param_name'  => 'style',
							'heading'     => esc_html__( 'Style', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'value'		  => array(
								esc_html__( 'Default', 'furion' ) => 'default',
							),
						),
						array(
							'param_name'  => 'right',
							'heading'     => esc_html__( 'position right', 'furion' ),
							'description' => esc_html__( 'unit px like: 20px', 'furion' ),
							'type'        => 'textfield',
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_page_nav );


				/*  [ page navigation item ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_page_nav_item = array(
					'base'            => 'page_nav_item',
					'name'            => esc_html__( 'Page navigation item', 'furion' ),
					'icon'            => 'fa fa-terminal',
					'category'        => esc_html__( 'K2T FURION SHORTCODE', 'furion' ),
					'as_child'		  => array( 'only' => 'page_nav' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'id_link',
							'heading'     => esc_html__( 'Id of section this item link to.', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'textfield',
						),
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_page_nav_item );

				/*  [ k2t divider ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_divider = array(
					'base'            => 'k2t_divider',
					'name'            => esc_html__( 'k2t divider', 'furion' ),
					'icon'            => 'fa fa-terminal',
					'category'        => esc_html__( 'K2T FURION SHORTCODE', 'furion' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'direction',
							'heading'     => esc_html__( 'Direction', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value'		  => array(
								esc_html__( 'Vertical', 'furion' ) => 'd-vertical',
								esc_html__( 'Horizontal', 'furion' ) => 'd-horizontal',
							),
						),
						array(
							'param_name'  => 'style',
							'heading'     => esc_html__( 'Style', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'value'		  => array(
								esc_html__( 'Solid', 'furion' )  => 'solid',
								esc_html__( 'Dotted', 'furion' )  => 'dotted',
								esc_html__( 'Dashed', 'furion' )  => 'dashed',
								esc_html__( 'Double', 'furion' )  => 'double',
								esc_html__( 'Groover', 'furion' )  => 'groove',
								esc_html__( 'Inset', 'furion' )  => 'inset',
								esc_html__( 'Outset', 'furion' )  => 'outset',
							),
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'width',
							'heading'     => esc_html__( 'Width', 'furion' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'height',
							'heading'     => esc_html__( 'Height', 'furion' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Color', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'colorpicker',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'align',
							'heading'     => esc_html__( 'Align', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value'		  => array(
								esc_html__( 'Left', 'furion' ) => 'align-left',
								esc_html__( 'Right', 'furion' ) => 'align-right',
								esc_html__( 'Center', 'furion' ) => 'align-center',
							),
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class,
					)
				);
				vc_map( $k2t_divider );
	
				/*  [ K2T Slider ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_slider = array(
					'base'            => 'k2t_slider',
					'name'            => esc_html__( 'K2T Carousel', 'furion' ),
					'icon'            => 'fa fa-exchange',
					'category'        => esc_html__( 'Content', 'furion' ),
					'as_parent'       => array( 'only' => 'vc_single_image, vc_raw_html, event, member' ),
					'js_view'         => 'VcColumnView',
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'items',
							'heading'     => esc_html__( 'Slides per view', 'furion' ),
							'description' => esc_html__( 'Numeric value only.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'items_desktop',
							'heading'     => esc_html__( 'Slides per view on desktop', 'furion' ),
							'description' => esc_html__( 'Item to display for desktop small (device width <= 1200px).', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'items_tablet',
							'heading'     => esc_html__( 'Slides per view on tablet', 'furion' ),
							'description' => esc_html__( 'Item to display for tablet (device width <= 768px).', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'items_mobile',
							'heading'     => esc_html__( 'Slides per view on mobile', 'furion' ),
							'description' => esc_html__( 'Item to display for mobile (device width <= 480px).', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'item_margin',
							'heading'     => esc_html__( 'Margin between items', 'furion' ),
							'description' => esc_html__( 'Ex: 30', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'auto_play',
							'heading'     => esc_html__( 'Auto Play', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => true
							),
						),
						array(
							'param_name'  => 'navigation',
							'heading'     => esc_html__( 'Navigation', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => true
							),
						),
						array(
							'param_name'  => 'pagination',
							'heading'     => esc_html__( 'Pagination', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => true
							),
						),
						$k2t_class
					)
				);
				vc_map( $k2t_slider );

				/*  [ Blog Post ]
				- - - - - - - - - - - - - - - - - - - */

				$k2t_social = array(
					'base'            => 'k2t_social',
					'name'            => esc_html__( 'K2t_social', 'furion' ),
					'icon'            => 'fa fa-terminal',
					'category'        => esc_html__( 'K2T FURION SHORTCODE', 'furion' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'align',
							'heading'     => esc_html__( 'align', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value'      => array(
								esc_html__( 'Left', 'furion' ) 	=> 'left',
								esc_html__( 'Right', 'furion' )    => 'right',
								esc_html__( 'Center', 'furion' ) 	=> 'center',
							),
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_social );
	
				/*  [ Blog Post ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_blog_post = array(
					'base'            => 'blog_post',
					'name'            => esc_html__( 'K2t Blog Post', 'furion' ),
					'icon'            => 'fa fa-file-text',
					'category'        => array( esc_html__( 'Content', 'furion' ), esc_html__( 'K2T FURION SHORTCODE', 'furion' ) ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'style',
							'heading'     => esc_html__( 'Style', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array( 
								esc_html__( 'blog time', 'furion' ) => 'Time-line',
								esc_html__( 'Blog 2', 'furion' ) => 'blog-2',
								esc_html__( 'Masonry', 'furion' ) => 'blog-masonry',
								esc_html__( 'Grid', 'furion' ) => 'blog-grid',
								esc_html__( 'Large', 'furion' ) => 'blog-large',
								esc_html__( 'Medium', 'furion' ) => 'blog-medium',
								esc_html__( 'Slider', 'furion' ) => 'blog-slider',
							),
						),
						array(
							'param_name'  => 'column',
							'heading'     => esc_html__( 'Column', 'furion' ),
							'description' => esc_html__( '', 'furion' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array( 
								esc_html__( '2 column', 'furion' ) => '2',
								esc_html__( '3 column', 'furion' ) => '3',
								esc_html__( '4 column', 'furion' ) => '4',
								esc_html__( '5 column', 'furion' ) => '5',
							),
							'dependency' => array(
								'element' => 'style',
								'value'   => array( 'blog-grid', 'blog-masonry' ),
							),
						),
						array(
							'param_name'  => 'limit',
							'heading'     => esc_html__( 'Number of posts to show', 'furion' ),
							'description' => esc_html__( 'Empty is show all posts.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'cat',
							'heading'     => esc_html__( 'Show posts associated with certain categories', 'furion' ),
							'description' => esc_html__( 'Using category id, separate multiple categories with commas.', 'furion' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'navigation',
							'heading'     => esc_html__( 'Navigation', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'style',
								'value'   => array( 'blog-slider' ),
							),
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'pagination',
							'heading'     => esc_html__( 'Pagination', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'style',
								'value'   => array( 'blog-slider' ),
							),
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'auto_play',
							'heading'     => esc_html__( 'Auto Play', 'furion' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'style',
								'value'   => array( 'blog-slider' ),
							),
							'value'       => array(
								'' => 'true'
							)
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_blog_post );

				/*  [ K2t Event ]
				- - - - - - - - - - - - - - - - - - - */
				if ( is_plugin_active( 'k-event/hooks.php' ) ) {
					$k2t_event_listing = array(
						'base'            => 'k_event_listing',
						'name'            => esc_html__( 'K Event Listing', 'furion' ),
						'icon'            => 'fa fa-calendar',
						'category'        => esc_html__( 'Furion Shortcode', 'furion' ),
						'content_element' => true,
						'params'          => array(
							array(
								'param_name'  => 'style',
								'heading'     => esc_html__( 'Event Listing Style', 'furion' ),
								'type'        => 'dropdown',
								'value'      => array(
									esc_html__('Calendar 1', 'k2t') 	=> 'style-1',
									esc_html__('Calendar 2', 'k2t') 	=> 'style-2',
									esc_html__('Grid', 'k2t') 			=> 'style-3',
									esc_html__('Classic', 'k2t') 		=> 'style-4',
									esc_html__('Carousel', 'k2t') 		=> 'style-5',
								),
								'holder'      => 'div'
							),
							array(
								'param_name'  => 'masonry_column',
								'heading'     => esc_html__( 'Masonry Columns', 'furion' ),
								'type'        => 'dropdown',
								'value'      => array(
									esc_html__('2 Columns', 'k2t') => 'columns-2',
									esc_html__('3 Columns', 'k2t') => 'columns-3',
									esc_html__('4 Columns', 'k2t') => 'columns-4',
								),
								'holder'      => 'div',
								'dependency' => array(
									'element' => 'style',
									'value'   => array( 'style-3', 'style-5' ),
								),
							),
							array(
								'param_name'  => 'event_masonry_filter',
								'heading'     => esc_html__( 'Show/Hide Filter', 'furion' ),
								'type'        => 'dropdown',
								'value'      => array(
									esc_html__('Show', 'k2t') => 'show',
									esc_html__('Hide', 'k2t') => 'hide',
								),
								'holder'      => 'div',
								'dependency'  => array(
									'element' => 'style',
									'value'   => array( 'style-3'),
								),
							),
							array(
								'param_name'  => 'post_per_page',
								'heading'     => esc_html__( 'Event per page', 'furion' ),
								'type'        => 'textfield',
								'holder'      => 'div',
								'std'         => '4',
								'dependency'  => array(
									'element' => 'style',
									'value'   => array( 'style-3', 'style-4' ),
								),
							),
							array(
								'param_name'  => 'number_post_show',
								'heading'     => esc_html__( 'Number of post show', 'furion' ),
								'type'        => 'textfield',
								'holder'      => 'div',
								'std'         => '9',
								'dependency'  => array(
									'element' => 'style',
									'value'   => array( 'style-5' ),
								),
							),
							array(
								'param_name'  => 'event_pagination',
								'heading'     => esc_html__( 'Show/Hide Pagination', 'furion' ),
								'type'        => 'dropdown',
								'value'      => array(
									esc_html__('Show', 'k2t') => 'show',
									esc_html__('Hide', 'k2t') => 'hide',
								),
								'holder'      => 'div',
								'dependency'  => array(
									'element' => 'style',
									'value'   => array( 'style-3', 'style-4', 'style-5' ),
								),
							),
							array(
								'param_name'  => 'event_navigation',
								'heading'     => esc_html__( 'Show/Hide Navigation', 'furion' ),
								'type'        => 'dropdown',
								'value'      => array(
									esc_html__('Show', 'k2t') => 'show',
									esc_html__('Hide', 'k2t') => 'hide',
								),
								'holder'      => 'div',
								'dependency'  => array(
									'element' => 'style',
									'value'   => array( 'style-5' ),
								),
							),
							$k2t_animation,
							$k2t_animation_name,
							$k2t_animation_delay,
							$k2t_id,
							$k2t_class
						)
					);
					vc_map( $k2t_event_listing );
				}

			}
	
			add_action( 'admin_init', 'k2t_furion_vc_map_shortcodes' );
	
			/*  [ Extend container class (parents) ]
			- - - - - - - - - - - - - - - - - - - - - - - - - */
			class WPBakeryShortCode_Accordion extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_Brands extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_Iconlist extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_Pricing extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_Sticky_Tab extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_K2t_Slider extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_isotope extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_testimonial extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_K2t_Furion_Slider extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_Page_Nav extends WPBakeryShortCodesContainer {}
		
			/*  [ Extend shortcode class (children) ]
			- - - - - - - - - - - - - - - - - - - - - - - - - */
			class WPBakeryShortCode_Toggle extends WPBakeryShortCode {}
			class WPBakeryShortCode_Brand extends WPBakeryShortCode {}
			class WPBakeryShortCode_Li extends WPBakeryShortCode {}
			class WPBakeryShortCode_Pricing_Column extends WPBakeryShortCode {}
			class WPBakeryShortCode_Step extends WPBakeryShortCode {}
			class WPBakeryShortCode_Tab extends WPBakeryShortCode {}
			class WPBakeryShortCode_K2t_Furion_Slide extends WPBakeryShortCode {}
			class WPBakeryShortCode_Page_Nav_Item extends WPBakeryShortCode {}

		endif;
	endif;
	
}
