<?php

global $GLOBALS; 

// Disable some theme options by id if not required
if( ! function_exists( 'k2t_disable_of_options' ) ){
	function k2t_disable_of_options($options = array()){
		global $of_options, $of_options_disable;
		$of_options_disable = array(
		);
		
		if( !empty($options) ){
			$options =& $of_options;
		}
		
		foreach( $of_options_disable as $op_id ){
			if( is_string( $op_id ) && isset( $of_options[$op_id] ) ){
				$of_options[$op_id]['disable'] = true;
			}
		}
		
		return $options;
	}
	
	add_filter('optionsframework_machine_before', 'k2t_disable_of_options');
}

if ( ! function_exists( 'k2t_render_titlebar_options' ) ) {
	function k2t_render_titlebar_options( $pre, $of_options ) {
		/* Titlebar */
		$of_options[] = array( 'name' => esc_html__( 'Titlebar', 'furion' ),
			'type' => 'info',
			'std'  => esc_html__( 'Titlebar', 'furion' ),
		);

		$of_options[] = array( 'name' => esc_html__( 'Show / Hide titlebar', 'furion' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . '-display-titlebar',
			'k2t_logictic' => array(
				'0' => array( ),
				'1' => array(  $pre . '-titlebar-font-size', $pre . '-titlebar-color', $pre . '-pading-top', $pre . '-pading-bottom', $pre . '-background-color', $pre . '-background-image', $pre . '-background-position', $pre . '-background-size', $pre . '-background-repeat', $pre . '-background-parallax', $pre . '-titlebar-overlay-opacity', $pre . '-titlebar-clipmask-opacity', $pre . '-titlebar-custom-content',  ),
			),
		);

		$of_options[] = array( 'name' => esc_html__( 'Titlebar title font size', 'furion' ),
			'type' => 'text',
			'std'  => '',
			'id'   => $pre . '-titlebar-font-size',
			'desc' => esc_html__( 'Unit: px.', 'furion' ),
		);

		$of_options[] = array( 'name' => esc_html__( 'Titlebar title color', 'furion' ),
			'type' => 'color',
			'std'  => '',
			'id'   => $pre . '-titlebar-color',
		);

		$of_options[] = array( 'name' => esc_html__( 'Padding top', 'furion' ),
			'type' => 'text',
			'std'  => '',
			'id'   => $pre . '-pading-top',
			'desc' => esc_html__( 'Unit: px. Eg: 10px;', 'furion' ),
		);

		$of_options[] = array( 'name' => esc_html__( 'Padding bottom', 'furion' ),
			'type' => 'text',
			'std'  => '',
			'id'   => $pre . '-pading-bottom',
			'desc' => esc_html__( 'Unit: px. Eg: 10px;', 'furion' ),
		);

		$of_options[] = array( 'name' => esc_html__( 'Background color', 'furion' ),
			'type' => 'color',
			'std'  => '',
			'id'   => $pre . '-background-color',
		);

		$of_options[] = array( 'name' => esc_html__( 'Background image', 'furion' ),
			'type' => 'media',
			'std'  => '',
			'id'   => $pre . '-background-image',
		);

		$of_options[] = array( 'name' => esc_html__( 'Background position', 'furion' ),
			'type' => 'select',
			'std'  => 'left',
			'options' => array(
				'left top'      => esc_html__( 'Left Top', 'furion' ),
				'left center'   => esc_html__( 'Left Center', 'furion' ),
				'left bottom'   => esc_html__( 'Left Bottom', 'furion' ),
				'right top'     => esc_html__( 'Right Top', 'furion' ),
				'right center'  => esc_html__( 'Right Center', 'furion' ),
				'right bottom'  => esc_html__( 'Right Bottom', 'furion' ),
				'center top'    => esc_html__( 'Center Top', 'furion' ),
				'center center' => esc_html__( 'Center Center', 'furion' ),
				'center bottom' => esc_html__( 'Center Bottom', 'furion' ),
			),
			'id'   => $pre . '-background-position',
		);

		$of_options[] = array( 'name' => esc_html__( 'Background size', 'furion' ),
			'type' => 'select',
			'std'  => 'inherit',
			'options' => array(
				'inherit' 		=> esc_html__( 'Inherit', 'furion' ),
				'cover'    		=> esc_html__( 'Cover', 'furion' ),
				'contain'  		=> esc_html__( 'Contain', 'furion' ),
				'full'  		=> esc_html__( '100%', 'furion' ),
			),
			'id'   => $pre . '-background-size',
		);

		$of_options[] = array( 'name' => esc_html__( 'Background repeat', 'furion' ),
			'type' => 'select',
			'std'  => 'repeat',
			'options' => array(
				'no-repeat' => esc_html__( 'No Repeat', 'furion' ),
				'repeat'    => esc_html__( 'Repeat', 'furion' ),
				'repeat-x'  => esc_html__( 'Repeat X', 'furion' ),
				'repeat-y'  => esc_html__( 'Repeat Y', 'furion' ),
			),
			'id'   => $pre . '-background-repeat',
		);

		$of_options[] = array( 'name' => esc_html__( 'Background parallax', 'furion' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . '-background-parallax',
		);

		$of_options[] = array( 'name' => esc_html__( 'Titlebar overlay opacity', 'furion' ),
			'type' => 'sliderui',
			'min'  => 0,
			'max'  => 10,
			'std'  => 0,
			'step' => 1,
			'id'   => $pre . '-titlebar-overlay-opacity',
		);

		$of_options[] = array( 'name' => esc_html__( 'Titlebar clipmask opacity', 'furion' ),
			'type' => 'sliderui',
			'min'  => 0,
			'max'  => 10,
			'std'  => 0,
			'step' => 1,
			'id'   => $pre . '-titlebar-clipmask-opacity',
		);

		$of_options[] = array( 'name' => esc_html__( 'Titlebar custom content', 'furion' ),
			'type' => 'textarea',
			'std'  => '',
			'id'   => $pre . '-titlebar-custom-content',
		);
		return $of_options;
	}
}

if ( ! function_exists( 'k2t_render_header_style' ) ) {
	function k2t_render_header_style( $pre, $of_options ) {
		
		$of_options[] = array( 'name' => esc_html__( 'Fixed header?', 'furion' ),
		 	'type' => 'switch',
		 	'std'  => false,
		 	'id'   => $pre . 'fixed-header',
		 	'desc' => esc_html__( 'If the setting is enabled, the body section will be displayed under the header. If the setting is disabled, the body section will be displayed above the header. In the 2nd case, you can make the header transparent to create a very nice style', 'furion' ),
		 	'k2t_logictic' => array(
				'0' => array( $pre . 'sticky-menu', $pre . 'smart-sticky', $pre . 'absolute' ),
				'1' => array(),
			),		
		 );
		$of_options[] = array( 'name' => esc_html__( 'Absolute header?', 'furion' ),
		 	'type' => 'switch',
		 	'std'  => false,
		 	'id'   => $pre . 'absolute',
		 	'desc' => esc_html__( 'absolute on top of below section', 'furion' ),
		);
		$of_options[] = array( 'name' => esc_html__( 'Sticky menu?', 'furion' ),
			'type' => 'select',
			'options' => array(
				''        => esc_html__( 'None', 'furion' ),
				'sticky_top' => esc_html__( 'Sticky menu on top header', 'furion' ),
				'sticky_mid' => esc_html__( 'Sticky menu on middle header', 'furion' ),
				'sticky_bot' => esc_html__( 'Sticky menu on bottom header', 'furion' ),
			),
			'std'  => '',
			'id'   => $pre . 'sticky-menu',
			'desc' => esc_html__( 'Enable this setting so that the header section and menus inlcuded in the header are sticky', 'furion' ),
		);		

		$of_options[] = array(  'name' => esc_html__( 'Smart sticky menu', 'furion' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . 'smart-sticky',
			'desc' => esc_html__( 'Turn ON to enable sticky menu, it will always stay in your page when scrolling to top and disappear when scrolling down.', 'furion' ),
		);


		$of_options[] = array( 'name' => esc_html__( 'Header Layouts', 'furion' ),
			'type' => 'info',
			'std'  => esc_html__( 'Header Layouts', 'furion' ),
			'id'   => $pre . 'header-layouts',
		);

		$of_options[] = array(  'name' => esc_html__( 'Max width of header layout', 'furion' ),
			'type' => 'text',
			'std'  => '1660px',
			'id'   => $pre . 'max-width-header',
			'desc' => esc_html__( 'Set max width for full-with like : 1660px or 100%', 'furion' ),
		);

		$of_options[] = array(  'name' => esc_html__( 'Fullwidth header layout', 'furion' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . 'full-header',
			'desc' => esc_html__( 'Turn it ON if you want to set full width header.', 'furion' ),
		);

		/* Visual Header */
		$of_options[] = array( 'name' => esc_html__( 'Top Header Section', 'furion' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . 'use-top-header',
			'desc' => esc_html__( 'Show or hide top header layout.', 'furion' ),
			'k2t_logictic' => array(
				'0' => array( ),
				'1' => array( $pre . 'header_section_1' ),
			),			
		);

		$of_options[] = array(
			'type' => 'k2t_header_option',
			'std'  => '',
			'id'   => $pre . 'header_section_1',
			'desc' => '',
			
		);

		$of_options[] = array( 'name' => esc_html__( 'Middle Header Section', 'furion' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'use-mid-header',
			'desc' => esc_html__( 'Show or hide middle header layout.', 'furion' ),
			'k2t_logictic' => array(
				'0' => array( ),
				'1' => array( $pre . 'header_section_2' ),
			),		
		);

		$of_options[] = array(
			'type' => 'k2t_header_option',
			'std'  => '{"name":"header_section_2","setting":{"bg_image":"","bg_color":"","opacity":"","fixed_abs":"fixed","custom_css":""},"columns_num":2,"htmlData":"","columns":[{"id":1,"percent":"2","value":[{"id":"1425696862388","type":"logo","value":{"custom_class":"","custom_id":""}}]},{"id":2,"value":[{"id":"1434273437540","type":"custom_menu","value":{"menu_id":"QWxsIFBhZ2Vz","custom_class":"","custom_id":""}}],"percent":"10"}]}',
			'id'   => $pre . 'header_section_2',
			'desc' => '',
		);

		$of_options[] = array( 'name' => esc_html__( 'Bottom Header Section', 'furion' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . 'use-bot-header',
			'desc' => esc_html__( 'Show or hide middle header layout.', 'furion' ),
			'k2t_logictic' => array(
				'0' => array( ),
				'1' => array( $pre . 'header_section_3' ),
			),		
		);

		$of_options[] = array(
			'type' => 'k2t_header_option',
			'std'  => '',
			'id'   => $pre . 'header_section_3',
			'desc' => '',
		);

		/* Logo */
		$of_options[] = array( 'name' => esc_html__( 'Logo', 'furion' ),
			'type' => 'info',
			'std'  => esc_html__( 'Logo', 'furion' ),
			'id'   => $pre . 'header-logo',
		);

		$of_options[] = array( 'name' => esc_html__( 'Use text logo', 'furion' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . 'use-text-logo',
			'desc' => esc_html__( 'Turn it ON if you want to use text logo instead of image logo.', 'furion' ),
			'logicstic' => true,
		);

		$of_options[] = array( 'name' => esc_html__( 'Text logo', 'furion' ),
			'type' => 'text',
			'std'  => '',
			'id'   => $pre . 'text-logo',
			'desc' => '',
			'conditional_logic' => array(
				$pre .'field'    => 'use-text-logo',
				$pre .'value'    => 'switch-1',
			),
		);

		$of_options[] = array( 'name' => esc_html__( 'Logo', 'furion' ),
			'type' => 'media',
			'id'   => $pre . 'logo',
			'std'  => '',
			'desc' => esc_html__( 'The logo size in our demo is 116x33px. Please use jpg, jpeg, png or gif image for best performance.', 'furion' ),
		);

		$of_options[] = array( 'name' => esc_html__( 'Retina logo', 'furion' ),
			'type' => 'media',
			'id'   => $pre . 'retina-logo',
			'std'  => '',
			'desc' => esc_html__( '2x times your logo dimension.', 'furion' ),
		);

		$of_options[] = array( 'name' => esc_html__( 'Logo margin top (px)', 'furion' ),
			'type' => 'sliderui',
			'id'   => $pre . 'logo-margin-top',
			'min'  => 0,
			'max'  => 200,
			'step' => 1,
			'std'  => 0,
		);

		$of_options[] = array( 'name' => esc_html__( 'Logo margin right (px)', 'furion' ),
			'type' => 'sliderui',
			'id'   => $pre . 'logo-margin-right',
			'min'  => 0,
			'max'  => 200,
			'step' => 1,
			'std'  => 0,
		);

		$of_options[] = array( 'name' => esc_html__( 'Logo margin bottom (px)', 'furion' ),
			'type' => 'sliderui',
			'id'   => $pre . 'logo-margin-bottom',
			'min'  => 0,
			'max'  => 200,
			'step' => 1,
			'std'  => 0,
		);

		$of_options[] = array( 'name' => esc_html__( 'Logo margin left (px)', 'furion' ),
			'type' => 'sliderui',
			'id'   => $pre . 'logo-margin-left',
			'min'  => 0,
			'max'  => 200,
			'step' => 1,
			'std'  => 0,
		);
		return $of_options;
	}
}

// Social Share Function
if ( ! function_exists( 'k2t_furion_social_share_options' ) ) {
	function k2t_furion_social_share_options( $pre, $of_options ) {
		/* Titlebar */
		$of_options[] = array( 'name' => esc_html__( 'Show Facebook Share', 'furion' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'social-share-facebook',
		);
		$of_options[] = array( 'name' => esc_html__( 'Show Twitter Share', 'furion' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'social-share-twitter',
		);
		$of_options[] = array( 'name' => esc_html__( 'Show Google Plus Share', 'furion' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'social-share-google',
		);
		$of_options[] = array( 'name' => esc_html__( 'Show Linkedin Share', 'furion' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'social-share-linkedin',
		);
		$of_options[] = array( 'name' => esc_html__( 'Show Tumblr Share', 'furion' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'social-share-tumblr',
		);
		$of_options[] = array( 'name' => esc_html__( 'Show Email Share', 'furion' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . 'social-share-email',
		);
		return $of_options;
	}
}



// Add some theme options if needs
if( ! function_exists('k2t_add_of_options') ){
	function k2t_add_of_options($options = array()){
		$add_options = array(
			// option array to add here
		);
		
		foreach($add_options as $option){
			if( !empty($option['id']) && !isset($options[$option['id']]) )
				$options[$option['id']] = $option;
			else{
				$options[] = $option;
			}
		}

		return $options;
	}
	
	add_filter('optionsframework_machine_before', 'k2t_add_of_options', 9);
}

//================= Register theme option's fields =======================

/*-----------------------------------------------------------------------------------*/
/* General */
/*-----------------------------------------------------------------------------------*/

$of_options = array(
	array( 'name' => esc_html__( 'General', 'furion' ),
		'type' => 'heading',
		'icon' => ADMIN_IMAGES . 'icon-settings.png'
	)
);

$of_options[] = array( 'name' => esc_html__( 'General', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'General', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide breadcrumb', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'breadcrumb',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide page loader', 'furion' ),
	'type' => 'switch',
	'std'  => false,
	'id'   => 'pageloader',
	'desc' => '',
);
$of_options[] = array( 'name' => esc_html__( 'Show/Hide place holder', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'place_holder',
	'desc' => '',
);
$of_options[] = array( 'name' => esc_html__( 'Sidebar width', 'furion' ),
	'type' => 'sliderui',
	'id'   => 'sidebar_width',
	'min'  => 0,
	'max'  => 100,
	'step' => 1,
	'std'  => 25,
	'desc' => esc_html__( 'Unit: %', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Header Code', 'furion' ),
	'type' => 'textarea',
	'std'  => '',
	'id'   => 'header_code',
	'desc' => esc_html__( 'You can load Google fonts here.', 'furion' ),
	'is_js_editor' => '1'
);

$of_options[] = array( 'name' => esc_html__( 'Footer Code', 'furion' ),
	'type' => 'textarea',
	'std'  => '',
	'id'   => 'footer_code',
	'desc' => esc_html__( 'You can fill Google Analytics tracking code or something here.', 'furion' ),
	'is_js_editor' => '1'
);

$of_options[] = array( 'name' => esc_html__( 'Custom CSS', 'furion' ),
	'type' => 'textarea',
	'std'  => '',
	'id'   => 'custom_css',
	'desc' => esc_html__( 'If you know a little about CSS, you can write your custom CSS here. Do not edit CSS files (it will be lost when you update this theme).', 'furion' ),
	'is_css_editor' => '1'
);

/* Icons */
$of_options[] = array( 'name' => esc_html__( 'Icons', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Icons', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Favicon', 'furion' ),
	'type' => 'media',
	'id'   => 'favicon',
	'std'  => '',
	'desc' => esc_html__( 'Favicon is a small icon image at the topbar of your browser. Should be 16x16px or 32x32px image (png, ico...)', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'IPhone icon (57x57px)', 'furion' ),
	'type' => 'media',
	'id'   => 'apple-iphone-icon',
	'std'  => '',
	'desc' => esc_html__( 'Similar to the Favicon, the <strong> iPhone icon</strong> is a file used for a web page icon on the  iPhone. When someone bookmarks your web page or adds your web page to their home screen, this icon is used. If this file is not found, these  products will use the screen shot of the web page, which often looks like no more than a white square.', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'IPhone retina icon (114x114px)', 'furion' ),
	'type' => 'media',
	'id'   => 'apple-iphone-retina-icon',
	'std'  => '',
	'desc' => esc_html__( 'The same as  iPhone icon but for Retina iPhone.', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'IPad icon (72x72px)', 'furion' ),
	'type' => 'media',
	'id'   => 'apple-ipad-icon',
	'std'  => '',
	'desc' => esc_html__( 'The same as  iPhone icon but for iPad.', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'IPad Retina icon (144x144px)', 'furion' ),
	'type' => 'media',
	'id'   => 'apple-ipad-retina-icon',
	'std'  => '',
	'desc' => esc_html__( 'The same as  iPhone icon but for Retina iPad.', 'furion' ),
);

/*-----------------------------------------------------------------------------------*/
/* Header
/*-----------------------------------------------------------------------------------*/
$of_options[] = array( 'name' => esc_html__( 'Header', 'furion' ),
	'type' => 'heading',
	'icon' => ADMIN_IMAGES . 'header.png"'
);

$of_options[] = array(  'name' => esc_html__( 'Header style', 'furion' ),
			'type' => 'select',
			'std'  => 'h1_header_',
			'id'   => 'header-style',
			'options' => array(
				'h1_header_'        	=> esc_html__( 'Header style 1', 'furion' ),
				'h2_header_'        	=> esc_html__( 'Header style 2', 'furion' ),
				'h3_header_' 			=> esc_html__( 'Header style 3', 'furion' ),
				'h4_header_' 			=> esc_html__( 'Header style 4', 'furion' ),
			),
			'hidden_option_for_all_section' => true,
			'hidden_option_array' => array( 'h1_header_', 'h2_header_','h3_header_', 'h4_header_' ),
			'k2t_logictic' => array(
				'h1_header_' => array( 'h1_header_' ),
				'h2_header_' => array( 'h2_header_'),
				'h3_header_' => array( 'h3_header_'),
				'h4_header_' => array( 'h4_header_'),
			),			
		);
/* Header */
$of_options = k2t_render_header_style( 'h1_header_', $of_options);
$of_options = k2t_render_header_style( 'h2_header_', $of_options);
$of_options = k2t_render_header_style( 'h3_header_', $of_options);
$of_options = k2t_render_header_style( 'h4_header_', $of_options);

/*-----------------------------------------------------------------------------------*/
/* Footer
/*-----------------------------------------------------------------------------------*/
$of_options[] = array( 'name' => esc_html__( 'Footer', 'furion' ),
	'type' => 'heading',
	'icon' => ADMIN_IMAGES . 'footer.png'
);

$of_options[] = array( 'name'   => esc_html__( 'Show/Hide "Go to top"', 'furion' ),
	'id'   => 'footer-gototop',
	'type' => 'switch',
	'std'  => true,
);

/* Widget area */
$of_options[] = array( 'name' => esc_html__( 'Widget area', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Main Footer', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Sidebars layout', 'furion' ),
	'type' => 'select',
	'id'   => 'bottom-sidebars-layout',
	'options' => array(
		'layout-1' => esc_html__( '1/4 1/4 1/4 1/4', 'furion' ),
		'layout-2' => esc_html__( '1/3 1/3 1/3', 'furion' ),
		'layout-3' => esc_html__( '1/2 1/4 1/4', 'furion' ),
		'layout-4' => esc_html__( '1/4 1/2 1/4', 'furion' ),
		'layout-5' => esc_html__( '1/4 1/4 1/2', 'furion' ),
		'layout-6' => esc_html__( '1/2 1/2', 'furion' ),
		'layout-7' => esc_html__( '1', 'furion' ),
	),
	'std'  => 'layout-2',
	'desc' => esc_html__( 'Select sidebars layout', 'furion' ),
);

$of_options[] = array( 'name'   => esc_html__( 'Background color', 'furion' ),
	'id'   => 'bottom-background-color',
	'type' => 'color',
	'std'  => '',
);

$of_options[] = array( 'name'   => esc_html__( 'Background image', 'furion' ),
	'id'   => 'bottom-background-image',
	'type' => 'upload',
	'std'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Background position?', 'furion' ),
	'type'    => 'select',
	'options' => array(
		''    => esc_html__( 'None', 'furion' ),
		'left top'      => esc_html__( 'Left Top', 'furion' ),
		'left center'   => esc_html__( 'Left Center', 'furion' ),
		'left bottom'   => esc_html__( 'Left Bottom', 'furion' ),
		'right top'     => esc_html__( 'Right Top', 'furion' ),
		'right center'  => esc_html__( 'Right Center', 'furion' ),
		'right bottom'  => esc_html__( 'Right Bottom', 'furion' ),
		'center top'    => esc_html__( 'Center Top', 'furion' ),
		'center center' => esc_html__( 'Center Center', 'furion' ),
		'center bottom' => esc_html__( 'Center Bottom', 'furion' ),
	),
	'std'  => '',
	'id'   => 'bottom-background-position',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Background repeat?', 'furion' ),
	'type'    => 'select',
	'options' => array(
		''    => esc_html__( 'None', 'furion' ),
		'no-repeat' => esc_html__( 'No repeat', 'furion' ),
		'repeat'    => esc_html__( 'Repeat', 'furion' ),
		'repeat-x'  => esc_html__( 'Repeat X', 'furion' ),
		'repeat-y'  => esc_html__( 'Repeat Y', 'furion' ),
	),
	'std'  => '',
	'id'  => 'bottom-background-repeat',
	'desc'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Background size?', 'furion' ),
	'type'    => 'select',
	'options' => array(
		''        => esc_html__( 'None', 'furion' ),
		'auto'    => esc_html__( 'Auto', 'furion' ),
		'cover'   => esc_html__( 'Cover', 'furion' ),
		'contain' => esc_html__( 'Contain', 'furion' ),
	),
	'std'  => '',
	'id'   => 'bottom-background-size',
	'desc' => '',
);

/* Footer bottom */
$of_options[] = array( 'name' => esc_html__( 'Footer', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Bottom Footer', 'furion' ),
);

$of_options[] = array( 'name'   => esc_html__( 'Background color', 'furion' ),
	'id'   => 'footer-background-color',
	'type' => 'color',
	'std'  => '',
);

$of_options[] = array( 'name'   => esc_html__( 'Background image', 'furion' ),
	'id'   => 'footer-background-image',
	'type' => 'upload',
	'std'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Background position?', 'furion' ),
	'type'    => 'select',
	'options' => array(
		''    => esc_html__( 'None', 'furion' ),
		'left top'      => esc_html__( 'Left Top', 'furion' ),
		'left center'   => esc_html__( 'Left Center', 'furion' ),
		'left bottom'   => esc_html__( 'Left Bottom', 'furion' ),
		'right top'     => esc_html__( 'Right Top', 'furion' ),
		'right center'  => esc_html__( 'Right Center', 'furion' ),
		'right bottom'  => esc_html__( 'Right Bottom', 'furion' ),
		'center top'    => esc_html__( 'Center Top', 'furion' ),
		'center center' => esc_html__( 'Center Center', 'furion' ),
		'center bottom' => esc_html__( 'Center Bottom', 'furion' ),
	),
	'std'  => '',
	'id'   => 'footer-background-position',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Background repeat?', 'furion' ),
	'type'    => 'select',
	'options' => array(
		''    => esc_html__( 'None', 'furion' ),
		'no-repeat' => esc_html__( 'No repeat', 'furion' ),
		'repeat'    => esc_html__( 'Repeat', 'furion' ),
		'repeat-x'  => esc_html__( 'Repeat X', 'furion' ),
		'repeat-y'  => esc_html__( 'Repeat Y', 'furion' ),
	),
	'std'  => '',
	'id'  => 'footer-background-repeat',
	'desc'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Background size?', 'furion' ),
	'type'    => 'select',
	'options' => array(
		''        => esc_html__( 'None', 'furion' ),
		'auto'    => esc_html__( 'Auto', 'furion' ),
		'cover'   => esc_html__( 'Cover', 'furion' ),
		'contain' => esc_html__( 'Contain', 'furion' ),
	),
	'std'  => '',
	'id'   => 'footer-background-size',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Footer copyright text', 'furion' ),
	'type' => 'textarea',
	'id'   => 'footer-copyright-text',
	'std'  => '<p style="float:left;line-height: 70px;color:#000;font-family: Montserrat;text-transform: uppercase;">&copy; 2016 Furion. All Rights Reserved</p>',
	'desc' => esc_html__( 'HTML and shortcodes are allowed.', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide Footer Bottom Menu', 'furion' ),
	'id'   => 'footer-bottom-menu',
	'type' => 'switch',
	'std'  => true,
);

/*-----------------------------------------------------------------------------------*/
/* Offcanvas sidebar
/*-----------------------------------------------------------------------------------*/
$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar', 'furion' ),
	'type' => 'heading',
	'icon' => ADMIN_IMAGES . 'footer.png'
);

$of_options[] = array( 'name'   => esc_html__( 'Show/Hide Offcanvas sidebar', 'furion' ),
	'id'   => 'offcanvas-turnon',
	'type' => 'switch',
	'std'  => true,
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar position', 'furion' ),
	'type'    => 'select',
	'options' => array(
		'right'    => esc_html__( 'Right', 'furion' ),
		'left'      => esc_html__( 'Left', 'furion' ),
	),
	'std'  => '',
	'id'   => 'offcanvas-sidebar-position',
	'desc' => '',
);

// Get all sidebar
$sidebars = array();
$widget_list = wp_get_sidebars_widgets();
if ( count( $widget_list ) > 0 ){
	foreach ( $widget_list as $sidebar => $val ) {
		$sidebars[$sidebar] = $sidebar;
	}
}
$of_options[] = array( 'name' => esc_html__( 'Shown sidebar?', 'furion' ),
	'type' => 'select',
	'options' => $sidebars,
	'id' => 'offcanvas-sidebar',
);

$of_options[] = array( 'name' => esc_html__( 'Background setting', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Background setting', 'furion' ),
);

$of_options[] = array( 'name'   => esc_html__( 'Offcanvas sidebar background image', 'furion' ),
	'id'   => 'offcanvas-sidebar-background-image',
	'type' => 'upload',
	'std'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar background position?', 'furion' ),
	'type'    => 'select',
	'options' => array(
		''    => esc_html__( 'None', 'furion' ),
		'left top'      => esc_html__( 'Left Top', 'furion' ),
		'left center'   => esc_html__( 'Left Center', 'furion' ),
		'left bottom'   => esc_html__( 'Left Bottom', 'furion' ),
		'right top'     => esc_html__( 'Right Top', 'furion' ),
		'right center'  => esc_html__( 'Right Center', 'furion' ),
		'right bottom'  => esc_html__( 'Right Bottom', 'furion' ),
		'center top'    => esc_html__( 'Center Top', 'furion' ),
		'center center' => esc_html__( 'Center Center', 'furion' ),
		'center bottom' => esc_html__( 'Center Bottom', 'furion' ),
	),
	'std'  => '',
	'id'   => 'offcanvas-sidebar-background-position',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar background repeat?', 'furion' ),
	'type'    => 'select',
	'options' => array(
		''    => esc_html__( 'None', 'furion' ),
		'no-repeat' => esc_html__( 'No repeat', 'furion' ),
		'repeat'    => esc_html__( 'Repeat', 'furion' ),
		'repeat-x'  => esc_html__( 'Repeat X', 'furion' ),
		'repeat-y'  => esc_html__( 'Repeat Y', 'furion' ),
	),
	'std'  => '',
	'id'  => 'offcanvas-sidebar-background-repeat',
	'desc'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar background size?', 'furion' ),
	'type'    => 'select',
	'options' => array(
		''        => esc_html__( 'None', 'furion' ),
		'auto'    => esc_html__( 'Auto', 'furion' ),
		'cover'   => esc_html__( 'Cover', 'furion' ),
		'contain' => esc_html__( 'Contain', 'furion' ),
	),
	'std'  => '',
	'id'   => 'offcanvas-sidebar-background-size',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar background color', 'furion' ),
	'type' => 'color',
	'id' => 'offcanvas-sidebar-background-color',
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar text color', 'furion' ),
	'type' => 'color',
	'id' => 'offcanvas-sidebar-text-color',
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar custom css', 'furion' ),
	'type' => 'textarea',
	'std'  => '',
	'id'   => 'offcanvas-sidebar-custom-css',
);

/*-----------------------------------------------------------------------------------*/
/* Layout
/*-----------------------------------------------------------------------------------*/
$of_options[] = array( 'name' => esc_html__( 'Layout', 'furion' ),
	'type' => 'heading',
	'icon' => ADMIN_IMAGES . 'layout.png'
);
/* Layout */
$of_options[] = array( 'name' => esc_html__( 'Layout', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Layout', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Content Width (px)', 'furion' ),
	'type' => 'sliderui',
	'std'  => 1170,
	'min'  => 940,
	'max'  => 1200,
	'step' => 10,
	'id'   => 'use-content-width',
	'desc' => esc_html__( 'You can choose content width in the range from 940px to 1200px.', 'furion' ),
);

$of_options = k2t_render_titlebar_options( 'page', $of_options );

/*-----------------------------------------------------------------------------------*/
/* Styling
/*-----------------------------------------------------------------------------------*/
$of_options[] = array( 'name' => esc_html__( 'Style', 'furion' ),
	'type' => 'heading',
	'icon' => ADMIN_IMAGES . 'icon-paint.png'
);

$of_options[] = array( 'name' => esc_html__( 'Primary Color', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Primary Color', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Primary color', 'furion' ),
	'type' => 'select',
	'id'   => 'theme-primary-color',
	'std'  => 'light',
	'options' => array(
		'default'   => esc_html__( 'Default', 'furion' ),
		'blue' 		=> esc_html__( 'Blue', 'furion' ),
		'green' 	=> esc_html__( 'Green', 'furion' ),
		'red' 		=> esc_html__( 'Red', 'furion' ),
		'orange' 	=> esc_html__( 'Orange', 'furion' ),
		'brown' 	=> esc_html__( 'Brown', 'furion' ),
		'custom' 	=> esc_html__( 'Custom', 'furion' ),
	),
	'k2t_logictic' => array(
		'default' 	=> array( ),
		'blue' 		=> array( ),
		'green' 	=> array( ),
		'red' 		=> array( ),
		'orange' 	=> array( ),
		'brown' 	=> array( ),
		'custom' 	=> array( 'primary-color' ),
	),
);

$of_options[] = array( 'name' => esc_html__( 'Primary Color Cusom', 'furion' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'primary-color',
	'desc' => esc_html__( 'Primary color is the main color of site.', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Heading color', 'furion' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'heading-color',
	'desc' => esc_html__( 'Heading color', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Text color', 'furion' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'text-color',
	'desc' => esc_html__( 'Text color', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Links', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Links', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Link color', 'furion' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'link-color',
);

$of_options[] = array( 'name' => esc_html__( 'Link hover color', 'furion' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'link-hover-color',
);

$of_options[] = array( 'name' => esc_html__( 'Footer color', 'furion' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'footer-color',
);

$of_options[] = array( 'name' => esc_html__( 'Footer link color', 'furion' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'footer-link-color',
);

$of_options[] = array( 'name' => esc_html__( 'Menu colors', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Menu colors', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Main menu color', 'furion' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'main-menu-color',
);

$of_options[] = array( 'name' => esc_html__( 'Sub menu color', 'furion' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'sub-menu-color',
);


/*-----------------------------------------------------------------------------------*/
/* Typography
/*-----------------------------------------------------------------------------------*/
$of_options[] = array( 'name' => esc_html__( 'Typography', 'furion' ),
	'type' => 'heading',
	'icon' => ADMIN_IMAGES . 'mac-smz-icon.gif'
);

$of_options[] = array( 'name' => esc_html__( 'Font family', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Font family', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Body font', 'furion' ),
	'desc' => esc_html__( 'You can choose a normal font or Google font.', 'furion' ),
	'id'   => 'body-font',
	'std'  => 'Droid Serif',
	'type' => 'select_google_font',
	'preview'  => array(
		'text' => 'This is the preview!', //this is the text from preview box
		'size' => '30px' //this is the text size from preview box
	),
	'options' => k2t_furion_fonts_array(),
);

$of_options[] = array( 'name' => esc_html__( 'Heading font', 'furion' ),
	'desc' => esc_html__( 'You can choose a normal font or Google font', 'furion' ),
	'id'   => 'heading-font',
	'std'  => 'Montserrat',
	'type' => 'select_google_font',
	'preview' => array(
		'text' => 'This is the preview!', //this is the text from preview box
		'size' => '30px' //this is the text size from preview box
	),
	'options' => k2t_furion_fonts_array(),
);

$of_options[] = array( 'name' => esc_html__( 'Navigation font', 'furion' ),
	'desc' => esc_html__( 'You can choose a normal font or Google font', 'furion' ),
	'id'   => 'mainnav-font',
	'std'  => 'Montserrat',
	'type' => 'select_google_font',
	'preview' => array(
		'text' => 'This is the preview!', //this is the text from preview box
		'size' => '30px' //this is the text size from preview box
	),
	'options' => k2t_furion_fonts_array(),
);

$of_options[] = array( 'name' => esc_html__( 'General font size', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'General font size', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Body font size (px)', 'furion' ),
	'type' => 'sliderui',
	'std'  => 14,
	'min'  => 8,
	'max'  => 28,
	'step' => 1,
	'id'   => 'body-size',
);

$of_options[] = array( 'name' => esc_html__( 'Main navigation font size (px)', 'furion' ),
	'type' => 'sliderui',
	'std'  => 15,
	'min'  => 9,
	'max'  => 24,
	'step' => 1,
	'id'   => 'mainnav-size',
);

$of_options[] = array( 'name' => esc_html__( 'Submenu of Main navigation font size (px)', 'furion' ),
	'type' => 'sliderui',
	'std'  => 15,
	'min'  => 9,
	'max'  => 24,
	'step' => 1,
	'id'   => 'submenu-mainnav-size',
);

$of_options[] = array( 'name' => esc_html__( 'Titlebar title font size (px)', 'furion' ),
	'type' => 'sliderui',
	'std'  => 30,
	'min'  => 14,
	'max'  => 120,
	'step' => 1,
	'id'   => 'titlebar_font_size',
);

$of_options[] = array( 'name' => esc_html__( 'Headings font size', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Headings font size', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'H1 font size (px)', 'furion' ),
	'type' => 'sliderui',
	'std'  => 45,
	'min'  => 20,
	'max'  => 80,
	'step' => 1,
	'id'   => 'h1-size',
);

$of_options[] = array( 'name' => esc_html__( 'H2 font size (px)', 'furion' ),
	'type' => 'sliderui',
	'std'  => 40,
	'min'  => 16,
	'max'  => 64,
	'step' => 1,
	'id'   => 'h2-size',
);

$of_options[] = array( 'name' => esc_html__( 'H3 font size (px)', 'furion' ),
	'type' => 'sliderui',
	'std'  => 28,
	'min'  => 12,
	'max'  => 48,
	'step' => 1,
	'id'   => 'h3-size',
);

$of_options[] = array( 'name' => esc_html__( 'H4 font size (px)', 'furion' ),
	'type' => 'sliderui',
	'std'  => 24,
	'min'  => 8,
	'max'  => 32,
	'step' => 1,
	'id'   => 'h4-size',
);

$of_options[] = array( 'name' => esc_html__( 'H5 font size (px)', 'furion' ),
	'type' => 'sliderui',
	'std'  => 20,
	'min'  => 8,
	'max'  => 30,
	'step' => 1,
	'id'   => 'h5-size',
);

$of_options[] = array( 'name' => esc_html__( 'H6 font size (px)', 'furion' ),
	'type' => 'sliderui',
	'std'  => 14,
	'min'  => 8,
	'max'  => 30,
	'step' => 1,
	'id'   => 'h6-size',
);

$of_options[] = array( 'name' => esc_html__( 'Font type', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Font type', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Navigation text transform', 'furion' ),
	'desc' => esc_html__( 'Select navigation text transform.', 'furion' ),
	'type' => 'select',
	'id'   => 'mainnav-text-transform',
	'std'  => 'uppercase',
	'options' => array (
		'none'       => esc_html__( 'None', 'furion' ),
		'capitalize' => esc_html__( 'Capitalize', 'furion' ),
		'uppercase'  => esc_html__( 'Uppercase', 'furion' ),
		'lowercase'  => esc_html__( 'Lowercase', 'furion' ),
		'inherit'    => esc_html__( 'Inherit', 'furion' ),
	),
);

$of_options[] = array( 'name' => esc_html__( 'Navigation font weight', 'furion' ),
	'desc' => esc_html__( 'Select navigation font weight.', 'furion' ),
	'type' => 'select',
	'id'   => 'mainnav-font-weight',
	'std'  => '300',
	'options' => array (
		'100' => esc_html__( '100', 'furion' ),
		'200' => esc_html__( '200', 'furion' ),
		'300' => esc_html__( '300', 'furion' ),
		'400' => esc_html__( '400', 'furion' ),
		'500' => esc_html__( '500', 'furion' ),
		'600' => esc_html__( '600', 'furion' ),
		'700' => esc_html__( '700', 'furion' ),
		'800' => esc_html__( '800', 'furion' ),
		'900' => esc_html__( '900', 'furion' ),
	),
);

/*-----------------------------------------------------------------------------------*/
/* Blog
/*-----------------------------------------------------------------------------------*/
$of_options[] = array( 'name' => esc_html__( 'Blog', 'furion' ),
	'type' => 'heading',
	'icon' => ADMIN_IMAGES . 'icon-docs.png'
);

$of_options[] = array( 'name' => esc_html__( 'Blog layout', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Blog layout', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Blog layout', 'furion' ),
	'type' => 'select',
	'id'   => 'blog-layout',
	'options' => array (
		'right_sidebar' => esc_html__( 'Right Sidebar (default)', 'furion' ),
		'left_sidebar'  => esc_html__( 'Left Sidebar', 'furion' ),
		'no_sidebar'    => esc_html__( 'No Sidebar', 'furion' ) ),
	'std'  => 'right_sidebar',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Blog sidebar', 'furion' ),
	'type' => 'text',
	'std'  => '',
	'id'   => 'blog-custom-sidebar',
	'desc'   => '',
);

$of_options[] = array( 'name' => esc_html__( 'Blog style', 'furion' ),
	'type' => 'select',
	'id'   => 'blog-style',
	'options' => array (
		'large'    => esc_html__( 'Large', 'furion' ),
		'grid'     => esc_html__( 'Grid', 'furion' ),
		'medium'   => esc_html__( 'Medium', 'furion' ),
		'masonry'  => esc_html__( 'Masonry', 'furion' ),
	),
	'std'       => 'large',
	'desc'      => esc_html__( 'Select blog style.', 'furion' ),
	'logicstic' => true,
);

$of_options[] = array(  'name' => esc_html__( 'Columns', 'furion' ),
	'type'    => 'select',
	'id'      => 'blog-masonry-column',
	'options' => array (
		'column-2' => esc_html__( '2 Columns', 'furion' ),
		'column-3' => esc_html__( '3 Columns', 'furion' ),
		'column-4' => esc_html__( '4 Columns', 'furion' ),
		'column-5' => esc_html__( '5 Columns', 'furion' )
	),
	'std'  => 'column-3',
	'desc' => esc_html__( 'Select column for layout masonry.', 'furion' ),
	'conditional_logic' => array(
		'field'    => 'blog-style',
		'value'    => 'masonry',
	),
);
$of_options[] = array(  'name' => esc_html__( 'Columns', 'furion' ),
	'type'    => 'select',
	'id'      => 'blog-grid-column',
	'options' => array (
		'column-2' => esc_html__( '2 Columns', 'furion' ),
		'column-3' => esc_html__( '3 Columns', 'furion' ),
	),
	'std'  => 'column-2',
	'desc' => esc_html__( 'Select column for layout grid.', 'furion' ),
	'conditional_logic' => array(
		'field'    => 'blog-style',
		'value'    => 'grid',
	),
);
$of_options[] = array(  'name' => esc_html__( 'Full width', 'furion' ),
	'type' => 'switch',
	'std'  => false,
	'id'   => 'blog-masonry-full-width',
	'desc' => esc_html__( 'Enable full width layout for masonry blog.', 'furion' ),
	'conditional_logic' => array(
		'field'    => 'blog-style',
		'value'    => 'masonry',
	),
);

$of_options[] = array( 'name' => esc_html__( 'Blog Options', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Blog Options', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Content or excerpt', 'furion' ),
	'type'    => 'select',
	'id'      => 'blog-display',
	'options' => array (
		'excerpts' => esc_html__( 'Excerpt', 'furion' ),
		'contents' => esc_html__( 'Content', 'furion' ) ),
	'std'  => 'excerpt',
	'desc' => esc_html__( 'Select display post content or excerpt on the blog.', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Excerpt length (words)', 'furion' ),
	'type' => 'sliderui',
	'std'  => 25,
	'step' => 1,
	'min'  => 10,
	'max'  => 80,
	'id'   => 'excerpt-length',
);

$of_options[] = array(  'name' => esc_html__( 'Infinite Scroll', 'furion' ),
	'type'    => 'select',
	'id'      => 'pagination-type',
	'options' => array (
		'pagination_number' => esc_html__( 'Pagination Number', 'furion' ),
		'pagination_lite'   => esc_html__( 'Pagination Lite', 'furion' ),
		'pagination_ajax'   => esc_html__( 'Pagination Ajax', 'furion' ),
	),
	'std' => 'pagination_number'
);

$of_options[] = array(  'name' => esc_html__( 'Show/Hide categories filter', 'furion' ),
	'type' => 'switch',
	'id'   => 'blog-categories-filter',
	'std'  => true
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide title link?', 'furion' ),
	'type' => 'switch',
	'id'   => 'blog-post-link',
	'std'  => true
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide post date', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog-date',
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide the number of comments', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog-number-comment',
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide categories', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog-categories',
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide author', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog-author',
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide "Reamore" link', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog-readmore',
);

$of_options = k2t_render_titlebar_options( 'blog', $of_options );

/* Social Impact */
$of_options[] = array( 'name' => esc_html__( 'Social', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Social', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide social buttons', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog-social',
	'desc' => esc_html__( 'Turn it OFF if you do not want to display social buttons', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Facebook?', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog-social-facebook',
);

$of_options[] = array( 'name' => esc_html__( 'Twitter?', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog-social-twitter',
);

$of_options[] = array( 'name' => esc_html__( 'Google+?', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog-social-google-plus',
);

$of_options[] = array( 'name' => esc_html__( 'Linkedin?', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog-social-linkedin',
);

$of_options[] = array( 'name' => esc_html__( 'Tumblr?', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog-social-tumblr',
);

$of_options[] = array( 'name' => esc_html__( 'Email?', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog-social-email',
);

/*-----------------------------------------------------------------------------------*/
/* Single
/*-----------------------------------------------------------------------------------*/
$of_options[] = array( 'name' => esc_html__( 'Single', 'furion' ),
	'type' => 'heading',
	'icon' => ADMIN_IMAGES . 'icon-edit.png'
);

/* Featured Image */
$of_options[] = array( 'name' => esc_html__( 'Single Post Layout', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Single Post Layout', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Single post layout', 'furion' ),
	'type' => 'select',
	'id'   => 'single-layout',
	'options' => array (
		'right_sidebar' => esc_html__( 'Right Sidebar (default)', 'furion' ),
		'left_sidebar'  => esc_html__( 'Left Sidebar', 'furion' ),
		'no_sidebar'    => esc_html__( 'No Sidebar', 'furion' ) ),
	'std'  => 'right_sidebar',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Single custom sidebar', 'furion' ),
	'type' => 'text',
	'std'  => '',
	'id'   => 'single-custom-sidebar',
	'desc'   => '',
);

/* Meta */
$of_options[] = array( 'name' => esc_html__( 'Meta', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( 'Meta', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide Tags', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'single-tags',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display tags on single post', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide Commnet Number', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'single-comments',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display comment number on single post', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide Next / Previous links?', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'single-nav',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display post navigation links on single post', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide authorbox', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'single-authorbox',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display author box on single post', 'furion' ),
);

$of_options[] = array(  'name' => esc_html__( 'Show/Hide related post', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'single-related-post',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display related post on single post', 'furion' ),
);

$of_options[] = array(  'name' => esc_html__( 'Related post title', 'furion' ),
	'type' => 'text',
	'std'  => 'You may also like',
	'id'   => 'single-related-post-title',
	'desc' => '',
);
$of_options[] = array( 'name' => esc_html__( 'Number of related post', 'furion' ),
	'type' => 'text',
	'std'  => 3,
	'id'   => 'single-related-post-number',
	'desc' => esc_html__( 'Fill out -1 if you want to display ALL related post.', 'furion' ),
);
$of_options[] = array( 'name' => esc_html__( 'Show/Hide comment form', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'single-commnet-form',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display comment form on single post', 'furion' ),
);

$of_options = k2t_render_titlebar_options( 'single', $of_options );

/*-----------------------------------------------------------------------------------*/
/* Gallery
/*-----------------------------------------------------------------------------------*/
$of_options[] = array( 'name' => esc_html__( 'Gallery', 'furion' ),
	'type' => 'heading',
	'icon' => ADMIN_IMAGES . 'gallery.png'
);

$of_options[] = array( 'name' => esc_html__( 'Gallery Slug', 'furion' ),
	'type' => 'text',
	'std'  => '',
	'id'   => 'gallery-slug',
);

/*-----------------------------------------------------------------------------------*/
/* 404 Page
/*-----------------------------------------------------------------------------------*/
$of_options[] = array( 'name' => esc_html__( '404 Page', 'furion' ),
	'type' => 'heading',
	'icon' => ADMIN_IMAGES . '404.png'
);

$of_options[] = array( 'name' => esc_html__( '404 page', 'furion' ),
	'type' => 'info',
	'std'  => esc_html__( '404 page', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( '404 Title', 'furion' ),
	'type' => 'text',
	'std'  => 'Oops! Looks like something was broken.',
	'id'   => '404-title',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( '404 Image', 'furion' ),
	'type' => 'media',
	'std'  => '',
	'id'   => '404-image',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( '404 Custom Text', 'furion' ),
	'type' => 'textarea',
	'id'   => '404-text',
);

/*-----------------------------------------------------------------------------------*/
/* Social Icons
/*-----------------------------------------------------------------------------------*/
$of_options[] = array( 'name' => esc_html__( 'Social', 'furion' ),
	'type'  => 'heading',
	'icon'  => ADMIN_IMAGES . 'twitter.png'
);

$of_options[] = array( 'name' => esc_html__( 'Target', 'furion' ),
	'type' => 'select',
	'std'  => '_blank',
	'options' => array(
		'_self'  => esc_html__( 'Same tab', 'furion' ),
		'_blank' => esc_html__( 'New tab', 'furion' ),
	),
	'id' => 'social-target',
);

$of_options[] = array( 'name' => esc_html__( 'Twitter username?', 'furion' ),
	'type' => 'text',
	'std'  => 'themelead',
	'id'   => 'twitter-username',
	'desc' => esc_html__( 'Twitter username used for tweet share buttons.', 'furion' ),
);

$of_options[] = array( 'name' => esc_html__( 'Icon title?', 'furion' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'social-title',
	'desc' => esc_html__( 'Turn it ON if you want to display social icon titles like Facebook, Google+, Twitter... when hover icons.', 'furion' ),
);


foreach ( k2t_furion_social_array() as $s => $c ):

	$of_options[] = array( 'name' => $c,
		'type' => 'text',
		'std'  => '',
		'id'   => 'social-' . $s,
	);

endforeach;


$of_options[] = array( 'name' => esc_html__( 'Social Share', 'furion' ),
	'type'  => 'heading',
	'icon'  => ADMIN_IMAGES . 'twitter.png'
);

$of_options = k2t_furion_social_share_options( '', $of_options );

/*-----------------------------------------------------------------------------------*/
/* Backup Options */
/*-----------------------------------------------------------------------------------*/
$of_options[] = array( 'name'   => esc_html__( 'Backup Options', 'furion' ),
	'type'  => 'heading',
	'icon'  => ADMIN_IMAGES . 'icon-slider.png'
);

$of_options[] = array( 'name'   => esc_html__( 'Backup and Restore Options', 'furion' ),
	'id'   => 'of_backup',
	'std'  => '',
	'type' => 'backup',
	'desc' => esc_html__( 'You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need back.', 'furion' ),
);

$of_options[] = array( 'name'   => esc_html__( 'Transfer Theme Options Data', 'furion' ),
	'id'   => 'of_transfer',
	'std'  => '',
	'type' => 'transfer',
	'desc' => esc_html__( 'You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another installation, replace the data in the text box with the one from another installation and click "Import Options".', 'furion' ),
);

/*-----------------------------------------------------------------------------------*/
/* One Click Install */
/*-----------------------------------------------------------------------------------*/
 $of_options[] = array( 'name'   => esc_html__( 'One Click Install', 'furion' ),
 	'type'  => 'heading',
 	'icon'  => ADMIN_IMAGES . 'one-click-install.png'
 );
 $of_options[] = array( 'name'   => esc_html__( 'Transfer Theme Options Data', 'furion' ),
 	'id'   => 'k2t_advance_backup',
 	'std'  => '',
 	'type' => 'k2t_advance_backup',
 );

/*-----------------------------------------------------------------------------------*/
/* Get data for sample data */
/*-----------------------------------------------------------------------------------*/
// $of_options[] = array(  'name'   => esc_html__( 'Sample theme options', 'grid' ),
// 	'id'   => 'get_theme_option_widget',
// 	'std'  => '',
// 	'type' => 'get_theme_option_widget',
// );
