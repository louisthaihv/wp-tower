<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Ruby
 * @author  KingKongThemes
 * @link	http://www.kingkongthemes.com
 */

// Get theme option
global $smof_data;

get_header(); ?>

	<section class="k2t-not-found">
		<main class="k2t-wrap" role="main">

			<div class="k2t-error-404">
				<header class="page-header">
					<img src="<?php echo esc_url( $smof_data['404-image'] ); ?>" />
					<h1><?php echo esc_html( $smof_data['404-title'] ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php echo $smof_data['404-text']; ?></p>
				</div><!-- .page-content -->
			</div><!-- .k2t-error-404 -->

		</main><!-- .k2t-wrap -->
	</section><!-- .k2t-not-found -->

<?php get_footer(); ?>
