# Ruby changelog
![mou icon](http://i.imgur.com/Ktz1FB3.png)

## Version 1.3
### Released date: 10-06-2015
* Fix: header on Mobile
* Update: Security for Visual Composer
* Tweak: Update all plugins included in the theme to the latest version

## Version 1.2
### Released date: 09-21-2015
* Fix: Blog post conflict with VC
* Add: New mobile menu
* Tweak: Update all plugins included in the theme to the latest version

## Version 1.1.2
### Released date: 08-01-2015
* Fix: Icon box display
* Tweak: Update all plugins included in the theme to the latest version

## Version 1.1.1
### Released date: 07-14-2015
* Fix:   Icon Box
* Fix: 	 Related Post
* Tweak: Update all plugins included in the theme to the latest version

## Version 1.1
### Released date: 06-30-2015
* Fix:   Small issues when Install Sample Data
* Tweak: Edit Small blog thumbnail template

## Version 1.0.9
### Released date: 06-24-2015
* Fix:   Post format gallery not working in blog infinity scroll timeline
* Tweak: Update all plugins included in the theme to the latest version
* Tweak: Rename all plugins of the theme
* New:   Auto deactive old plugins
* New:   Add related post for blog single

## Version 1.0.8
### Released date: 06-17-2015
* Fix:   XSS vulnerability in the prettyPhoto
* Fix:   Border top in box heading
* Tweak: Update all plugins included in the theme to the latest version
* Tweak: Update latest google fonts list

## Version 1.0.7
### Released date: 06-08-2015
* Fix:   Custom CSS in header section doesn�t show up when editing
* Fix:   On/off switch in theme options was reversed back to default value after saving all changes

## Version 1.0.6
### Released date: 06-06-2015
* Fix:   Some minor bugs
* Fix:   Responsive for any column in footer
* Fix:   Font-size for Heading shortcode
* Tweak: Update all plugins included in the theme to the latest version

## Version 1.0.5
### Released date: 05-24-2015
* Tweak: Optimize Import Sample Data function on server
* New:   Add the Ebook - WordPress Decoded For Businesses

## Version 1.0.4
### Released date: 05-15-2015
* Fix:   Update enqueue script mechanism of google map element
* Fix:   Display style of Heading and google maps
* Fix:   Getimagesize function
* Fix:   Some minor bugs
* Tweak: Update all plugins included in the theme to the latest version
* New:   Compatible with WPML

## Version 1.0.3
### Released date: 04-30-2015
* Tweak: Update all plugins included in the theme to the latest version
* New:   Add child theme to Download package

## Version 1.0.2
### Released date: 04-23-2015
* Fix:   Mobile menu doesnt work on Chrome
* Fix:   Add default style for mobile menu
* Fix:   Fixing add_query_arg() and remove_query_arg() usage on mega menu
* Tweak: Update all plugins included in the theme to the latest version

## Version 1.0.1
### Released date: 04-18-2015
* Fix:   Tabs do not work on k2t-shortcodes plugin
* Fix:   Html content does not display on footer
* Fix:   Retina logo does not work
* Fix:   Some minor bugs
* Tweak: Change install sample data mechanism
* Tweak: Update latest version of all plugins included in the theme
* New:   Added blog template files
* New:   Added support for Envato Toolkit plugin (theme auto-update plugin)

## Version 1.0
### Released date: 04-08-2015
#### Initial release