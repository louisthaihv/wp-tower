<?php
add_action( 'init', 'of_options' );

if ( ! function_exists( 'of_options' ) ) {
	function of_options() {

		/*-----------------------------------------------------------------------------------*/
		/* The Options Array */
		/*-----------------------------------------------------------------------------------*/

		// Set the Options Array
		global $of_options, $smof_data;
		$of_options = array();

		$patterns_path = get_stylesheet_directory() . '/assets/img/patterns/light/';
		$patterns_url  = get_template_directory_uri() . '/assets/img/patterns/light/';
		$light_pattern_arr = array( '' => __( 'None', 'ruby' ) );
		if ( is_dir( $patterns_path ) ) {
			if ( $patterns_dir = opendir( $patterns_path ) ) {
				while ( ( $patterns_file = readdir( $patterns_dir ) ) !== false ) {
					if ( ( stristr( $patterns_file, '.png' ) !== false || stristr( $patterns_file, '.jpg' ) !== false || stristr( $patterns_file, '.gif' ) !== false ) && stristr( $patterns_file , '2X' ) === false ) {
						$light_pattern_arr[ $patterns_url . $patterns_file ] = $patterns_file;
					}
				}
			}
		}
		asort( $light_pattern_arr );

		$patterns_path = get_stylesheet_directory() . '/assets/img/patterns/dark/';
		$patterns_url = get_template_directory_uri() . '/assets/img/patterns/dark/';
		$dark_pattern_arr = array( '' => __( 'None', 'ruby' ) );
		if ( is_dir( $patterns_path ) ) {
			if ( $patterns_dir = opendir( $patterns_path ) ) {
				while ( ( $patterns_file = readdir( $patterns_dir ) ) !== false ) {
					if ( ( stristr( $patterns_file, '.png' ) !== false || stristr( $patterns_file, '.jpg' ) !== false || stristr( $patterns_file, '.gif' ) !== false ) && stristr( $patterns_file , '2X' ) === false ) {
						$dark_pattern_arr[ $patterns_url . $patterns_file ] = $patterns_file;
					}
				}
			}
		}
		asort( $dark_pattern_arr );

		// Get all menu
		$total_menu = get_terms( 'nav_menu', array( 'hide_empty' => true ) );
		$total_menu_arr = array( '' => __( 'None', 'ruby' ) );
		if ( count( $total_menu ) > 0 ) {
			foreach ( $total_menu as $menu ) {
				$total_menu_arr[$menu->term_id] = $menu->name;
			}
		}

		// Get all sidebar
		$sidebars = array();
		if ( count( $GLOBALS['wp_registered_sidebars'] > 0 ) ){
			foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {

				$sidebars[$sidebar['id']] = $sidebar['name'];
			}
		}

		/*-----------------------------------------------------------------------------------*/
		/* General */
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( 'General', 'ruby' ),
			'type' => 'heading',
			'icon' => ADMIN_IMAGES . 'icon-settings.png'
		);

		$of_options[] = array(  'name' => __( 'General', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'General', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Show/Hide breadcrumb', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'breadcrumb',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide page loader', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'pageloader',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( 'Sidebar width', 'ruby' ),
			'type' => 'sliderui',
			'id'   => 'sidebar_width',
			'min'  => 0,
			'max'  => 100,
			'step' => 1,
			'std'  => 30,
			'desc' => __( 'Unit: %', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Header Code', 'ruby' ),
			'type' => 'textarea',
			'std'  => '',
			'id'   => 'header_code',
			'desc' => __( 'You can load Google fonts here.', 'ruby' ),
			'is_js_editor' => '1'
		);

		$of_options[] = array(  'name' => __( 'Custom CSS', 'ruby' ),
			'type' => 'textarea',
			'std'  => '',
			'id'   => 'custom_css',
			'desc' => __( 'If you know a little about CSS, you can write your custom CSS here. Do not edit CSS files (it will be lost when you update this theme).', 'ruby' ),
			'is_css_editor' => '1'
		);

		/* Icons */
		$of_options[] = array(  'name' => __( 'Icons', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Icons', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Favicon', 'ruby' ),
			'type' => 'media',
			'id'   => 'favicon',
			'std'  => get_template_directory_uri() . '/assets/img/icons/favicon.png',
			'desc' => __( 'Favicon is a small icon image at the topbar of your browser. Should be 16x16px or 32x32px image (png, ico...)', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'IPhone icon (57x57px)', 'ruby' ),
			'type' => 'media',
			'id'   => 'apple-iphone-icon',
			'std'  => get_template_directory_uri() . '/assets/img/icons/iphone.png',
			'desc' => __( 'Similar to the Favicon, the <strong> iPhone icon</strong> is a file used for a web page icon on the  iPhone. When someone bookmarks your web page or adds your web page to their home screen, this icon is used. If this file is not found, these  products will use the screen shot of the web page, which often looks like no more than a white square.', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'IPhone retina icon (114x114px)', 'ruby' ),
			'type' => 'media',
			'id'   => 'apple-iphone-retina-icon',
			'std'  => get_template_directory_uri() . '/assets/img/icons/iphone@2x.png',
			'desc' => __( 'The same as  iPhone icon but for Retina iPhone.', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'IPad icon (72x72px)', 'ruby' ),
			'type' => 'media',
			'id'   => 'apple-ipad-icon',
			'std'  => get_template_directory_uri() . '/assets/img/icons/ipad.png',
			'desc' => __( 'The same as  iPhone icon but for iPad.', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'IPad Retina icon (144x144px)', 'ruby' ),
			'type' => 'media',
			'id'   => 'apple-ipad-retina-icon',
			'std'  => get_template_directory_uri() . '/assets/img/icons/ipad@2x.png',
			'desc' => __( 'The same as  iPhone icon but for Retina iPad.', 'ruby' ),
		);

		/*-----------------------------------------------------------------------------------*/
		/* Header
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( 'Header', 'ruby' ),
			'type' => 'heading',
			'icon' => ADMIN_IMAGES . 'header.png"'
		);
		/* Header */
		$of_options[] = array(  'name' => __( 'Header Backup Options', 'ruby' ),
			'type' => 'advance_importer',
			'std'  => __( 'Header Backup Options', 'ruby' ),
			'desc' => 'Import or Export Header layouts for all Header sections',
			'id' => 'header-advance-import',
			'advance_importer' => array( 'sticky-menu' , 'vertical-menu', 'fixed-header' , 'full-header' , 'use-top-header' , 'header_section_1' , 'use-mid-header' , 'header_section_2' , 'use-bot-header' , 'header_section_3' )
		);
		/* Header */
		$of_options[] = array(  'name' => __( 'Header Settings', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Header Settings', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Enable menu appear on other header sections?', 'ruby' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => 'enable_menu_header_section',
			'desc' => __( 'Enable this option can let the menu display on Top Header Secion or Bottom Header Section. By default "OFF", the menu only displays in Header Middle Section', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Sticky menu?', 'ruby' ),
			'type' => 'select',
			'options' => array(
				''        => __( 'None', 'ruby' ),
				'sticky_top' => __( 'Sticky menu on top header', 'ruby' ),
				'sticky_mid' => __( 'Sticky menu on middle header', 'ruby' ),
				'sticky_bot' => __( 'Sticky menu on bottom header', 'ruby' ),
			),
			'std'  => '',
			'id'   => 'sticky-menu',
			'desc' => __( 'Enable this setting so that the header section and menus inlcuded in the header are sticky', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Vertical menu?', 'ruby' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => 'vertical-menu',
			'desc' => __( 'Enable to display menu on Header Middle Section in the vertical style. When do that, Top Header Section and Bottom Header Section will be disable automatically', 'ruby' ),
			'hidden_option_array' => array( 'full-header', 'fixed-header', 'use-top-header', 'header_section_1', 'use-bot-header', 'header_section_3' ),
			'k2t_logictic' => array(
				/* 0 is display those */
				'0' => array( 'full-header','use-mid-header', 'fixed-header', 'use-top-header', 'header_section_1', 'use-bot-header', 'header_section_3' ),
				/* 1 is display those */
				'1' => array( 'use-mid-header' ),
			),			
		);

		$of_options[] = array(  'name' => __( 'Header Layouts', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Header Layouts', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Fixed header?', 'ruby' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => 'fixed-header',
			'desc' => __( 'If the setting is enabled, the body section will be displayed under the header. If the setting is disabled, the body section will be displayed above the header. In the 2nd case, you can make the header transparent to create a very nice style', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Fullwidth header layout', 'ruby' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => 'full-header',
			'desc' => __( 'Turn it ON if you want to set full width header.', 'ruby' ),
		);

		/* Visual Header */
		$of_options[] = array(  'name' => __( 'Top Header Section', 'ruby' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => 'use-top-header',
			'desc' => __( 'Show or hide top header layout.', 'ruby' ),
			'k2t_logictic' => array(
				'0' => array( ),
				'1' => array( 'header_section_1' ),
			),			
		);

		$of_options[] = array(
			'type' => 'k2t_header_option',
			'std'  => '',
			'id'   => 'header_section_1',
			'desc' => '',
			
		);

		$of_options[] = array(  'name' => __( 'Middle Header Section', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'use-mid-header',
			'desc' => __( 'Show or hide middle header layout.', 'ruby' ),
			'k2t_logictic' => array(
				'0' => array( ),
				'1' => array( 'header_section_2' ),

			),		
		);

		$of_options[] = array(
			'type' => 'k2t_header_option',
			'std'  => '{"name":"header_section_2","setting":{"bg_image":"","bg_color":"","opacity":"","fixed_abs":"fixed","custom_css":""},"columns_num":2,"htmlData":"","columns":[{"id":1,"percent":"2","value":[{"id":"1425696862388","type":"logo","value":{"custom_class":"","custom_id":""}}]},{"id":2,"value":[{"id":"1427731147425","type":"custom_menu","value":{"menu_id":"TWFpbg==","custom_class":"","custom_id":""}}],"percent":"10"}]}',
			'id'   => 'header_section_2',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( 'Bottom Header Section', 'ruby' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => 'use-bot-header',
			'desc' => __( 'Show or hide middle header layout.', 'ruby' ),
			'k2t_logictic' => array(
				'0' => array( ),
				'1' => array( 'header_section_3' ),
			),		
		);

		$of_options[] = array(
			'type' => 'k2t_header_option',
			'std'  => '',
			'id'   => 'header_section_3',
			'desc' => '',
		);

		/* Logo */
		$of_options[] = array(  'name' => __( 'Logo', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Logo', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Use text logo', 'ruby' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => 'use-text-logo',
			'desc' => __( 'Turn it ON if you want to use text logo instead of image logo.', 'ruby' ),
			'logicstic' => true,
		);

		$of_options[] = array(  'name' => __( 'Text logo', 'ruby' ),
			'type' => 'text',
			'std'  => '',
			'id'   => 'text-logo',
			'desc' => '',
			'conditional_logic' => array(
				'field'    => 'use-text-logo',
				'value'    => 'switch-1',
			),
		);

		$of_options[] = array(  'name' => __( 'Logo', 'ruby' ),
			'type' => 'media',
			'id'   => 'logo',
			'std'  => get_template_directory_uri() . '/assets/img/logo.png',
			'desc' => __( 'The logo size in our demo is 116x33px. Please use jpg, jpeg, png or gif image for best performance.', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Retina logo', 'ruby' ),
			'type' => 'media',
			'id'   => 'retina-logo',
			'std'  => get_template_directory_uri() . '/assets/img/logo@2x.png',
			'desc' => __( '2x times your logo dimension.', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Logo margin top (px)', 'ruby' ),
			'type' => 'sliderui',
			'id'   => 'logo-margin-top',
			'min'  => 0,
			'max'  => 200,
			'step' => 1,
			'std'  => 30,
		);

		$of_options[] = array(  'name' => __( 'Logo margin right (px)', 'ruby' ),
			'type' => 'sliderui',
			'id'   => 'logo-margin-right',
			'min'  => 0,
			'max'  => 200,
			'step' => 1,
			'std'  => 19,
		);

		$of_options[] = array(  'name' => __( 'Logo margin bottom (px)', 'ruby' ),
			'type' => 'sliderui',
			'id'   => 'logo-margin-bottom',
			'min'  => 0,
			'max'  => 200,
			'step' => 1,
			'std'  => 19,
		);

		$of_options[] = array(  'name' => __( 'Logo margin left (px)', 'ruby' ),
			'type' => 'sliderui',
			'id'   => 'logo-margin-left',
			'min'  => 0,
			'max'  => 200,
			'step' => 1,
			'std'  => 0,
		);

		/*-----------------------------------------------------------------------------------*/
		/* Footer
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( 'Footer', 'ruby' ),
			'type' => 'heading',
			'icon' => ADMIN_IMAGES . 'footer.png'
		);

		$of_options[] = array(  'name'   => __( 'Show/Hide "Go to top"', 'ruby' ),
			'id'   => 'footer-gototop',
			'type' => 'switch',
			'std'  => true,
		);

		/* Widget area */
		$of_options[] = array(  'name' => __( 'Widget area', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Main Footer', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Sidebars layout', 'ruby' ),
			'type' => 'select',
			'id'   => 'bottom-sidebars-layout',
			'options' => array(
				'layout-1' => __( '1/4 1/4 1/4 1/4', 'ruby' ),
				'layout-2' => __( '1/3 1/3 1/3', 'ruby' ),
				'layout-3' => __( '1/2 1/4 1/4', 'ruby' ),
				'layout-4' => __( '1/4 1/2 1/4', 'ruby' ),
				'layout-5' => __( '1/4 1/4 1/2', 'ruby' ),
				'layout-6' => __( '1/2 1/2', 'ruby' ),
				'layout-7' => __( '1', 'ruby' ),
			),
			'std'  => 'layout-1',
			'desc' => __( 'Select sidebars layout', 'ruby' ),
		);

		$of_options[] = array(  'name'   => __( 'Background color', 'ruby' ),
			'id'   => 'bottom-background-color',
			'type' => 'color',
			'std'  => '#fff',
		);

		$of_options[] = array(  'name'   => __( 'Background image', 'ruby' ),
			'id'   => 'bottom-background-image',
			'type' => 'upload',
			'std'  => '',
		);

		$of_options[] = array(  'name' => __( 'Background position?', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				''    => __( 'None', 'ruby' ),
				'left top'      => __( 'Left Top', 'ruby' ),
				'left center'   => __( 'Left Center', 'ruby' ),
				'left bottom'   => __( 'Left Bottom', 'ruby' ),
				'right top'     => __( 'Right Top', 'ruby' ),
				'right center'  => __( 'Right Center', 'ruby' ),
				'right bottom'  => __( 'Right Bottom', 'ruby' ),
				'center top'    => __( 'Center Top', 'ruby' ),
				'center center' => __( 'Center Center', 'ruby' ),
				'center bottom' => __( 'Center Bottom', 'ruby' ),
			),
			'std'  => '',
			'id'   => 'bottom-background-position',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( 'Background repeat?', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				''    => __( 'None', 'ruby' ),
				'no-repeat' => __( 'No repeat', 'ruby' ),
				'repeat'    => __( 'Repeat', 'ruby' ),
				'repeat-x'  => __( 'Repeat X', 'ruby' ),
				'repeat-y'  => __( 'Repeat Y', 'ruby' ),
			),
			'std'  => '',
			'id'  => 'bottom-background-repeat',
			'desc'  => '',
		);

		$of_options[] = array(  'name' => __( 'Background size?', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				''        => __( 'None', 'ruby' ),
				'auto'    => __( 'Auto', 'ruby' ),
				'cover'   => __( 'Cover', 'ruby' ),
				'contain' => __( 'Contain', 'ruby' ),
			),
			'std'  => '',
			'id'   => 'bottom-background-size',
			'desc' => '',
		);

		/* Footer bottom */
		$of_options[] = array(  'name' => __( 'Footer', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Bottom Footer', 'ruby' ),
		);

		$of_options[] = array(  'name'   => __( 'Background color', 'ruby' ),
			'id'   => 'footer-background-color',
			'type' => 'color',
			'std'  => '#282828',
		);

		$of_options[] = array(  'name'   => __( 'Background image', 'ruby' ),
			'id'   => 'footer-background-image',
			'type' => 'upload',
			'std'  => '',
		);

		$of_options[] = array(  'name' => __( 'Background position?', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				''    => __( 'None', 'ruby' ),
				'left top'      => __( 'Left Top', 'ruby' ),
				'left center'   => __( 'Left Center', 'ruby' ),
				'left bottom'   => __( 'Left Bottom', 'ruby' ),
				'right top'     => __( 'Right Top', 'ruby' ),
				'right center'  => __( 'Right Center', 'ruby' ),
				'right bottom'  => __( 'Right Bottom', 'ruby' ),
				'center top'    => __( 'Center Top', 'ruby' ),
				'center center' => __( 'Center Center', 'ruby' ),
				'center bottom' => __( 'Center Bottom', 'ruby' ),
			),
			'std'  => '',
			'id'   => 'footer-background-position',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( 'Background repeat?', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				''    => __( 'None', 'ruby' ),
				'no-repeat' => __( 'No repeat', 'ruby' ),
				'repeat'    => __( 'Repeat', 'ruby' ),
				'repeat-x'  => __( 'Repeat X', 'ruby' ),
				'repeat-y'  => __( 'Repeat Y', 'ruby' ),
			),
			'std'  => '',
			'id'  => 'footer-background-repeat',
			'desc'  => '',
		);

		$of_options[] = array(  'name' => __( 'Background size?', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				''        => __( 'None', 'ruby' ),
				'auto'    => __( 'Auto', 'ruby' ),
				'cover'   => __( 'Cover', 'ruby' ),
				'contain' => __( 'Contain', 'ruby' ),
			),
			'std'  => '',
			'id'   => 'footer-background-size',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( 'Footer copyright text', 'ruby' ),
			'type' => 'textarea',
			'id'   => 'footer-copyright-text',
			'std'  => __( 'R-theme - Design copyright &#169; 2014 KingkongThemes&#174; All rights reserved.', 'ruby' ),
			'desc' => __( 'HTML and shortcodes are allowed.', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Show/Hide social icon', 'ruby' ),
			'id'   => 'footer-social',
			'type' => 'switch',
			'std'  => true,
		);

		/*-----------------------------------------------------------------------------------*/
		/* Offcanvas sidebar
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( 'Offcanvas sidebar', 'ruby' ),
			'type' => 'heading',
			'icon' => ADMIN_IMAGES . 'footer.png'
		);

		$of_options[] = array(  'name'   => __( 'Show/Hide Offcanvas sidebar', 'ruby' ),
			'id'   => 'offcanvas-turnon',
			'type' => 'switch',
			'std'  => true,
		);

		$of_options[] = array(  'name'   => __( 'Body swipe', 'ruby' ),
			'id'   => 'offcanvas-swipe',
			'type' => 'switch',
			'std'  => true,
		);

		$of_options[] = array(  'name' => __( 'Offcanvas sidebar position', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				'right'    => __( 'Right', 'ruby' ),
				'left'      => __( 'Left', 'ruby' ),
			),
			'std'  => '',
			'id'   => 'offcanvas-sidebar-position',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( 'Shown sidebar?', 'ruby' ),
			'type' => 'select',
			'options' => $sidebars,
			'id' => 'offcanvas-sidebar',
		);

		$of_options[] = array(  'name' => __( 'Background setting', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Background setting', 'ruby' ),
		);

		$of_options[] = array(  'name'   => __( 'Offcanvas sidebar background image', 'ruby' ),
			'id'   => 'offcanvas-sidebar-background-image',
			'type' => 'upload',
			'std'  => '',
		);

		$of_options[] = array(  'name' => __( 'Offcanvas sidebar background position?', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				''    => __( 'None', 'ruby' ),
				'left top'      => __( 'Left Top', 'ruby' ),
				'left center'   => __( 'Left Center', 'ruby' ),
				'left bottom'   => __( 'Left Bottom', 'ruby' ),
				'right top'     => __( 'Right Top', 'ruby' ),
				'right center'  => __( 'Right Center', 'ruby' ),
				'right bottom'  => __( 'Right Bottom', 'ruby' ),
				'center top'    => __( 'Center Top', 'ruby' ),
				'center center' => __( 'Center Center', 'ruby' ),
				'center bottom' => __( 'Center Bottom', 'ruby' ),
			),
			'std'  => '',
			'id'   => 'offcanvas-sidebar-background-position',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( 'Offcanvas sidebar background repeat?', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				''    => __( 'None', 'ruby' ),
				'no-repeat' => __( 'No repeat', 'ruby' ),
				'repeat'    => __( 'Repeat', 'ruby' ),
				'repeat-x'  => __( 'Repeat X', 'ruby' ),
				'repeat-y'  => __( 'Repeat Y', 'ruby' ),
			),
			'std'  => '',
			'id'  => 'offcanvas-sidebar-background-repeat',
			'desc'  => '',
		);

		$of_options[] = array(  'name' => __( 'Offcanvas sidebar background size?', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				''        => __( 'None', 'ruby' ),
				'auto'    => __( 'Auto', 'ruby' ),
				'cover'   => __( 'Cover', 'ruby' ),
				'contain' => __( 'Contain', 'ruby' ),
			),
			'std'  => '',
			'id'   => 'offcanvas-sidebar-background-size',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( 'Offcanvas sidebar background color', 'ruby' ),
			'type' => 'color',
			'id' => 'offcanvas-sidebar-background-color',
		);

		$of_options[] = array(  'name' => __( 'Offcanvas sidebar text color', 'ruby' ),
			'type' => 'color',
			'id' => 'offcanvas-sidebar-text-color',
		);

		$of_options[] = array(  'name' => __( 'Offcanvas sidebar custom css', 'ruby' ),
			'type' => 'textarea',
			'std'  => '',
			'id'   => 'offcanvas-sidebar-custom-css',
		);

		/*-----------------------------------------------------------------------------------*/
		/* Layout
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( 'Layout', 'ruby' ),
			'type' => 'heading',
			'icon' => ADMIN_IMAGES . 'layout.png'
		);
		/* Layout */
		$of_options[] = array(  'name' => __( 'Layout', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Layout', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Enable Boxed Layout', 'ruby' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => 'boxed-layout',
		);

		$of_options[] = array(  'name' => __( 'Content Width (px)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 1170,
			'min'  => 940,
			'max'  => 1200,
			'step' => 10,
			'id'   => 'use-content-width',
			'desc' => __( 'You can choose content width in the range from 940px to 1200px.', 'ruby' ),
		);

		/* Titlebar */
		$of_options[] = array(  'name' => __( 'Titlebar', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Titlebar', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Titlebar layout', 'ruby' ),
			'type' => 'select',
			'std'  => 'justify',
			'options' => array(
				'justify' => __( 'Justify', 'ruby' ),
				'center'  => __( 'Center', 'ruby' ),
			),
			'id' => 'titlebar-layout',
		);

		$of_options[] = array(  'name' => __( 'Top padding', 'ruby' ),
			'type' => 'text',
			'std'  => '',
			'id'   => 'pading-top',
			'desc' => __( 'Unit: px. Eg: 10px;', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Bottom padding', 'ruby' ),
			'type' => 'text',
			'std'  => '',
			'id'   => 'pading-bottom',
			'desc' => __( 'Unit: px. Eg: 10px;', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Background image', 'ruby' ),
			'type' => 'media',
			'std'  => '',
			'id'   => 'background-image',
		);

		$of_options[] = array(  'name' => __( 'Background color', 'ruby' ),
			'type' => 'color',
			'std'  => '',
			'id'   => 'background-color',
		);

		$of_options[] = array(  'name' => __( 'Background position', 'ruby' ),
			'type' => 'select',
			'std'  => 'left',
			'options' => array(
				'left top'      => __( 'Left Top', 'ruby' ),
				'left center'   => __( 'Left Center', 'ruby' ),
				'left bottom'   => __( 'Left Bottom', 'ruby' ),
				'right top'     => __( 'Right Top', 'ruby' ),
				'right center'  => __( 'Right Center', 'ruby' ),
				'right bottom'  => __( 'Right Bottom', 'ruby' ),
				'center top'    => __( 'Center Top', 'ruby' ),
				'center center' => __( 'Center Center', 'ruby' ),
				'center bottom' => __( 'Center Bottom', 'ruby' ),
			),
			'id' => 'background-position',
		);

		$of_options[] = array(  'name' => __( 'Background repeat', 'ruby' ),
			'type' => 'select',
			'std'  => 'repeat',
			'options' => array(
				'no-repeat' => __( 'No Repeat', 'ruby' ),
				'repeat'    => __( 'Repeat', 'ruby' ),
				'repeat-x'  => __( 'Repeat X', 'ruby' ),
				'repeat-y'  => __( 'Repeat Y', 'ruby' ),
			),
			'id' => 'background-repeat',
		);

		$of_options[] = array(  'name' => __( 'Background parallax', 'ruby' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => 'background-parallax',
		);

		$of_options[] = array(  'name' => __( 'Titlebar color', 'ruby' ),
			'type' => 'color',
			'std'  => '',
			'id'   => 'titlebar-title-color',
		);

		$of_options[] = array(  'name' => __( 'Titlebar shadow opacity', 'ruby' ),
			'type' => 'sliderui',
			'min'  => 0,
			'max'  => 10,
			'std'  => 5,
			'step' => 1,
			'id'   => 'titlebar-shadow-opacity',
		);

		$of_options[] = array(  'name' => __( 'Titlebar overlay opacity', 'ruby' ),
			'type' => 'sliderui',
			'min'  => 0,
			'max'  => 10,
			'std'  => 0,
			'step' => 1,
			'id'   => 'titlebar-overlay-opacity',
		);

		$of_options[] = array(  'name' => __( 'Titlebar clipmask opacity', 'ruby' ),
			'type' => 'sliderui',
			'min'  => 0,
			'max'  => 10,
			'std'  => 0,
			'step' => 1,
			'id'   => 'titlebar-clipmask-opacity',
		);

		$of_options[] = array(  'name' => __( 'Titlebar custom content', 'ruby' ),
			'type' => 'textarea',
			'std'  => '',
			'id'   => 'titlebar-custom-content',
		);

		/*-----------------------------------------------------------------------------------*/
		/* Styling
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( 'Style', 'ruby' ),
			'type' => 'heading',
			'icon' => ADMIN_IMAGES . 'icon-paint.png'
		);


		$of_options[] = array(  'name' => __( 'Primary Color', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Primary Color', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Primary color', 'ruby' ),
			'type' => 'color',
			'std'  => '#5ec5f4',
			'id'   => 'primary-color',
			'desc' => __( 'Primary color is the main color of site.', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Heading color', 'ruby' ),
			'type' => 'color',
			'std'  => '#4f4f4f',
			'id'   => 'heading-color',
			'desc' => __( '', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Text color', 'ruby' ),
			'type' => 'color',
			'std'  => '',
			'id'   => 'text-color',
			'desc' => __( '', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Footer background color', 'ruby' ),
			'type' => 'color',
			'std'  => '',
			'id'   => 'footer-bg-color',
			'desc' => __( '', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Links', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Links', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Link color', 'ruby' ),
			'type' => 'color',
			'std'  => '#5ec5f4',
			'id'   => 'link-color',
		);

		$of_options[] = array(  'name' => __( 'Link hover color', 'ruby' ),
			'type' => 'color',
			'std'  => '',
			'id'   => 'link-hover-color',
		);

		$of_options[] = array(  'name' => __( 'Footer color', 'ruby' ),
			'type' => 'color',
			'std'  => '',
			'id'   => 'footer-color',
		);

		$of_options[] = array(  'name' => __( 'Footer link color', 'ruby' ),
			'type' => 'color',
			'std'  => '',
			'id'   => 'footer-link-color',
		);

		$of_options[] = array(  'name' => __( 'Menu colors', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Menu colors', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Main menu color', 'ruby' ),
			'type' => 'color',
			'std'  => '#767676',
			'id'   => 'main-menu-color',
		);

		$of_options[] = array(  'name' => __( 'Sub menu color', 'ruby' ),
			'type' => 'color',
			'std'  => '#767676',
			'id'   => 'sub-menu-color',
		);


		/*-----------------------------------------------------------------------------------*/
		/* Typography
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( 'Typography', 'ruby' ),
			'type' => 'heading',
			'icon' => ADMIN_IMAGES . 'mac-smz-icon.gif'
		);

		$of_options[] = array(  'name' => __( 'Font family', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Font family', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Body font', 'ruby' ),
			'desc' => __( 'You can choose a normal font or Google font.', 'ruby' ),
			'id'   => 'body-font',
			'std'  => 'Roboto',
			'type' => 'select_google_font',
			'preview'  => array(
				'text' => 'This is the preview!', //this is the text from preview box
				'size' => '30px' //this is the text size from preview box
			),
			'options' => k2t_fonts_array(),
		);

		$of_options[] = array(  'name' => __( 'Heading font', 'ruby' ),
			'desc' => __( 'You can choose a normal font or Google font', 'ruby' ),
			'id'   => 'heading-font',
			'std'  => 'Raleway',
			'type' => 'select_google_font',
			'preview' => array(
				'text' => 'This is the preview!', //this is the text from preview box
				'size' => '30px' //this is the text size from preview box
			),
			'options' => k2t_fonts_array(),
		);

		$of_options[] = array(  'name' => __( 'Navigation font', 'ruby' ),
			'desc' => __( 'You can choose a normal font or Google font', 'ruby' ),
			'id'   => 'mainnav-font',
			'std'  => 'Raleway',
			'type' => 'select_google_font',
			'preview' => array(
				'text' => 'This is the preview!', //this is the text from preview box
				'size' => '30px' //this is the text size from preview box
			),
			'options' => k2t_fonts_array(),
		);

		$of_options[] = array(  'name' => __( 'General font size', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'General font size', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Body font size (px)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 14,
			'min'  => 8,
			'max'  => 28,
			'step' => 1,
			'id'   => 'body-size',
		);

		$of_options[] = array(  'name' => __( 'Main navigation font size (px)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 12,
			'min'  => 9,
			'max'  => 24,
			'step' => 1,
			'id'   => 'mainnav-size',
		);

		$of_options[] = array(  'name' => __( 'Submenu of Main navigation font size (px)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 11,
			'min'  => 9,
			'max'  => 24,
			'step' => 1,
			'id'   => 'submenu-mainnav-size',
		);

		$of_options[] = array(  'name' => __( 'Titlebar title font size (px)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 20,
			'min'  => 14,
			'max'  => 120,
			'step' => 1,
			'id'   => 'titlebar-title-size',
		);

		$of_options[] = array(  'name' => __( 'Headings font size', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Headings font size', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'H1 font size (px)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 32,
			'min'  => 32,
			'max'  => 80,
			'step' => 1,
			'id'   => 'h1-size',
		);

		$of_options[] = array(  'name' => __( 'H2 font size (px)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 24,
			'min'  => 24,
			'max'  => 32,
			'step' => 1,
			'id'   => 'h2-size',
		);

		$of_options[] = array(  'name' => __( 'H3 font size (px)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 18,
			'min'  => 18,
			'max'  => 32,
			'step' => 1,
			'id'   => 'h3-size',
		);

		$of_options[] = array(  'name' => __( 'H4 font size (px)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 16,
			'min'  => 8,
			'max'  => 32,
			'step' => 1,
			'id'   => 'h4-size',
		);

		$of_options[] = array(  'name' => __( 'H5 font size (px)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 14,
			'min'  => 8,
			'max'  => 32,
			'step' => 1,
			'id'   => 'h5-size',
		);

		$of_options[] = array(  'name' => __( 'H6 font size (px)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 12,
			'min'  => 8,
			'max'  => 32,
			'step' => 1,
			'id'   => 'h6-size',
		);

		$of_options[] = array(  'name' => __( 'Font type', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Font type', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Navigation text transform', 'ruby' ),
			'desc' => __( 'Select navigation text transform.', 'ruby' ),
			'type' => 'select',
			'id'   => 'mainnav-text-transform',
			'std'  => 'uppercase',
			'options' => array (
				'none'       => __( 'None', 'ruby' ),
				'capitalize' => __( 'Capitalize', 'ruby' ),
				'uppercase'  => __( 'Uppercase', 'ruby' ),
				'lowercase'  => __( 'Lowercase', 'ruby' ),
				'inherit'    => __( 'Inherit', 'ruby' ),
			),
		);

		$of_options[] = array(  'name' => __( 'Navigation font weight', 'ruby' ),
			'desc' => __( 'Select navigation font weight.', 'ruby' ),
			'type' => 'select',
			'id'   => 'mainnav-font-weight',
			'std'  => '400',
			'options' => array (
				'100' => __( '100', 'ruby' ),
				'200' => __( '200', 'ruby' ),
				'300' => __( '300', 'ruby' ),
				'400' => __( '400', 'ruby' ),
				'500' => __( '500', 'ruby' ),
				'600' => __( '600', 'ruby' ),
				'700' => __( '700', 'ruby' ),
				'800' => __( '800', 'ruby' ),
				'900' => __( '900', 'ruby' ),
			),
		);

		/*-----------------------------------------------------------------------------------*/
		/* Blog
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( 'Blog', 'ruby' ),
			'type' => 'heading',
			'icon' => ADMIN_IMAGES . 'icon-docs.png'
		);

		$of_options[] = array(  'name' => __( 'Blog layout', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Blog layout', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Blog style', 'ruby' ),
			'type' => 'select',
			'id'   => 'blog-style',
			'options' => array (
				'large'    => __( 'Large', 'ruby' ),
				'medium'   => __( 'Medium', 'ruby' ),
				'small'    => __( 'Small', 'ruby' ),
				'masonry'  => __( 'Masonry', 'ruby' ),
				'timeline' => __( 'Timeline', 'ruby' ),
			),
			'std'       => 'large',
			'desc'      => __( 'Select blog style.', 'ruby' ),
			'logicstic' => true,
		);

		$of_options[] = array(  'name' => __( 'Columns', 'ruby' ),
			'type'    => 'select',
			'id'      => 'blog-masonry-column',
			'options' => array (
				'column-2' => __( '2 Columns', 'ruby' ),
				'column-3' => __( '3 Columns', 'ruby' ),
				'column-4' => __( '4 Columns', 'ruby' ),
				'column-5' => __( '5 Columns', 'ruby' )
			),
			'std'  => 'column-3',
			'desc' => __( 'Select column for layout masonry.', 'ruby' ),
			'conditional_logic' => array(
				'field'    => 'blog-style',
				'value'    => 'masonry',
			),
		);

		$of_options[] = array(  'name' => __( 'Full width', 'ruby' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => 'blog-masonry-full-width',
			'desc' => __( 'Enable full width layout for masonry blog.', 'ruby' ),
			'conditional_logic' => array(
				'field'    => 'blog-style',
				'value'    => 'masonry',
			),
		);

		$of_options[] = array(  'name' => __( 'Blog sidebar position', 'ruby' ),
			'type'    => 'select',
			'id'      => 'blog-sidebar-position',
			'options' => array (
				'right_sidebar' => __( 'Right Sidebar', 'ruby' ),
				'left_sidebar'  => __( 'Left Sidebar', 'ruby' ),
				'two_sidebars'  => __( 'Two Sidebars', 'ruby' ),
				'no_sidebar'    => __( 'No Sidebar', 'ruby' )
			),
			'std'  => 'right_sidebar',
			'desc' => __( 'Select blog sidebar position.', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Blog Options', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Blog Options', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Content or excerpt', 'ruby' ),
			'type'    => 'select',
			'id'      => 'blog-display',
			'options' => array (
				'excerpts' => __( 'Excerpt', 'ruby' ),
				'contents' => __( 'Content', 'ruby' ) ),
			'std'  => 'excerpt',
			'desc' => __( 'Select display post content or excerpt on the blog.', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Excerpt length (words)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 40,
			'step' => 1,
			'min'  => 10,
			'max'  => 80,
			'id'   => 'excerpt-length',
		);

		$of_options[] = array(  'name' => __( 'Infinite Scroll', 'ruby' ),
			'type'    => 'select',
			'id'      => 'pagination-type',
			'options' => array (
				'pagination_number' => __( 'False', 'ruby' ),
				'pagination_ajax'   => __( 'True', 'ruby' )
			),
			'std' => 'pagination_number'
		);

		$of_options[] = array(  'name' => __( 'Blog timeline image', 'ruby' ),
			'type' => 'media',
			'std'  => '',
			'id'   => 'blog-timeline-image',
			'desc' => __( 'Only support Blog Timeline style', 'ruby' ),
			'conditional_logic' => array(
				'field'    => 'blog-style',
				'value'    => 'timeline',
			),
		);

		$of_options[] = array(  'name' => __( 'Show/Hide categories filter', 'ruby' ),
			'type' => 'switch',
			'id'   => 'blog-categories-filter',
			'std'  => true
		);

		$of_options[] = array(  'name' => __( 'Show/Hide title link?', 'ruby' ),
			'type' => 'switch',
			'id'   => 'blog-post-link',
			'std'  => true
		);

		$of_options[] = array(  'name' => __( 'Show/Hide post date', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-date',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide the number of comments', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-number-comment',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide categories', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-categories',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide author', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-author',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide excerpt', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-excerpt',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide "Reamore" link', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-readmore',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide tags', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-tags',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide social share', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-social-share',
		);

		/* Social Impact */
		$of_options[] = array(  'name' => __( 'Social', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Social', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Show/Hide social buttons', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-social',
			'desc' => __( 'Turn it OFF if you do not want to display social buttons', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Facebook?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-social-facebook',
		);

		$of_options[] = array(  'name' => __( 'Twitter?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-social-twitter',
		);

		$of_options[] = array(  'name' => __( 'Google+?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-social-google-plus',
		);

		$of_options[] = array(  'name' => __( 'Linkedin?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-social-linkedin',
		);

		$of_options[] = array(  'name' => __( 'Tumblr?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-social-tumblr',
		);

		$of_options[] = array(  'name' => __( 'Email?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'blog-social-email',
		);

		/*-----------------------------------------------------------------------------------*/
		/* Single
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( 'Single', 'ruby' ),
			'type' => 'heading',
			'icon' => ADMIN_IMAGES . 'icon-edit.png'
		);

		/* Featured Image */
		$of_options[] = array(  'name' => __( 'Single Post Layout', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Single Post Layout', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Single post layout', 'ruby' ),
			'type' => 'select',
			'id'   => 'single-layout',
			'options' => array (
				'right_sidebar' => __( 'Right Sidebar (default)', 'ruby' ),
				'left_sidebar'  => __( 'Left Sidebar', 'ruby' ),
				'no_sidebar'    => __( 'No Sidebar', 'ruby' ) ),
			'std'  => 'right_sidebar',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( 'Single custom sidebar', 'ruby' ),
			'type' => 'text',
			'std'  => '',
			'id'   => 'single-custom-sidebar',
			'desc'   => '',
		);

		/* Meta */
		$of_options[] = array(  'name' => __( 'Meta', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Meta', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Show/Hide Categories', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-categories',
			'desc' => __( 'Turn OFF if you don\'t want to display categories on single post', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Show/Hide post date', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-post-date',
			'desc' => __( 'Turn OFF if you don\'t want to display post date on single post', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Show/Hide Next / Previous links?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-nav',
			'desc' => __( 'Turn OFF if you don\'t want to display post navigation links on single post', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Show/Hide tags', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-tags',
			'desc' => __( 'Turn OFF if you don\'t want to display post tags on single post', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Show/Hide authorbox', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-authorbox',
			'desc' => __( 'Turn OFF if you don\'t want to display author box on single post', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Show/Hide related post', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-related-post',
			'desc' => __( 'Turn OFF if you don\'t want to display related post on single post', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Related post title', 'ruby' ),
			'type' => 'text',
			'std'  => 'You may also like',
			'id'   => 'single-related-post-title',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide comment form', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-commnet-form',
			'desc' => __( 'Turn OFF if you don\'t want to display comment form on single post', 'ruby' ),
		);

		/* Titlebar */
		$of_options[] = array(  'name' => __( 'Titlebar', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Titlebar', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Titlebar layout', 'ruby' ),
			'type' => 'select',
			'std'  => 'justify',
			'options' => array(
				'justify' => __( 'Justify', 'ruby' ),
				'center'  => __( 'Center', 'ruby' ),
			),
			'id' => 'single-titlebar-layout',
		);

		$of_options[] = array(  'name' => __( 'Top padding', 'ruby' ),
			'type' => 'text',
			'std'  => '',
			'id'   => 'single-pading-top',
			'desc' => __( 'Unit: px. Eg: 10px;', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Bottom padding', 'ruby' ),
			'type' => 'text',
			'std'  => '',
			'id'   => 'single-pading-bottom',
			'desc' => __( 'Unit: px. Eg: 10px;', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Background image', 'ruby' ),
			'type' => 'media',
			'std'  => '',
			'id'   => 'single-background-image',
		);

		$of_options[] = array(  'name' => __( 'Background color', 'ruby' ),
			'type' => 'color',
			'std'  => '',
			'id'   => 'single-background-color',
		);

		$of_options[] = array(  'name' => __( 'Background repeat', 'ruby' ),
			'type' => 'select',
			'std'  => 'no-repeat',
			'options' => array(
				'no-repeat' => __( 'No Repeat', 'ruby' ),
				'repeat'    => __( 'Repeat', 'ruby' ),
				'repeat-x'  => __( 'Repeat X (width)', 'ruby' ),
				'repeat-y'  => __( 'Repeat Y (height)', 'ruby' ),
			),
			'id' => 'single-background-repeat',
		);

		$of_options[] = array(  'name' => __( 'Background position', 'ruby' ),
			'type' => 'select',
			'std'  => 'left top',
			'options' => array(
				'left top'      => __( 'Left Top', 'ruby' ),
				'left center'   => __( 'Left Center', 'ruby' ),
				'left bottom'   => __( 'Left Bottom', 'ruby' ),
				'right top'     => __( 'Right Top', 'ruby' ),
				'right center'  => __( 'Right Center', 'ruby' ),
				'right bottom'  => __( 'Right Bottom', 'ruby' ),
				'center top'    => __( 'Center Top', 'ruby' ),
				'center center' => __( 'Center Center', 'ruby' ),
				'center bottom' => __( 'Center Bottom', 'ruby' ),
			),
			'id' => 'single-background-position',
		);

		$of_options[] = array(  'name' => __( 'Background parallax', 'ruby' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => 'single-background-parallax',
		);

		$of_options[] = array(  'name' => __( 'Titlebar shadow opacity', 'ruby' ),
			'type' => 'sliderui',
			'min'  => 0,
			'max'  => 10,
			'std'  => 5,
			'step' => 1,
			'id'   => 'single-titlebar-shadow-opacity',
		);

		$of_options[] = array(  'name' => __( 'Titlebar overlay opacity', 'ruby' ),
			'type' => 'sliderui',
			'min'  => 0,
			'max'  => 10,
			'std'  => 0,
			'step' => 1,
			'id'   => 'single-titlebar-overlay-opacity',
		);

		$of_options[] = array(  'name' => __( 'Titlebar clipmask opacity', 'ruby' ),
			'type' => 'sliderui',
			'min'  => 0,
			'max'  => 10,
			'std'  => 0,
			'step' => 1,
			'id'   => 'single-titlebar-clipmask-opacity',
		);

		$of_options[] = array(  'name' => __( 'Titlebar custom content', 'ruby' ),
			'type' => 'textarea',
			'std'  => '',
			'id'   => 'single-titlebar-custom-content',
		);

		/* Social Impact */
		$of_options[] = array(  'name' => __( 'Social', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Social', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Show/Hide social buttons', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-social',
			'desc' => __( 'Turn it OFF if you do not want to display social buttons', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Facebook?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-social-facebook',
		);

		$of_options[] = array(  'name' => __( 'Twitter?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-social-twitter',
		);

		$of_options[] = array(  'name' => __( 'Google+?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-social-google-plus',
		);

		$of_options[] = array(  'name' => __( 'Linkedin?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-social-linkedin',
		);

		$of_options[] = array(  'name' => __( 'Tumblr?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-social-tumblr',
		);

		$of_options[] = array(  'name' => __( 'Email?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'single-social-email',
		);

		/*-----------------------------------------------------------------------------------*/
		/* Portfolio
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( 'Portfolio', 'ruby' ),
			'type' => 'heading',
			'icon' => ADMIN_IMAGES . 'work.png'
		);

		/* Portfolio Category */
		$of_options[] = array(  'name' => __( 'Portfolio Category', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Portfolio Category', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Portfolio category Slug', 'ruby' ),
			'type' => 'text',
			'std'  => '',
			'id'   => 'portfolio-category-slug',
		);

		$of_options[] = array(  'name' => __( 'Layout', 'ruby' ),
			'type' => 'select',
			'options' => array(
				'right_sidebar' => __( 'Right Sidebar', 'ruby' ),
				'left_sidebar'  => __( 'Left Sidebar', 'ruby' ),
				'no_sidebar'    => __( 'Fullwidth', 'ruby' ),
				'full_width'    => __( '100% Width', 'ruby' ),
			),
			'std' => 'right_sidebar',
			'id'  => 'portfolio-category-layout',
		);

		$of_options[] = array(  'name' => __( 'Sidebar name', 'ruby' ),
			'type' => 'text',
			'std'  => '',
			'id'   => 'portfolio-custom-sidebar',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide titlebar', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'portfolio-display-titlebar',
		);

		$of_options[] = array(  'name' => __( 'Style', 'ruby' ),
			'type' => 'select',
			'options' => array(
				'gallery_masonry' => __( 'Gallery Masonry', 'ruby' ),
				'gallery_grid'    => __( 'Gallery Grid', 'ruby' ),
				'text_masonry'    => __( 'Text Masonry', 'ruby' ),
				'text_grid'       => __( 'Text Grid', 'ruby' ),
			),
			'std' => 'gallery_masonry',
			'id'  => 'portfolio-category-style',
		);

		$of_options[] = array(  'name' => __( 'Child Style', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				'none'                       => __( 'None', 'ruby' ),
				'gallery_masonry_free_style' => __( 'Gallery masonry free style', 'ruby' ),
				'gallery_grid_no_padding'    => __( 'Gallery grid no padding', 'ruby' ),
			),
			'std' => 'none',
			'id'  => 'portfolio-category-child-style',
		);

		$of_options[] = array(  'name' => __( 'Column?', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				'2_columns'  => __( '2 columns', 'ruby' ),
				'3_columns'  => __( '3 columns', 'ruby' ),
				'4_columns'  => __( '4 columns', 'ruby' ),
				'5_columns'  => __( '5 columns', 'ruby' ),
			),
			'std' => '3_columns',
			'id'  => 'portfolio-category-column',
		);

		$of_options[] = array(  'name' => __( 'Number of projects per page', 'ruby' ),
			'type' => 'text',
			'std'  => 9,
			'id'   => 'portfolio-category-number',
			'desc' => __( 'Fill out -1 if you want to display ALL projects.', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Display Portfolio Filter?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'portfolio-display-filter',
		);

		$of_options[] = array(  'name' => __( 'Portfolio Infinite Scroll', 'ruby' ),
			'type'    => 'select',
			'options' => array(
				'pagination_number' => __( 'False', 'ruby' ),
				'pagination_ajax'   => __( 'True', 'ruby' ),
			),
			'std' => 'pagination_number',
			'id'  => 'portfolio-category-pagination-type',
		);

		/* Single Portfolio */
		$of_options[] = array(  'name' => __( 'Single Portfolio', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Single Portfolio', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Portfolio Slug', 'ruby' ),
			'type' => 'text',
			'std'  => '',
			'id'   => 'portfolio-slug',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide related portfolio', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'portfolio-related-post',
			'desc' => __( 'Turn OFF if you don\'t want to display related post on single portfolio', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Related portfolio title', 'ruby' ),
			'type' => 'text',
			'std'  => 'Related Projects',
			'id'   => 'portfolio-related-post-title',
			'desc' => '',
		);
		$of_options[] = array(  'name' => __( 'Related portfolio sub title', 'ruby' ),
			'type' => 'text',
			'std'  => '',
			'id'   => 'single-related-post-sub-title',
			'desc' => '',
		);

		/*-----------------------------------------------------------------------------------*/
		/* WooCommerce
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( 'WooCommerce', 'ruby' ),
			'type' => 'heading',
			'icon' => ADMIN_IMAGES . 'cart.png'
		);

		/* Menu */
		$of_options[] = array(  'name' => __( 'Cart menu icon', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Cart menu icon', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Should Empty Cart Link To?', 'ruby' ),
			'type' => 'select',
			'std'  => 'cart',
			'options' => array(
				'cart'   => __( 'Link to Cart page', 'ruby' ),
				'shop'   => __( 'Link to Shop page', 'ruby' ),
				'custom' => __( 'Link to a custom URL', 'ruby' ),
				'hide'   => __( 'Not display', 'ruby' ),
			),
			'id' => 'shop-cart-menu-empty',
		);

		$of_options[] = array(  'name' => __( 'Custom link when cart is empty', 'ruby' ),
			'type' => 'text',
			'std'  => '',
			'id'   => 'shop-cart-menu-empty-link',
			'desc' => __( 'If left blank, it\'ll link to the shop page. If the cart is not empty, it\'ll link to the Cart page.', 'ruby' ),
		);
		/* Product hover effect */
		$of_options[] = array(  'name' => __( 'Product hover effect', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Product hover effect', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Choose hover effect product?', 'ruby' ),
			'type' => 'select',
			'std'  => 'fade',
			'options' => array(
				'fade'  => __( 'Fade', 'ruby' ),
				'slide' => __( 'Slide', 'ruby' ),
			),
			'id' => 'product-hover-effect',
		);

		/* Shop Archive Page */
		$of_options[] = array(  'name' => __( 'Shop Archive', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Shop Archive', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Shop Archive Template', 'ruby' ),
			'type' => 'select',
			'std'  => 'right_sidebar',
			'options' => array(
				'right_sidebar' => __( 'Right Sidebar', 'ruby' ),
				'left_sidebar'  => __( 'Left Sidebar', 'ruby' ),
				'no_sidebar'    => __( 'No Sidebar', 'ruby' ),
			),
			'id' => 'shop-template',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide WooCommerce Breadcrumb', 'ruby' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => 'shop-breadcrumb',
		);

		$of_options[] = array(  'name' => __( 'Show "sorting"?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'shop-display-sorting',
		);

		$of_options[] = array(  'name' => __( 'Show "result count"?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'shop-display-result-count',
		);

		$of_options[] = array(  'name' => __( 'Number of columns (default)', 'ruby' ),
			'type' => 'sliderui',
			'std'  => 3,
			'min'  => 3,
			'max'  => 4,
			'id'   => 'shop-products-column',
		);

		$of_options[] = array(  'name' => __( 'Number of products per page', 'ruby' ),
			'type' => 'text',
			'std'  => '',
			'id'   => 'shop-products-per-page',
			'desc' => __( 'Fill it -1 if you want to display all products.', 'ruby' ),
		);

		/* Single Product */
		$of_options[] = array(  'name' => __( 'Single Product', 'ruby' ),
			'type' => 'info',
			'std'  => __( 'Single Product', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Template', 'ruby' ),
			'type' => 'select',
			'std'  => 'right_sidebar',
			'options' => array(
				'right_sidebar' => __( 'Right Sidebar', 'ruby' ),
				'left_sidebar'  => __( 'Left Sidebar', 'ruby' ),
				'no_sidebar'    => __( 'No Sidebar', 'ruby' ),
			),
			'id' => 'shop-single-template',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide share your products?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'shop-single-display-share-products',
		);

		$of_options[] = array(  'name' => __( 'Show/Hide related products?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'shop-single-display-related-products',
			'logicstic' => true,
		);

		$of_options[] = array(  'name' => __( 'Number of related products to show on desktop', 'ruby' ),
			'type' => 'sliderui',
			'min'  => 2,
			'max'  => 5,
			'std'  => 4,
			'step' => 1,
			'id'   => 'shop-related-products-number',
			'conditional_logic' => array(
				'field' => 'shop-single-display-related-products',
				'value' => 'switch-1',
			),
		);

		$of_options[] = array(  'name' => __( 'Number of related products to show on tablets (<=768px)', 'ruby' ),
			'type' => 'sliderui',
			'min'  => 1,
			'max'  => 3,
			'std'  => 2,
			'step' => 1,
			'id'   => 'shop-related-products-number-tablet',
			'conditional_logic' => array(
				'field' => 'shop-single-display-related-products',
				'value' => 'switch-1',
			),
		);

		$of_options[] = array(  'name' => __( 'Number of related products to show on mobile (<=320px)', 'ruby' ),
			'type' => 'sliderui',
			'min'  => 1,
			'max'  => 3,
			'std'  => 1,
			'step' => 1,
			'id'   => 'shop-related-products-number-mobile',
			'conditional_logic' => array(
				'field' => 'shop-single-display-related-products',
				'value' => 'switch-1',
			),
		);

		/*-----------------------------------------------------------------------------------*/
		/* 404 Page
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( '404 Page', 'ruby' ),
			'type' => 'heading',
			'icon' => ADMIN_IMAGES . '404.png'
		);

		$of_options[] = array(  'name' => __( '404 page', 'ruby' ),
			'type' => 'info',
			'std'  => __( '404 page', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( '404 Title', 'ruby' ),
			'type' => 'text',
			'std'  => 'Opps! Something went wrong...',
			'id'   => '404-title',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( '404 Image', 'ruby' ),
			'type' => 'media',
			'std'  => get_template_directory_uri() . '/assets/img/icons/404.png',
			'id'   => '404-image',
			'desc' => '',
		);

		$of_options[] = array(  'name' => __( '404 Custom Text', 'ruby' ),
			'type' => 'textarea',
			'id'   => '404-text',
			'std'  => __( 'Page not found. Please continue to our <a href="' . esc_url ( home_url() ) . '" rel="home">Home page</a>.', 'ruby' ),
			'desc' => __( 'The message you want to show when they got to 404 page. HTML is allowed.', 'ruby' ),
		);

		/*-----------------------------------------------------------------------------------*/
		/* Social Icons
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name' => __( 'Social', 'ruby' ),
			'type'  => 'heading',
			'icon'  => ADMIN_IMAGES . 'twitter.png'
		);

		$of_options[] = array(  'name' => __( 'Target', 'ruby' ),
			'type' => 'select',
			'std'  => '_blank',
			'options' => array(
				'_self'  => __( 'Same tab', 'ruby' ),
				'_blank' => __( 'New tab', 'ruby' ),
			),
			'id' => 'social-target',
		);

		$of_options[] = array(  'name' => __( 'Twitter username?', 'ruby' ),
			'type' => 'text',
			'std'  => 'themelead',
			'id'   => 'twitter-username',
			'desc' => __( 'Twitter username used for tweet share buttons.', 'ruby' ),
		);

		$of_options[] = array(  'name' => __( 'Icon title?', 'ruby' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => 'social-title',
			'desc' => __( 'Turn it ON if you want to display social icon titles like Facebook, Google+, Twitter... when hover icons.', 'ruby' ),
		);


		foreach ( k2t_social_array() as $s => $c ):

			$of_options[] = array(  'name' => $c,
				'type' => 'text',
				'std'  => '',
				'id'   => 'social-' . $s,
			);

		endforeach;

		/*-----------------------------------------------------------------------------------*/
		/* Backup Options */
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name'   => __( 'Backup Options', 'ruby' ),
			'type'  => 'heading',
			'icon'  => ADMIN_IMAGES . 'icon-slider.png'
		);

		$of_options[] = array(  'name'   => __( 'Backup and Restore Options', 'ruby' ),
			'id'   => 'of_backup',
			'std'  => '',
			'type' => 'backup',
			'desc' => __( 'You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need back.', 'ruby' ),
		);

		$of_options[] = array(  'name'   => __( 'Transfer Theme Options Data', 'ruby' ),
			'id'   => 'of_transfer',
			'std'  => '',
			'type' => 'transfer',
			'desc' => __( 'You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another installation, replace the data in the text box with the one from another installation and click "Import Options".', 'ruby' ),
		);
		
		/*-----------------------------------------------------------------------------------*/
		/* One Click Install */
		/*-----------------------------------------------------------------------------------*/
		$of_options[] = array(  'name'   => __( 'One Click Install', 'ruby' ),
			'type'  => 'heading',
			'icon'  => ADMIN_IMAGES . 'icon-slider.png'
		);
		$of_options[] = array(  'name'   => __( 'Transfer Theme Options Data', 'ruby' ),
			'id'   => 'k2t_advance_backup',
			'std'  => '',
			'type' => 'k2t_advance_backup',
		);
		
		// $of_options[] = array(  'name'   => __( 'Sample theme options', 'ruby' ),
		// 	'id'   => 'get_theme_option_widget',
		// 	'std'  => '',
		// 	'type' => 'get_theme_option_widget',
		// );

	}//End function: of_options()
}//End check if function exists: of_options()
?>
