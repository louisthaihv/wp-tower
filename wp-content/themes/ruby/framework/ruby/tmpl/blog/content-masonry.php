<?php
/**
 * The template for displaying content masonry.
 *
 * @package Ruby
 * @author  KingKongThemes
 * @link	http://www.kingkongthemes.com
 */

// Get theme options
global $smof_data, $post;

// Get post format
$post_format = get_post_format();
$link        = ( function_exists( 'get_field' ) ) ? get_field( 'link_format_url', get_the_ID() ) : '';
$large       = ( function_exists( 'get_field' ) ) ? get_field( 'post_large', get_the_ID() ) : '';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $large ); ?>>
	
	<?php
		include get_template_directory() . '/framework/ruby/tmpl/blog/post-format.php';

		if ( 'quote' != $post_format ) :
	?>
	
		<div class="k2t-text">

			<div class="k2t-meta">
				<?php
				if ( 'link' == $post_format ) {
					the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( $link ) ), '</a></h2>' );
				} else {
					if ( $smof_data['blog-post-link'] ) {
						the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
					}
				}

				if ( $smof_data['blog-author'] ) { ?>
					<div class="post-author">
						<?php echo sprintf( __( '<span>%s</span> / ', 'ruby' ), get_the_author_link() );?>
					</div>
				<?php } 

				if ( $smof_data['blog-date'] ) { ?>
					<div class="posted-on">
						<?php the_time( 'j M Y' ); ?>
					</div>
				<?php } ?>
			</div><!-- .k2t-meta -->

			<div class="k2t-entry">
				<?php
					if ( 'excerpts' == $smof_data['blog-display'] ) {
						echo $trimmed_content = wp_trim_words( get_the_content(), $smof_data['excerpt-length'], '<a class="more-link" href="'. get_permalink() .'">Read More</a>' );
					} else {
						if ( $smof_data['blog-readmore'] ) {
							the_content( sprintf( __( 'Read more', 'ruby' ) ) );
						} else {
							the_content( sprintf( __( ' ', 'ruby' ) ) );
						}	
					}
				?>
			</div><!-- .k2t-entry -->

		</div>

	<?php endif; ?>

</article><!-- #post-## -->