<?php
/**
 * The template for displaying content small image thumbnail.
 *
 * @package Ruby
 * @author  KingKongThemes
 * @link	http://www.kingkongthemes.com
 */

// Get theme options
global $smof_data;

// Get post format
$post_format = get_post_format();
$link        = ( function_exists( 'get_field' ) ) ? get_field( 'link_format_url', get_the_ID() ) : '';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		include get_template_directory() . '/framework/ruby/tmpl/blog/post-format.php';

		if ( 'quote' != $post_format ) :
	?>

		<div class="k2t-entry">
			<div class="k2t-meta">
				<?php if ( $smof_data['blog-author'] ) { ?>
					<div class="post-author">
						<?php echo sprintf( __( 'Posted by <span>%s</span>', 'ruby' ), get_the_author_link() );?>
					</div>
				<?php }

				if ( $smof_data['blog-date'] ) { ?>
					<div class="posted-on">
						<i class="fa fa-clock-o"></i><?php the_time( 'j M Y' ); ?>
					</div>
				<?php }

				if ( $smof_data['blog-number-comment'] ) {
					if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
						<div class="post-comment">
							<a href="<?php comments_link(); ?>"><i class="fa fa-comments"></i><?php comments_number( '0 Comment', '1 Comment', '% Comments' ); ?></a>
						</div>
				<?php
					endif;
				}

				?>
			</div><!-- .k2t-meta -->

			<?php 
				if ( 'link' == $post_format ) {
					the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( $link ) ), '</a></h2>' );
				} else {
					if ( $smof_data['blog-post-link'] ) {
						the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
					}
				} // End format = link

				if ( 'excerpts' == $smof_data['blog-display'] ) {
					echo $trimmed_content = wp_trim_words( get_the_content(), $smof_data['excerpt-length'], '<a class="more-link" href="' . esc_url( get_permalink() ) . '">Read More</a>' );
				} else {
					if ( $smof_data['blog-readmore'] ) {
						the_content( sprintf( __( 'Read moreK2T', 'ruby' ) ) );
					} else {
						the_content( sprintf( __( ' ', 'ruby' ) ) );
					}
				}
			?>
		</div><!-- .k2t-entry -->
	<?php endif; ?>

</article><!-- #post-## -->
