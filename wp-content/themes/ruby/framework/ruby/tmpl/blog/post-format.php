<?php
/**
 * The template for displaying post formats.
 *
 * @package Ruby
 * @author  KingKongThemes
 * @link	http://www.kingkongthemes.com
 */

// Get theme options
global $smof_data, $post;

// Get blog style
$blog_style = $smof_data['blog-style'];

// Get post format
$post_format = get_post_format();

// Post format video
$video_source       = ( function_exists( 'get_field' ) ) ? get_field( 'video_format_source', get_the_ID() ) : '';
$video_source_link  = ( function_exists( 'get_field' ) ) ? get_field( 'video_url', get_the_ID() ) : '';
$video_source_embed = ( function_exists( 'get_field' ) ) ? get_field( 'video_code', get_the_ID() ) : '';
$video_source_local = ( function_exists( 'get_field' ) ) ? get_field( 'video_local', get_the_ID() ) : '';

// Post format audio
$audio_source       = ( function_exists( 'get_field' ) ) ? get_field( 'audio_format_source', get_the_ID() ) : '';
$audio_source_link  = ( function_exists( 'get_field' ) ) ? get_field( 'audio_url', get_the_ID() ) : '';
$audio_source_local = ( function_exists( 'get_field' ) ) ? get_field( 'audio_local', get_the_ID() ) : '';

// Post format gallery
$post_gallery = ( function_exists( 'get_field' ) ) ? get_field( 'post_gallery', get_the_ID() ) : array();
$auto_play    = ( function_exists( 'get_field' ) ) ? get_field( 'gallery_auto', get_the_ID() ) : '';
$duration     = ( function_exists( 'get_field' ) ) ? get_field( 'gallery_auto_time_wait', get_the_ID() ) : '';
$speed        = ( function_exists( 'get_field' ) ) ? get_field( 'gallery_speed', get_the_ID() ) : '';
$pagination   = ( function_exists( 'get_field' ) ) ? get_field( 'gallery_pagination', get_the_ID() ) : '';
$navigation   = ( function_exists( 'get_field' ) ) ? get_field( 'gallery_navigation', get_the_ID() ) : '';
$mouse        = ( function_exists( 'get_field' ) ) ? get_field( 'gallery_mousewheel', get_the_ID() ) : '';
if ( $post_format == 'gallery' && count( $post_gallery ) > 0 && is_array( $post_gallery ) ) {
	wp_enqueue_script( 'k2t-owlcarousel' );
	$script = '
		<scr' . 'ipt>
			(function($) {
				"use strict";
				$(document).ready(function() {
					$(".k2t-thumb-gallery").owlCarousel({
						singleItem 		: true,
						pagination 		: ' . $pagination . ',
						navigation 		: ' . $navigation . ',
						slideSpeed 		: ' . $speed . ',
						rewindSpeed 	: ' . $duration . ',
						autoPlay 		: ' . $auto_play . ',
						navigationText	: [
							"<i class=\"fa fa-angle-left\"></i>",
							"<i class=\"fa fa-angle-right\"></i>"
						],
					});
				});
			})(jQuery);
		</scr' . 'ipt>
	';
}

// Post format quote
$quote_author  = ( function_exists( 'get_field' ) ) ? get_field( 'quote_author', get_the_ID() ) : '';
$quote_link    = ( function_exists( 'get_field' ) ) ? get_field( 'author_quote_url', get_the_ID() ) : '';
$quote_content = ( function_exists( 'get_field' ) ) ? get_field( 'quote_content', get_the_ID() ) : '';
?>
<div class="k2t-thumb">
	<?php
		switch ( $post_format ) :
			case 'video':
				if ( 'link' == $video_source ) :
					echo do_shortcode( '[vc_video link="' . esc_url( $video_source_link ) . '"/]' );
				elseif ( 'embed' == $video_source ) :
					echo $video_source_embed;
				elseif ( 'local' == $video_source ) :
					echo do_shortcode('[video src="' . esc_url( $video_source_local['url'] ) . '"/]');
				else :
					echo '<img src="' . get_template_directory_uri() . '/assets/img/placeholder/800x350.png" alt="' . get_the_title() . '" />';
				endif;
			break;

			case 'audio':
				if ( 'link' == $audio_source ) :
					global $wp_embed;
						$media_result = $wp_embed->run_shortcode( '[embed]' . esc_url( $audio_source_link ) . '[/embed]' );
					echo $media_result;
				elseif ( 'local' == $audio_source ) :
					echo do_shortcode('[audio src="' . esc_url( $audio_source_local['url'] ) . '"/]');
				else :
					echo '<img src="' . get_template_directory_uri() . '/assets/img/placeholder/800x350.png" alt="' . get_the_title() . '" />';
				endif;
			break;

			case 'gallery':
				if ( count( $post_gallery ) > 0 && is_array( $post_gallery ) ) :
					echo $script;
					echo '<div class="k2t-thumb-gallery">';
						foreach ( $post_gallery as $slide ):

							if ( is_array( $slide ) && ! empty( $slide['ID'] ) ) : $image = wp_get_attachment_image( $slide['ID'], 'thumb_800x350' ); ?>
								<div class="item"> 
									<?php echo $image; ?>
								</div>

							<?php elseif ( ! empty( $slide ) ) : $image = wp_get_attachment_image( $slide, 'thumb_800x350' ); ?>
								<div class="item"> 
									<?php echo $image; ?>
								</div>
							<?php endif;

						endforeach;
					echo '</div>';
				else :
					echo '<img src="' . get_template_directory_uri() . '/assets/img/placeholder/800x350.png" alt="' . get_the_title() . '" />';
				endif;
			break;

			case 'quote': ?>
				<div class="k2t-thumb-quote">
					<div class="quote-content">
						<i class="fa fa-quote-left"></i>
						<?php
							if ( ! empty( $quote_content ) ) :
								echo $quote_content;
							else :
								the_content();
							endif;
						?>
					</div><!-- .quote-content -->
					<div class="quote-author">
						<a href="<?php echo esc_url( $quote_link ) ?>"><?php echo $quote_author ?></a>
					</div><!-- .quote-author -->
				</div><!-- .k2t-thumb-quote -->
			<?php
			break;

		?>
			
		<?php 
			break;
		default: ?>
			
				<?php
					if ( 'large' == $blog_style || 'timeline' == $blog_style ) {
						if ( has_post_thumbnail() ) :
							the_post_thumbnail( 'thumb_800x350' );
						else :
							echo '<img src="' . get_template_directory_uri() . '/assets/img/placeholder/800x350.png" alt="' . get_the_title() . '" />';
						endif;
					} elseif ( 'medium' == $blog_style || 'small' == $blog_style ) {
						$thumbnail = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );
						$image     = aq_resize( $thumbnail, 250, 250, true );
						if ( has_post_thumbnail() ) :
							echo '<img src="' . esc_url( $image ) . '" alt="' . get_the_title() . '" />';
						else :
							echo '<img src="' . get_template_directory_uri() . '/assets/img/placeholder/250x250.png" alt="' . get_the_title() . '" />';
						endif;
					} else {
						if ( has_post_thumbnail() ) :
							the_post_thumbnail();
						endif;
					}
				?>
				<?php if ( !is_single() ) : ?>
				<div class="mask"><a href="<?php esc_url( the_permalink() ); ?>"><i class="fa fa-link"></i></a></div>
				<?php endif; ?>
	<?php break;
	endswitch; ?>
</div><!-- .post-format -->