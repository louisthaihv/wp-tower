<?php
/**
 * Alters the default functionality of the VC.
 *
 * @package Ruby
 * @author  K2T Team
 * @link	http://www.kingkongthemes.com
 */

/**
 * Removes tabs such as the "Design Options" from the Visual Composer Settings
 *
 * @package Ruby
 */
if ( class_exists( 'Vc_Manager' ) ) :
	vc_set_as_theme( true );
endif;

/*-------------------------------------------------------------------
	Map for Visual Composer Shortcode.
--------------------------------------------------------------------*/
if ( class_exists( 'Vc_Manager' ) ) :
	if ( ! function_exists( 'k2t_vc_map_shortcodes' ) ) :

		function k2t_vc_map_shortcodes() {

			$k2t_icon = array( '', 'fa fa-glass', 'fa fa-music', 'fa fa-search', 'fa fa-envelope-o', 'fa fa-heart', 'fa fa-star', 'fa fa-star-o', 'fa fa-user', 'fa fa-film', 'fa fa-th-large', 'fa fa-th', 'fa fa-th-list', 'fa fa-check', 'fa fa-remove', 'fa fa-close', 'fa fa-times', 'fa fa-search-plus', 'fa fa-search-minus', 'fa fa-power-off', 'fa fa-signal', 'fa fa-gear', 'fa fa-cog', 'fa fa-trash-o', 'fa fa-home', 'fa fa-file-o', 'fa fa-clock-o', 'fa fa-road', 'fa fa-download', 'fa fa-arrow-circle-o-down', 'fa fa-arrow-circle-o-up', 'fa fa-inbox', 'fa fa-play-circle-o', 'fa fa-rotate-right', 'fa fa-repeat', 'fa fa-refresh', 'fa fa-list-alt', 'fa fa-lock', 'fa fa-flag', 'fa fa-headphones', 'fa fa-volume-off', 'fa fa-volume-down', 'fa fa-volume-up', 'fa fa-qrcode', 'fa fa-barcode', 'fa fa-tag', 'fa fa-tags', 'fa fa-book', 'fa fa-bookmark', 'fa fa-print', 'fa fa-camera', 'fa fa-font', 'fa fa-bold', 'fa fa-italic', 'fa fa-text-height', 'fa fa-text-width', 'fa fa-align-left', 'fa fa-align-center', 'fa fa-align-right', 'fa fa-align-justify', 'fa fa-list', 'fa fa-dedent', 'fa fa-outdent', 'fa fa-indent', 'fa fa-video-camera', 'fa fa-photo', 'fa fa-image', 'fa fa-picture-o', 'fa fa-pencil', 'fa fa-map-marker', 'fa fa-adjust', 'fa fa-tint', 'fa fa-edit', 'fa fa-pencil-square-o', 'fa fa-share-square-o', 'fa fa-check-square-o', 'fa fa-arrows', 'fa fa-step-backward', 'fa fa-fast-backward', 'fa fa-backward', 'fa fa-play', 'fa fa-pause', 'fa fa-stop', 'fa fa-forward', 'fa fa-fast-forward', 'fa fa-step-forward', 'fa fa-eject', 'fa fa-chevron-left', 'fa fa-chevron-right', 'fa fa-plus-circle', 'fa fa-minus-circle', 'fa fa-times-circle', 'fa fa-check-circle', 'fa fa-question-circle', 'fa fa-info-circle', 'fa fa-crosshairs', 'fa fa-times-circle-o', 'fa fa-check-circle-o', 'fa fa-ban', 'fa fa-arrow-left', 'fa fa-arrow-right', 'fa fa-arrow-up', 'fa fa-arrow-down', 'fa fa-mail-forward', 'fa fa-share', 'fa fa-expand', 'fa fa-compress', 'fa fa-plus', 'fa fa-minus', 'fa fa-asterisk', 'fa fa-exclamation-circle', 'fa fa-gift', 'fa fa-leaf', 'fa fa-fire', 'fa fa-eye', 'fa fa-eye-slash', 'fa fa-warning', 'fa fa-exclamation-triangle', 'fa fa-plane', 'fa fa-calendar', 'fa fa-random', 'fa fa-comment', 'fa fa-magnet', 'fa fa-chevron-up', 'fa fa-chevron-down', 'fa fa-retweet', 'fa fa-shopping-cart', 'fa fa-folder', 'fa fa-folder-open', 'fa fa-arrows-v', 'fa fa-arrows-h', 'fa fa-bar-chart-o', 'fa fa-bar-chart', 'fa fa-twitter-square', 'fa fa-facebook-square', 'fa fa-camera-retro', 'fa fa-key', 'fa fa-gears', 'fa fa-cogs', 'fa fa-comments', 'fa fa-thumbs-o-up', 'fa fa-thumbs-o-down', 'fa fa-star-half', 'fa fa-heart-o', 'fa fa-sign-out', 'fa fa-linkedin-square', 'fa fa-thumb-tack', 'fa fa-external-link', 'fa fa-sign-in', 'fa fa-trophy', 'fa fa-github-square', 'fa fa-upload', 'fa fa-lemon-o', 'fa fa-phone', 'fa fa-square-o', 'fa fa-bookmark-o', 'fa fa-phone-square', 'fa fa-twitter', 'fa fa-facebook', 'fa fa-github', 'fa fa-unlock', 'fa fa-credit-card', 'fa fa-rss', 'fa fa-hdd-o', 'fa fa-bullhorn', 'fa fa-bell', 'fa fa-certificate', 'fa fa-hand-o-right', 'fa fa-hand-o-left', 'fa fa-hand-o-up', 'fa fa-hand-o-down', 'fa fa-arrow-circle-left', 'fa fa-arrow-circle-right', 'fa fa-arrow-circle-up', 'fa fa-arrow-circle-down', 'fa fa-globe', 'fa fa-wrench', 'fa fa-tasks', 'fa fa-filter', 'fa fa-briefcase', 'fa fa-arrows-alt', 'fa fa-group', 'fa fa-users', 'fa fa-chain', 'fa fa-link', 'fa fa-cloud', 'fa fa-flask', 'fa fa-cut', 'fa fa-scissors', 'fa fa-copy', 'fa fa-files-o', 'fa fa-paperclip', 'fa fa-save', 'fa fa-floppy-o', 'fa fa-square', 'fa fa-navicon', 'fa fa-reorder', 'fa fa-bars', 'fa fa-list-ul', 'fa fa-list-ol', 'fa fa-strikethrough', 'fa fa-underline', 'fa fa-table', 'fa fa-magic', 'fa fa-truck', 'fa fa-pinterest', 'fa fa-pinterest-square', 'fa fa-google-plus-square', 'fa fa-google-plus', 'fa fa-money', 'fa fa-caret-down', 'fa fa-caret-up', 'fa fa-caret-left', 'fa fa-caret-right', 'fa fa-columns', 'fa fa-unsorted', 'fa fa-sort', 'fa fa-sort-down', 'fa fa-sort-desc', 'fa fa-sort-up', 'fa fa-sort-asc', 'fa fa-envelope', 'fa fa-linkedin', 'fa fa-rotate-left', 'fa fa-undo', 'fa fa-legal', 'fa fa-gavel', 'fa fa-dashboard', 'fa fa-tachometer', 'fa fa-comment-o', 'fa fa-comments-o', 'fa fa-flash', 'fa fa-bolt', 'fa fa-sitemap', 'fa fa-umbrella', 'fa fa-paste', 'fa fa-clipboard', 'fa fa-lightbulb-o', 'fa fa-exchange', 'fa fa-cloud-download', 'fa fa-cloud-upload', 'fa fa-user-md', 'fa fa-stethoscope', 'fa fa-suitcase', 'fa fa-bell-o', 'fa fa-coffee', 'fa fa-cutlery', 'fa fa-file-text-o', 'fa fa-building-o', 'fa fa-hospital-o', 'fa fa-ambulance', 'fa fa-medkit', 'fa fa-fighter-jet', 'fa fa-beer', 'fa fa-h-square', 'fa fa-plus-square', 'fa fa-angle-double-left', 'fa fa-angle-double-right', 'fa fa-angle-double-up', 'fa fa-angle-double-down', 'fa fa-angle-left', 'fa fa-angle-right', 'fa fa-angle-up', 'fa fa-angle-down', 'fa fa-desktop', 'fa fa-laptop', 'fa fa-tablet', 'fa fa-mobile-phone', 'fa fa-mobile', 'fa fa-circle-o', 'fa fa-quote-left', 'fa fa-quote-right', 'fa fa-spinner', 'fa fa-circle', 'fa fa-mail-reply', 'fa fa-reply', 'fa fa-github-alt', 'fa fa-folder-o', 'fa fa-folder-open-o', 'fa fa-smile-o', 'fa fa-frown-o', 'fa fa-meh-o', 'fa fa-gamepad', 'fa fa-keyboard-o', 'fa fa-flag-o', 'fa fa-flag-checkered', 'fa fa-terminal', 'fa fa-code', 'fa fa-mail-reply-all', 'fa fa-reply-all', 'fa fa-star-half-empty', 'fa fa-star-half-full', 'fa fa-star-half-o', 'fa fa-location-arrow', 'fa fa-crop', 'fa fa-code-fork', 'fa fa-unlink', 'fa fa-chain-broken', 'fa fa-question', 'fa fa-info', 'fa fa-exclamation', 'fa fa-superscript', 'fa fa-subscript', 'fa fa-eraser', 'fa fa-puzzle-piece', 'fa fa-microphone', 'fa fa-microphone-slash', 'fa fa-shield', 'fa fa-calendar-o', 'fa fa-fire-extinguisher', 'fa fa-rocket', 'fa fa-maxcdn', 'fa fa-chevron-circle-left', 'fa fa-chevron-circle-right', 'fa fa-chevron-circle-up', 'fa fa-chevron-circle-down', 'fa fa-html5', 'fa fa-css3', 'fa fa-anchor', 'fa fa-unlock-alt', 'fa fa-bullseye', 'fa fa-ellipsis-h', 'fa fa-ellipsis-v', 'fa fa-rss-square', 'fa fa-play-circle', 'fa fa-ticket', 'fa fa-minus-square', 'fa fa-minus-square-o', 'fa fa-level-up', 'fa fa-level-down', 'fa fa-check-square', 'fa fa-pencil-square', 'fa fa-external-link-square', 'fa fa-share-square', 'fa fa-compass', 'fa fa-toggle-down', 'fa fa-caret-square-o-down', 'fa fa-toggle-up', 'fa fa-caret-square-o-up', 'fa fa-toggle-right', 'fa fa-caret-square-o-right', 'fa fa-euro', 'fa fa-eur', 'fa fa-gbp', 'fa fa-dollar', 'fa fa-usd', 'fa fa-rupee', 'fa fa-inr', 'fa fa-cny', 'fa fa-rmb', 'fa fa-yen', 'fa fa-jpy', 'fa fa-ruble', 'fa fa-rouble', 'fa fa-rub', 'fa fa-won', 'fa fa-krw', 'fa fa-bitcoin', 'fa fa-btc', 'fa fa-file', 'fa fa-file-text', 'fa fa-sort-alpha-asc', 'fa fa-sort-alpha-desc', 'fa fa-sort-amount-asc', 'fa fa-sort-amount-desc', 'fa fa-sort-numeric-asc', 'fa fa-sort-numeric-desc', 'fa fa-thumbs-up', 'fa fa-thumbs-down', 'fa fa-youtube-square', 'fa fa-youtube', 'fa fa-xing', 'fa fa-xing-square', 'fa fa-youtube-play', 'fa fa-dropbox', 'fa fa-stack-overflow', 'fa fa-instagram', 'fa fa-flickr', 'fa fa-adn', 'fa fa-bitbucket', 'fa fa-bitbucket-square', 'fa fa-tumblr', 'fa fa-tumblr-square', 'fa fa-long-arrow-down', 'fa fa-long-arrow-up', 'fa fa-long-arrow-left', 'fa fa-long-arrow-right', 'fa fa-apple', 'fa fa-windows', 'fa fa-android', 'fa fa-linux', 'fa fa-dribbble', 'fa fa-skype', 'fa fa-foursquare', 'fa fa-trello', 'fa fa-female', 'fa fa-male', 'fa fa-gittip', 'fa fa-sun-o', 'fa fa-moon-o', 'fa fa-archive', 'fa fa-bug', 'fa fa-vk', 'fa fa-weibo', 'fa fa-renren', 'fa fa-pagelines', 'fa fa-stack-exchange', 'fa fa-arrow-circle-o-right', 'fa fa-arrow-circle-o-left', 'fa fa-toggle-left', 'fa fa-caret-square-o-left', 'fa fa-dot-circle-o', 'fa fa-wheelchair', 'fa fa-vimeo-square', 'fa fa-turkish-lira', 'fa fa-try', 'fa fa-plus-square-o', 'fa fa-space-shuttle', 'fa fa-slack', 'fa fa-envelope-square', 'fa fa-wordpress', 'fa fa-openid', 'fa fa-institution', 'fa fa-bank', 'fa fa-university', 'fa fa-mortar-board', 'fa fa-graduation-cap', 'fa fa-yahoo', 'fa fa-google', 'fa fa-reddit', 'fa fa-reddit-square', 'fa fa-stumbleupon-circle', 'fa fa-stumbleupon', 'fa fa-delicious', 'fa fa-digg', 'fa fa-pied-piper', 'fa fa-pied-piper-alt', 'fa fa-drupal', 'fa fa-joomla', 'fa fa-language', 'fa fa-fax', 'fa fa-building', 'fa fa-child', 'fa fa-paw', 'fa fa-spoon', 'fa fa-cube', 'fa fa-cubes', 'fa fa-behance', 'fa fa-behance-square', 'fa fa-steam', 'fa fa-steam-square', 'fa fa-recycle', 'fa fa-automobile', 'fa fa-car', 'fa fa-cab', 'fa fa-taxi', 'fa fa-tree', 'fa fa-spotify', 'fa fa-deviantart', 'fa fa-soundcloud', 'fa fa-database', 'fa fa-file-pdf-o', 'fa fa-file-word-o', 'fa fa-file-excel-o', 'fa fa-file-powerpoint-o', 'fa fa-file-photo-o', 'fa fa-file-picture-o', 'fa fa-file-image-o', 'fa fa-file-zip-o', 'fa fa-file-archive-o', 'fa fa-file-sound-o', 'fa fa-file-audio-o', 'fa fa-file-movie-o', 'fa fa-file-video-o', 'fa fa-file-code-o', 'fa fa-vine', 'fa fa-codepen', 'fa fa-jsfiddle', 'fa fa-life-bouy', 'fa fa-life-buoy', 'fa fa-life-saver', 'fa fa-support', 'fa fa-life-ring', 'fa fa-circle-o-notch', 'fa fa-ra', 'fa fa-rebel', 'fa fa-ge', 'fa fa-empire', 'fa fa-git-square', 'fa fa-git', 'fa fa-hacker-news', 'fa fa-tencent-weibo', 'fa fa-qq', 'fa fa-wechat', 'fa fa-weixin', 'fa fa-send', 'fa fa-paper-plane', 'fa fa-send-o', 'fa fa-paper-plane-o', 'fa fa-history', 'fa fa-circle-thin', 'fa fa-header', 'fa fa-paragraph', 'fa fa-sliders', 'fa fa-share-alt', 'fa fa-share-alt-square', 'fa fa-bomb', 'fa fa-soccer-ball-o', 'fa fa-futbol-o', 'fa fa-tty', 'fa fa-binoculars', 'fa fa-plug', 'fa fa-slideshare', 'fa fa-twitch', 'fa fa-yelp', 'fa fa-newspaper-o', 'fa fa-wifi', 'fa fa-calculator', 'fa fa-paypal', 'fa fa-google-wallet', 'fa fa-cc-visa', 'fa fa-cc-mastercard', 'fa fa-cc-discover', 'fa fa-cc-amex', 'fa fa-cc-paypal', 'fa fa-cc-stripe', 'fa fa-bell-slash', 'fa fa-bell-slash-o', 'fa fa-trash', 'fa fa-copyright', 'fa fa-at', 'fa fa-eyedropper', 'fa fa-paint-brush', 'fa fa-birthday-cake', 'fa fa-area-chart', 'fa fa-pie-chart', 'fa fa-line-chart', 'fa fa-lastfm', 'fa fa-lastfm-square', 'fa fa-toggle-off', 'fa fa-toggle-on', 'fa fa-bicycle', 'fa fa-bus', 'fa fa-ioxhost', 'fa fa-angellist', 'fa fa-cc', 'fa fa-shekel', 'fa fa-sheqel', 'fa fa-ils', 'fa fa-meanpath' );
			sort( $k2t_icon );
			trim( join( 'fa ', $k2t_icon ) );

			$k2t_margin_top = array(
				'param_name'  => 'mgt',
				'heading'     => __( 'Margin Top', 'ruby' ),
				'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
				'type'        => 'textfield',
			);
			$k2t_margin_right = array(
				'param_name'  => 'mgr',
				'heading'     => __( 'Margin Right', 'ruby' ),
				'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
				'type'        => 'textfield',
			);
			$k2t_margin_bottom = array(
				'param_name'  => 'mgb',
				'heading'     => __( 'Margin Bottom', 'ruby' ),
				'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
				'type'        => 'textfield',
			);
			$k2t_margin_left = array(
				'param_name'  => 'mgl',
				'heading'     => __( 'Margin Left', 'ruby' ),
				'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
				'type'        => 'textfield',
			);
			$k2t_id = array(
				'param_name'  => 'id',
				'heading'     => __( 'ID', 'ruby' ),
				'description' => __( '(Optional) Enter a unique ID.', 'ruby' ),
				'type'        => 'textfield',
			);
			$k2t_class = array(
				'param_name'  => 'class',
				'heading'     => __( 'Class', 'ruby' ),
				'description' => __( '(Optional) Enter a unique class name.', 'ruby' ),
				'type'        => 'textfield',
			);
			$k2t_animation = array(
				'param_name' => 'anm',
				'heading' 	 => __( 'Enable Animation', 'ruby' ),
				'type' 		 => 'checkbox',
				'value'      => array(
					'' => true
				)
			);
			$k2t_animation_name = array(
				'param_name' => 'anm_name',
				'heading' 	 => __( 'Animation', 'ruby' ),
				'type' 		 => 'dropdown',
				'dependency' => array(
					'element' => 'anm',
					'value'   => array( '1' ),
					'not_empty' => false,
				),
				'value'      => array( 'bounce', 'flash', 'pulse', 'rubberBand', 'shake', 'swing', 'tada', 'wobble', 'bounceIn', 'bounceInDown', 'bounceInLeft', 'bounceInRight', 'bounceInUp', 'fadeIn', 'fadeInDown', 'fadeInDownBig', 'fadeInLeft', 'fadeInLeftBig', 'fadeInRight', 'fadeInRightBig', 'fadeInUp', 'fadeInUpBig', 'flip', 'flipInX', 'flipInY', 'lightSpeedIn', 'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft', 'rotateInUpRight', 'rollIn', 'zoomIn', 'zoomInDown', 'zoomInLeft', 'zoomInRight', 'zoomInUp' ),
			);
			$k2t_animation_delay = array(
				'param_name'  => 'anm_delay',
				'heading'     => __( 'Animation Delay', 'ruby' ),
				'description' => __( 'Numeric value only, 1000 = 1second.', 'ruby' ),
				'type'        => 'textfield',
				'std'		  => '2000',
				'dependency' => array(
					'element' => 'anm',
					'value'   => array( '1' ),
					'not_empty' => false,
				),
			);

			/*  [ Row ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_row', array(
					'name'        => __( 'Row', 'ruby' ),
					'icon'        => 'fa fa-tasks',
					'category'    => __( 'Structure', 'ruby' ),
					'description' => '',
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'session_layout',
					'heading'     => __( 'Session layout', 'ruby' ),
					'type'        => 'dropdown',
					'value'       => array(
						__( 'None', 'ruby' ) => '',
						__( 'Full Width', 'ruby' ) => 'no_wrap',
						__( 'Fullscreen', 'ruby' ) => 'fullscreen'
					)
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_setting',
					'heading'     => __( 'Background type', 'ruby' ),
					'type'        => 'dropdown',
					'value'       => array(
						__( 'Background Color', 'ruby' ) => 'bg_color',
						__( 'Background Image', 'ruby' ) => 'bg_image',
						__( 'Background Video', 'ruby' ) => 'bg_video',
						__( 'Background Slider', 'ruby' ) => 'bg_slider',
					)
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'dark_background_style',
					'heading'     => __( 'Background with dark style', 'ruby' ),
					'type'        => 'checkbox',
					'holder'      => 'div',
					'value'       => array(
						'' => 'true'
					)
				)
			);
			// Background Image
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_img',
					'heading'     => __( 'Background Image', 'ruby' ),
					'type'        => 'attach_image',
					'holder'      => 'div',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image' )
					),
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_img_position',
					'heading'     => __( 'Background Position', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image' )
					),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'None', 'ruby' )      => '',
						__( 'Left Top', 'ruby' )      => 'left top',
						__( 'Left Center', 'ruby' )   => 'left center',
						__( 'Left Bottom', 'ruby' )   => 'left bottom',
						__( 'Right Top', 'ruby' )     => 'right top',
						__( 'Right Center', 'ruby' )  => 'right center',
						__( 'Right Bottom', 'ruby' )  => 'right bottom',
						__( 'Center Top', 'ruby' )    => 'center top',
						__( 'Center Center', 'ruby' ) => 'center center',
						__( 'Center Bottom', 'ruby' ) => 'center bottom',
					),
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_img_repeat',
					'heading'     => __( 'Background Repeat', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image' )
					),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'No repeat', 'ruby' ) => 'no-repeat',
						__( 'Repeat', 'ruby' )    => 'repeat',
						__( 'Repeat X', 'ruby' )  => 'repeat-x',
						__( 'Repeat Y', 'ruby' )  => 'repeat-y',
					),
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_img_size',
					'heading'     => __( 'Background size', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image' )
					),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'None', 'ruby' ) => '',
						__( 'Auto', 'ruby' )    => 'auto',
						__( 'Length', 'ruby' )  => 'length',
						__( 'Percentage', 'ruby' )  => 'percentage',
						__( 'Cover', 'ruby' )  => 'cover',
						__( 'Contain', 'ruby' )  => 'contain',
						__( 'Initial', 'ruby' )  => 'initial',
					),
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_img_animation',
					'heading'     => __( 'Background Animation', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image' )
					),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'None', 'ruby' ) => '',
						__( 'Left to Right', 'ruby' ) => 'left_to_right',
						__( 'Right to Left', 'ruby' )    => 'right_to_left',
						__( 'Top to Bottom', 'ruby' )  => 'top_to_bottom',
						__( 'Bottom to Top', 'ruby' )  => 'bottom_to_top',
					),
				)
			);
			// Background Video
			vc_add_param(
				'vc_row', array(
					'param_name' => 'background_video_link',
					'heading'    => __( 'Background Video ID', 'ruby' ),
					'type'       => 'textfield',
					'description' => __( 'Only support youtube (eg: fDxFScMvoG8)', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_video' )
					),
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name' => 'background_video_play_id',
					'heading'    => __( 'Set background video ID', 'ruby' ),
					'description' => __( 'You can use this ID for your action ( ex: button, link, image...) to play background video', 'ruby' ),
					'type'       => 'textfield',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_video' )
					),
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_video_mute',
					'heading'     => __( 'Video Mute', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_video' )
					),
					'type'        => 'checkbox',
					'holder'      => 'div',
					'value'       => array(
						'' => 'false'
					)
				)
			);
			// Slider Background
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_slider_images',
					'heading'     => __( 'Background Slider Images', 'ruby' ),
					'type'        => 'attach_images',
					'holder'      => 'div',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_slider' )
					),
				)
			);
			// General Background
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_gen_auto_play',
					'heading'     => __( 'Auto Play', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_video', 'bg_slider' )
					),
					'type'        => 'checkbox',
					'holder'      => 'div',
					'value'       => array(
						'' => 'false'
					)
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_gen_parallax',
					'heading'     => __( 'Parallax Background', 'ruby' ),
					'description' => __( 'Parallax effect for background images', 'ruby' ),
					'type'        => 'checkbox',
					'value'       => array(
						'' => true,
					),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image', 'bg_slider' )
					),
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_gen_color',
					'heading'     => __( 'Background Color', 'ruby' ),
					'type'        => 'colorpicker',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_color', 'bg_image', 'bg_video', 'bg_slider' )
					),
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_gen_mask_layer_image',
					'heading'     => __( 'Mask Layer Image', 'ruby' ),
					'type'        => 'attach_image',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image', 'bg_video', 'bg_slider' )
					),
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_gen_mask_layer_repeat',
					'heading'     => __( 'Mask Layer Repeat', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image', 'bg_video', 'bg_slider' )
					),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'No repeat', 'ruby' ) => 'no-repeat',
						__( 'Repeat', 'ruby' )    => 'repeat',
						__( 'Repeat X', 'ruby' )  => 'repeat-x',
						__( 'Repeat Y', 'ruby' )  => 'repeat-y',
					),
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name' => 'background_gen_mask_layer_opacity',
					'heading'    => __( 'Mask Layer Opacity', 'ruby' ),
					'type'       => 'textfield',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image', 'bg_video', 'bg_slider' )
					),
				)
			);
			vc_add_param(
				'vc_row', array(
					'param_name'  => 'background_gen_mask_layer_color',
					'heading'     => __( 'Mask Layer Color', 'ruby' ),
					'type'        => 'colorpicker',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image', 'bg_video', 'bg_slider' )
					),
				)
			);
			vc_remove_param( 'vc_row', 'font_color' );
			vc_remove_param( 'vc_row', 'el_class' );
			vc_add_param( 'vc_row', $k2t_id );
			vc_add_param( 'vc_row', $k2t_class );

			/*  [ Column ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_column', array(
					'name'        => __( 'Row', 'ruby' ),
					'icon'        => 'fa fa-tasks',
					'category'    => __( 'Structure', 'ruby' ),
					'description' => '',
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_setting',
					'heading'     => __( 'Background type', 'ruby' ),
					'type'        => 'dropdown',
					'value'       => array(
						__( 'Background Color', 'ruby' ) => 'bg_color',
						__( 'Background Image', 'ruby' ) => 'bg_image',
						__( 'Background Video', 'ruby' ) => 'bg_video',
						__( 'Background Slider', 'ruby' ) => 'bg_slider',
					)
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'dark_background_style',
					'heading'     => __( 'Background with dark style', 'ruby' ),
					'type'        => 'checkbox',
					'holder'      => 'div',
					'value'       => array(
						'' => 'true'
					)
				)
			);
			// Background Image
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_img',
					'heading'     => __( 'Background Image', 'ruby' ),
					'type'        => 'attach_image',
					'holder'      => 'div',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image' )
					),
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_img_position',
					'heading'     => __( 'Background Position', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image' )
					),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'None', 'ruby' )      => '',
						__( 'Left Top', 'ruby' )      => 'left top',
						__( 'Left Center', 'ruby' )   => 'left center',
						__( 'Left Bottom', 'ruby' )   => 'left bottom',
						__( 'Right Top', 'ruby' )     => 'right top',
						__( 'Right Center', 'ruby' )  => 'right center',
						__( 'Right Bottom', 'ruby' )  => 'right bottom',
						__( 'Center Top', 'ruby' )    => 'center top',
						__( 'Center Center', 'ruby' ) => 'center center',
						__( 'Center Bottom', 'ruby' ) => 'center bottom',
					),
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_img_repeat',
					'heading'     => __( 'Background Repeat', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image' )
					),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'No repeat', 'ruby' ) => 'no-repeat',
						__( 'Repeat', 'ruby' )    => 'repeat',
						__( 'Repeat X', 'ruby' )  => 'repeat-x',
						__( 'Repeat Y', 'ruby' )  => 'repeat-y',
					),
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_img_size',
					'heading'     => __( 'Background size', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image' )
					),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'None', 'ruby' ) => '',
						__( 'Auto', 'ruby' )    => 'auto',
						__( 'Length', 'ruby' )  => 'length',
						__( 'Percentage', 'ruby' )  => 'percentage',
						__( 'Cover', 'ruby' )  => 'cover',
						__( 'Contain', 'ruby' )  => 'contain',
						__( 'Initial', 'ruby' )  => 'initial',
					),
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_img_animation',
					'heading'     => __( 'Background Animation', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image' )
					),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'None', 'ruby' ) => '',
						__( 'Left to Right', 'ruby' ) => 'left_to_right',
						__( 'Right to Left', 'ruby' )    => 'right_to_left',
						__( 'Top to Bottom', 'ruby' )  => 'top_to_bottom',
						__( 'Bottom to Top', 'ruby' )  => 'bottom_to_top',
					),
				)
			);
			// Background Video
			vc_add_param(
				'vc_column', array(
					'param_name' => 'background_video_link',
					'heading'    => __( 'Background Video ID', 'ruby' ),
					'type'       => 'textfield',
					'description' => __( 'Only support youtube (eg: fDxFScMvoG8)', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_video' )
					),
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name' => 'background_video_play_id',
					'heading'    => __( 'Set background video ID', 'ruby' ),
					'description' => __( 'You can use this ID for your action ( ex: button, link, image...) to play background video', 'ruby' ),
					'type'       => 'textfield',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_video' )
					),
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_video_mute',
					'heading'     => __( 'Video Mute', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_video' )
					),
					'type'        => 'checkbox',
					'holder'      => 'div',
					'value'       => array(
						'' => 'false'
					)
				)
			);
			// Slider Background
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_slider_images',
					'heading'     => __( 'Background Slider Images', 'ruby' ),
					'type'        => 'attach_images',
					'holder'      => 'div',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_slider' )
					),
				)
			);
			// General Background
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_gen_auto_play',
					'heading'     => __( 'Background Auto Play', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_video', 'bg_slider' )
					),
					'type'        => 'checkbox',
					'holder'      => 'div',
					'value'       => array(
						'' => 'false'
					)
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_gen_parallax',
					'heading'     => __( 'Parallax Background', 'ruby' ),
					'description' => __( 'Parallax effect for background images', 'ruby' ),
					'type'        => 'checkbox',
					'value'       => array(
						'' => true,
					),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image', 'bg_slider' )
					),
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_gen_color',
					'heading'     => __( 'Background Color', 'ruby' ),
					'type'        => 'colorpicker',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_color', 'bg_image', 'bg_video', 'bg_slider' )
					),
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_gen_mask_layer_image',
					'heading'     => __( 'Mask Layer Image', 'ruby' ),
					'type'        => 'attach_image',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image', 'bg_video', 'bg_slider' )
					),
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_gen_mask_layer_repeat',
					'heading'     => __( 'Mask Layer Repeat', 'ruby' ),
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image', 'bg_video', 'bg_slider' )
					),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'No repeat', 'ruby' ) => 'no-repeat',
						__( 'Repeat', 'ruby' )    => 'repeat',
						__( 'Repeat X', 'ruby' )  => 'repeat-x',
						__( 'Repeat Y', 'ruby' )  => 'repeat-y',
					),
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name' => 'background_gen_mask_layer_opacity',
					'heading'    => __( 'Mask Layer Opacity', 'ruby' ),
					'type'       => 'textfield',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image', 'bg_video', 'bg_slider' )
					),
				)
			);
			vc_add_param(
				'vc_column', array(
					'param_name'  => 'background_gen_mask_layer_color',
					'heading'     => __( 'Mask Layer Color', 'ruby' ),
					'type'        => 'colorpicker',
					'dependency' => array(
						'element' => 'background_setting',
						'value'   => array( 'bg_image', 'bg_video', 'bg_slider' )
					),
				)
			);
			vc_remove_param( 'vc_column', 'font_color' );
			vc_remove_param( 'vc_column', 'el_class' );
			vc_add_param( 'vc_column', $k2t_id );
			vc_add_param( 'vc_column', $k2t_class );

			/*  [ VC Tabs]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_tabs', array(
					'name'        => __( 'Tabs', 'ruby' ),
					'icon'        => 'fa fa-text-height',
					'category'    => __( 'Content', 'ruby' ),
					'description' => '',
				)
			);
			vc_add_param(
				'vc_tabs', array(
					'param_name' => 'align',
					'heading' 	 => __( 'Align', 'ruby' ),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'Left', 'ruby' ) 					=> 'left',
						__( 'Center', 'ruby' )    				=> 'center',
						__( 'Right', 'ruby' )    				=> 'right',
					),
				)
			);
			vc_add_param(
				'vc_tabs', array(
					'param_name' => 'style',
					'heading' 	 => __( 'Style', 'ruby' ),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'OutLine', 'ruby' ) 				=> 'outline',
						__( 'Fill', 'ruby' ) 					=> 'fill',
						__( 'Solid', 'ruby' )    				=> 'solid',
						__( 'Bottom Line', 'ruby' )    			=> 'bottom_line',
					),
				)
			);
			vc_add_param(
				'vc_tabs', array(
					'param_name'  => 'icon_font_size',
					'heading'     => __( 'Icon Font size', 'ruby' ),
					'type'        => 'textfield',
					'holder'      => 'div',
				)
			);
			vc_remove_param( 'vc_tabs', 'el_class' );
			vc_add_param( 'vc_tabs', $k2t_class );

			/*  [ VC Vertical tab ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_tour', array(
					'name'        => __( 'Vertical tabs', 'ruby' ),
					'icon'        => 'fa fa-list-ul',
					'category'    => __( 'Structure', 'ruby' ),
					'description' => '',
				)
			);
			vc_add_param(
				'vc_tour', array(
					'param_name' => 'style',
					'heading' 	 => __( 'Styles', 'ruby' ),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'OutLine', 'ruby' ) 				=> 'outline',
						__( 'Solid', 'ruby' )    				=> 'solid',
					),
				)
			);

			/*  [ VC Tab]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_tab', array(
					'name'        => __( 'Text Block', 'ruby' ),
					'icon'        => 'fa fa-text-height',
					'category'    => __( 'Content', 'ruby' ),
					'description' => '',
				)
			);
			vc_add_param(
				'vc_tab', array(
					'param_name' => 'icon',
					'heading' 	 => __( 'Icons', 'ruby' ),
					'type' 		 => 'k2t_icon',
					'value'      => '',
				)
			);
			vc_add_param(
				'vc_tab', array(
					'param_name'  => 'icon_pos',
					'heading'     => __( 'Icon Position', 'ruby' ),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'Left', 'ruby' ) => 'left',
						__( 'Top', 'ruby' )    => 'top',
					),
				)
			);

			/*  [ VC Column Text ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_column_text', array(
					'name'        => __( 'Text Block', 'ruby' ),
					'icon'        => 'fa fa-text-height',
					'category'    => __( 'Content', 'ruby' ),
					'description' => '',
				)
			);
			vc_remove_param( 'vc_column_text', 'css_animation' );
			vc_remove_param( 'vc_column_text', 'el_class' );
			vc_add_param( 'vc_column_text', $k2t_animation );
			vc_add_param( 'vc_column_text', $k2t_animation_name );
			vc_add_param( 'vc_column_text', $k2t_animation_delay );
			vc_add_param( 'vc_column_text', $k2t_id );
			vc_add_param( 'vc_column_text', $k2t_class );

			/*  [ VC Separator ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_separator', array(
					'name'        => __( 'Separator', 'ruby' ),
					'icon'        => 'fa fa-minus',
					'category'    => __( 'Structure', 'ruby' ),
					'description' => '',
				)
			);
			vc_add_param(
				'vc_separator', array(
					'param_name'  => 'style',
					'heading'     => __( 'Style', 'ruby' ),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'Border', 'ruby' ) => '',
						__( 'Dashed', 'ruby' )    => 'dashed',
						__( 'Dotted', 'ruby' )    => 'dotted',
						__( 'Double', 'ruby' )    => 'double',
						__( 'Shadow', 'ruby' )    => 'shadow',
					),
				)
			);

			/*  [ VC Separator With Text ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_text_separator', array(
					'name'        => __( 'Separator with text', 'ruby' ),
					'icon'        => 'fa fa-text-width',
					'category'    => __( 'Structure', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC Message Box ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_message', array(
					'name'        => __( 'Message box', 'ruby' ),
					'icon'        => 'fa fa-file-text-o',
					'category'    => __( 'Content', 'ruby' ),
					'description' => '',
				)
			);
			vc_add_param(
				'vc_message', array(
					'param_name' => 'icon',
					'heading' 	 => __( 'Icons', 'ruby' ),
					'type' 		 => 'k2t_icon',
					'value'      => '',
				)
			);
			vc_add_param(
				'vc_message', array(
					'param_name'  => 'background_transparent',
					'heading'     => __( 'Background Transparent', 'ruby' ),
					'type'        => 'checkbox',
					'holder'      => 'div',
					'value'       => array(
						'' => true
					)
				)
			);
			vc_add_param(
				'vc_message', array(
					'param_name'  => 'is_close',
					'heading'     => __( 'Is Close', 'ruby' ),
					'type'        => 'checkbox',
					'holder'      => 'div',
					'value'       => array(
						'' => true
					)
				)
			);
			vc_remove_param( 'vc_message', 'css_animation' );
			vc_remove_param( 'vc_message', 'el_class' );
			vc_remove_param( 'vc_message', 'icon_fontawesome' );
			vc_remove_param( 'vc_message', 'icon_type' );
			vc_add_param( 'vc_message', $k2t_animation );
			vc_add_param( 'vc_message', $k2t_animation_name );
			vc_add_param( 'vc_message', $k2t_animation_delay );
			vc_add_param( 'vc_message', $k2t_id );
			vc_add_param( 'vc_message', $k2t_class );

			/*  [ VC Facebook ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_facebook', array(
					'name'        => __( 'Facebook like', 'ruby' ),
					'icon'        => 'fa fa-facebook',
					'category'    => __( 'Socials', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC Tweetmeme ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_tweetmeme', array(
					'name'        => __( 'Tweetmeme', 'ruby' ),
					'icon'        => 'fa fa-twitter',
					'category'    => __( 'Socials', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC Google Plus ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_googleplus', array(
					'name'        => __( 'Google Plus', 'ruby' ),
					'icon'        => 'fa fa-google-plus',
					'category'    => __( 'Socials', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC Pinterest ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_pinterest', array(
					'name'        => __( 'Pinterest', 'ruby' ),
					'icon'        => 'fa fa-pinterest',
					'category'    => __( 'Socials', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC Single Image ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_single_image', array(
					'name'        => __( 'Single Image', 'ruby' ),
					'icon'        => 'fa fa-image',
					'category'    => __( 'Content', 'ruby' ),
					'description' => '',
				)
			);
			vc_add_param(
				'vc_single_image', array(
					'param_name'  => 'image_style',
					'heading'     => __( 'Image Style', 'ruby' ),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'None', 'ruby' ) => '',
						__( 'Rounded', 'ruby' )    => 'rounded',
						__( 'Border', 'ruby' )    => 'border',
						__( 'Outline', 'ruby' )    => 'outline',
						__( 'Shadow', 'ruby' )    => 'shadow',
					),
				)
			);
			vc_add_param(
				'vc_single_image', array(
					'param_name'  => 'image_hover_style',
					'heading'     => __( 'Image Hover Style', 'ruby' ),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'None', 'ruby' ) => '',
						__( 'Dark', 'ruby' )    => 'dark',
						__( 'Light', 'ruby' )    => 'light',
						__( 'Banner', 'ruby' )    => 'banner',
					),
				)
			);
			vc_add_param(
				'vc_single_image', array(
					'param_name' => 'image_banner_hover',
					'heading'    => __( 'Image Banner Content', 'ruby' ),
					'type'       => 'textarea',
					'dependency' => array(
						'element' => 'image_hover_style',
						'value'   => array( 'banner' )
					),
				)
			);
			vc_remove_param( 'vc_single_image', 'css_animation' );
			vc_remove_param( 'vc_single_image', 'el_class' );
			vc_remove_param( 'vc_single_image', 'style' );
			vc_remove_param( 'vc_single_image', 'border_color' );
			vc_remove_param( 'vc_single_image', 'img_link_large' );
			vc_remove_param( 'vc_single_image', 'img_link_target' );
			vc_add_param( 'vc_single_image', $k2t_animation );
			vc_add_param( 'vc_single_image', $k2t_animation_name );
			vc_add_param( 'vc_single_image', $k2t_animation_delay );
			vc_add_param( 'vc_single_image', $k2t_id );
			vc_add_param( 'vc_single_image', $k2t_class );

			/*  [ VC Gallery ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_gallery', array(
					'name'        => __( 'Gallery', 'ruby' ),
					'icon'        => 'fa fa-caret-square-o-right',
					'category'    => __( 'Media', 'ruby' ),
					'description' => '',
				)
			);
			vc_remove_param( 'vc_gallery', 'el_class' );
			vc_add_param( 'vc_gallery', $k2t_id );
			vc_add_param( 'vc_gallery', $k2t_class );

			/*  [ VC Carousel ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_images_carousel', array(
					'name'        => __( 'Carousel', 'ruby' ),
					'icon'        => 'fa fa-exchange',
					'category'    => __( 'Media', 'ruby' ),
					'description' => '',
				)
			);
			vc_remove_param( 'vc_images_carousel', 'el_class' );
			vc_add_param( 'vc_images_carousel', $k2t_id );
			vc_add_param( 'vc_images_carousel', $k2t_class );

			/*  [ VC Toggle ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_toggle', array(
					'name'        => __( 'Toggles', 'ruby' ),
					'icon'        => 'fa fa-indent',
					'category'    => __( 'Structure', 'ruby' ),
					'description' => '',
				)
			);
			vc_remove_param( 'vc_toggle', 'css_animation' );
			vc_remove_param( 'vc_toggle', 'el_class' );
			vc_add_param( 'vc_toggle', $k2t_animation );
			vc_add_param( 'vc_toggle', $k2t_animation_name );
			vc_add_param( 'vc_toggle', $k2t_animation_delay );
			vc_add_param( 'vc_toggle', $k2t_id );
			vc_add_param( 'vc_toggle', $k2t_class );

			/*  [ VC Video ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_video', array(
					'name'        => __( 'Video', 'ruby' ),
					'icon'        => 'fa fa-video-camera',
					'category'    => __( 'Media', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC Raw HTML ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_raw_html', array(
					'name'        => __( 'Raw HTML code', 'ruby' ),
					'icon'        => 'fa fa-code',
					'category'    => __( 'Structure', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC Raw JS ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_raw_js', array(
					'name'        => __( 'Raw JS code', 'ruby' ),
					'icon'        => 'fa fa-code',
					'category'    => __( 'Structure', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC Empty Space ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_empty_space', array(
					'name'        => __( 'Empty Space', 'ruby' ),
					'icon'        => 'fa fa-arrows-v',
					'category'    => __( 'Structure', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC Custom Heading ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_custom_heading', array(
					'name'        => __( 'Custom Heading', 'ruby' ),
					'icon'        => 'fa fa-header',
					'category'    => __( 'Typography', 'ruby' ),
					'description' => '',
				)
			);
			vc_add_param(
				'vc_custom_heading', array(
					'param_name' => 'text_transform',
					'heading' 	 => __( 'Text Transform', 'ruby' ),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'Inherit', 'ruby' )    => 'inherit',
						__( 'Uppercase', 'ruby' )  => 'uppercase',
						__( 'lowercase', 'ruby' )  => 'lowercase',
						__( 'initial', 'ruby' )    => 'initial',
						__( 'capitalize', 'ruby' ) => 'capitalize',
					),
				)
			);
			vc_remove_param( 'vc_custom_heading', 'el_class' );
			vc_add_param( 'vc_custom_heading', $k2t_animation );
			vc_add_param( 'vc_custom_heading', $k2t_animation_name );
			vc_add_param( 'vc_custom_heading', $k2t_animation_delay );
			vc_add_param( 'vc_custom_heading', $k2t_id );
			vc_add_param( 'vc_custom_heading', $k2t_class );

			/*  [ VC Posts Grid ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_posts_grid', array(
					'name'        => __( 'Post Grid', 'ruby' ),
					'icon'        => 'fa fa-th',
					'category'    => __( 'Content', 'ruby' ),
					'description' => ''
				)
			);
			vc_add_param(
				'vc_posts_grid', array(
					'param_name' => 'align',
					'heading' 	 => __( 'Align', 'ruby' ),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'Left', 'ruby' )    => 'left',
						__( 'Center', 'ruby' )  => 'center',
						__( 'Right', 'ruby' )  => 'right',
					),
				)
			);
			vc_add_param(
				'vc_posts_grid', array(
					'param_name' => 'style',
					'heading' 	 => __( 'Style', 'ruby' ),
					'type' 		 => 'dropdown',
					'value'      => array(
						__( 'Default', 'ruby' )    => 'default',
						__( 'Boxed', 'ruby' )  => 'boxed',
					),
				)
			);
			vc_remove_param( 'vc_posts_grid', 'filter' );

			/*  [ VC WP Search ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_wp_search', array(
					'name'        => __( 'WP Search', 'ruby' ),
					'icon'        => 'fa fa-wordpress',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC WP Meta ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_wp_meta', array(
					'name'        => __( 'WP Meta', 'ruby' ),
					'icon'        => 'fa fa-wordpress',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC WP recent comments ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_wp_recentcomments', array(
					'name'        => __( 'WP Recent Comments', 'ruby' ),
					'icon'        => 'fa fa-wordpress',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC WP calendar ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_wp_calendar', array(
					'name'        => __( 'WP Calendar', 'ruby' ),
					'icon'        => 'fa fa-wordpress',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC WP pages ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_wp_pages', array(
					'name'        => __( 'WP Pages', 'ruby' ),
					'icon'        => 'fa fa-wordpress',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC WP Tagcloud ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_wp_tagcloud', array(
					'name'        => __( 'WP Tagcloud', 'ruby' ),
					'icon'        => 'fa fa-wordpress',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC WP custom menu ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_wp_custommenu', array(
					'name'        => __( 'WP Custom Menu', 'ruby' ),
					'icon'        => 'fa fa-wordpress',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC WP text ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_wp_text', array(
					'name'        => __( 'WP Text', 'ruby' ),
					'icon'        => 'fa fa-wordpress',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC WP posts ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_wp_posts', array(
					'name'        => __( 'WP Posts', 'ruby' ),
					'icon'        => 'fa fa-wordpress',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC WP categories ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_wp_categories', array(
					'name'        => __( 'WP Categories', 'ruby' ),
					'icon'        => 'fa fa-wordpress',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC WP archives ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_wp_archives', array(
					'name'        => __( 'WP Archives', 'ruby' ),
					'icon'        => 'fa fa-wordpress',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ VC WP rss ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'vc_wp_rss', array(
					'name'        => __( 'WP RSS', 'ruby' ),
					'icon'        => 'fa fa-wordpress',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ Contact form 7 ]
			- - - - - - - - - - - - - - - - - - - */
			vc_map_update(
				'contact-form-7', array(
					'name'        => __( 'Contact Form 7', 'ruby' ),
					'icon'        => 'fa fa-list-alt',
					'category'    => __( 'WordPress', 'ruby' ),
					'description' => '',
				)
			);

			/*  [ Accordion ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_accordion = array(
				'base'            => 'accordion',
				'name'            => __( 'Accordion', 'ruby' ),
				'icon'            => 'fa fa-sort-amount-desc',
				'category'        => __( 'Structure', 'ruby' ),
				'as_parent'       => array( 'only' => 'toggle' ),
				'content_element' => true,
				'js_view'         => 'VcColumnView',
				'params'          => array(
					array(
						'param_name'  => 'style',
						'heading' 	  => __( 'Style', 'ruby' ),
						'description' => __( 'Select style for accordion', 'ruby'),
						'type' 		  => 'dropdown',
						'value'       => array( '1', '2', '3' ),
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_accordion );

			/*  [ Accordion Items ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_accordion_item = array(
				'base'            => 'toggle',
				'name'            => __( 'Accordion Item', 'ruby' ),
				'icon'            => 'fa fa-sort-amount-desc',
				'category'        => __( 'Structure', 'ruby' ),
				'as_child'        => array( 'only' => 'accordion' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'title',
						'heading'     => __( 'Title', 'ruby' ),
						'description' => __( 'Title for your accordion item.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'icon',
						'heading' 	 => __( 'Icons', 'ruby' ),
						'type' 		 => 'k2t_icon',
						'value'      => '',
					),
					array(
						'param_name'  => 'acc_content',
						'heading'     => __( 'Content', 'ruby' ),
						'description' => __( 'Enter your text.', 'ruby' ),
						'type'        => 'textarea',
						'holder'      => 'div',
						'value'       => ''
					),
					array(
						'param_name'  => 'open',
						'heading'     => __( 'Open', 'ruby' ),
						'description' => __( 'Select for your accordion item to be open by default.', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => 'true'
						)
					),
					$k2t_id, $k2t_class
				)
			);
			vc_map( $k2t_accordion_item );

			/*  [ Brands ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_brands = array(
				'base'            => 'brands',
				'name'            => __( 'Brands', 'ruby' ),
				'icon'            => 'fa fa-photo',
				'category'        => __( 'Content', 'ruby' ),
				'as_parent'       => array( 'only' => 'brand' ),
				'content_element' => true,
				'js_view'         => 'VcColumnView',
				'params'          => array(
					array(
						'param_name'  => 'column',
						'heading' 	  => __( 'Column', 'ruby' ),
						'description' => __( 'Select column display brand', 'ruby'),
						'type' 		  => 'dropdown',
						'value'       => array( '1', '2', '3', '4', '5', '6', '7', '8' ),
					),
					array(
						'param_name'  => 'padding',
						'heading'     => __( 'Padding', 'ruby' ),
						'description' => __( 'If you select true, it will be padding between item', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => 'true',
						)
					),
					array(
						'param_name'  => 'grayscale',
						'heading'     => __( 'Grayscale', 'ruby' ),
						'description' => __( 'Display grayscale.', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => 'true'
						)
					),
					$k2t_id, $k2t_class
				)
			);
			vc_map( $k2t_brands );

			/*  [ Brand Items ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_brands_item = array(
				'base'            => 'brand',
				'name'            => __( 'Brands Item', 'ruby' ),
				'icon'            => 'fa fa-photo',
				'category'        => __( 'Content', 'ruby' ),
				'as_child'        => array( 'only' => 'brands' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'title',
						'heading'     => __( 'Brand Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'tooltip',
						'heading'     => __( 'Tooltip', 'ruby' ),
						'description' => __( 'Enable tooltip.', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => 'true'
						)
					),
					array(
						'param_name'  => 'link',
						'heading'     => __( 'Upload Brand', 'ruby' ),
						'type'        => 'attach_image',
						'holder'      => 'div',
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
				)
			);
			vc_map( $k2t_brands_item );

			/*  [ Button ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_button = array(
				'base'            => 'button',
				'name'            => __( 'Button', 'ruby' ),
				'icon'            => 'fa fa-square',
				'category'        => __( 'Typography', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'title',
						'heading'     => __( 'Button Text', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'link',
						'heading'     => __( 'Link', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'target',
						'heading' 	 => __( 'Link Target', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Open in a new window', 'ruby' )                      => '_blank',
							__( 'Open in the same frame as it was clicked', 'ruby' )  => '_self'
						),
					),
					array(
						'param_name' => 'icon',
						'heading' 	 => __( 'Choose Icon', 'ruby' ),
						'type' 		 => 'k2t_icon',
						'value'      => '',
					),
					array(
						'param_name' => 'icon_position',
						'heading' 	 => __( 'Icon Position', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Right', 'ruby' ) 				=> 'right',
							__( 'Left', 'ruby' )  				=> 'left'
						),
					),
					array(
						'param_name' => 'size',
						'heading' 	 => __( 'Size', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Small', 'ruby' )  				=> 'small',
							__( 'Medium', 'ruby' ) 				=> 'medium',
							__( 'Large', 'ruby' )  				=> 'large'
						),
					),
					array(
						'param_name' => 'align',
						'heading' 	 => __( 'Align', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Left', 'ruby' )   				=> 'left',
							__( 'Center', 'ruby' ) 				=> 'center',
							__( 'Right', 'ruby' )  				=> 'right'
						),
					),
					array(
						'param_name' => 'button_style',
						'heading' 	 => __( 'Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Dark Grey', 'ruby' )   		=> 'dark_grey',
							__( 'Orange', 'ruby' ) 				=> 'orange',
							__( 'Dark Blue', 'ruby' )  			=> 'dark_blue',
							__( 'Dark Red', 'ruby' )  			=> 'dark_red',
							__( 'Light Grey', 'ruby' )  		=> 'light_grey',
							__( 'Light Blue', 'ruby' )  		=> 'light_blue',
							__( 'Green', 'ruby' )  				=> 'green',
							__( 'Custom', 'ruby' )  			=> 'custom',
						),
					),
					array(
						'param_name'  => 'fullwidth',
						'heading'     => __( 'Button Full Width', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => 'true'
						)
					),
					array(
						'param_name'  => 'color',
						'heading'     => __( 'Button Background Color', 'ruby' ),
						'type'        => 'colorpicker',
						'dependency' => array(
							'element' => 'button_style',
							'value'   => array( 'custom' )
						),
					),
					array(
						'param_name'  => 'text_color',
						'heading'     => __( 'Button Text Color', 'ruby' ),
						'type'        => 'colorpicker',
						'dependency' => array(
							'element' => 'button_style',
							'value'   => array( 'custom' )
						),
					),
					array(
						'param_name'  => 'hover_bg_color',
						'heading'     => __( 'Background Hover Color', 'ruby' ),
						'type'        => 'colorpicker',
						'dependency' => array(
							'element' => 'button_style',
							'value'   => array( 'custom' )
						),
					),
					array(
						'param_name'  => 'hover_text_color',
						'heading'     => __( 'Text Hover Color', 'ruby' ),
						'type'        => 'colorpicker',
						'dependency' => array(
							'element' => 'button_style',
							'value'   => array( 'custom' )
						),
					),
					array(
						'param_name'  => 'border_color',
						'heading'     => __( 'Button border Color', 'ruby' ),
						'type'        => 'colorpicker',
						'dependency' => array(
							'element' => 'button_style',
							'value'   => array( 'custom' )
						),
					),
					array(
						'param_name'  => 'border_width',
						'heading'     => __( 'Button border width', 'ruby' ),
						'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
						'dependency' => array(
							'element' => 'button_style',
							'value'   => array( 'custom' )
						),
					),
					array(
						'param_name'  => 'hover_border_color',
						'heading'     => __( 'Border Hover Color', 'ruby' ),
						'type'        => 'colorpicker',
						'dependency' => array(
							'element' => 'button_style',
							'value'   => array( 'custom' )
						),
					),
					array(
						'param_name'  => 'radius',
						'heading'     => __( 'Button radius', 'ruby' ),
						'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'pill',
						'heading'     => __( 'Pill', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => 'true'
						)
					),
					array(
						'param_name'  => 'd3',
						'heading'     => __( '3D', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => 'true'
						)
					),
					$k2t_margin_top,
					$k2t_margin_right,
					$k2t_margin_bottom,
					$k2t_margin_left,
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_button );

			/*  [ Circle button ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_circle_button = array(
				'base'            => 'circle_button',
				'name'            => __( 'Circle Button', 'ruby' ),
				'icon'            => 'fa fa-circle',
				'category'        => __( 'Typography', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'name',
						'heading'     => __( 'Button Name', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'link',
						'heading'     => __( 'Link To', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'icon_hover',
						'heading' 	 => __( 'Icon Hover', 'ruby' ),
						'type' 		 => 'k2t_icon',
						'value'      => '',
					),
					array(
						'param_name'  => 'background_color',
						'heading'     => __( 'Button Background Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_circle_button );

			/*  [ Counter ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_counter = array(
				'base'            => 'counter',
				'name'            => __( 'Counter', 'ruby' ),
				'icon'            => 'fa fa-list-ol',
				'category'        => __( 'Content', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name' => 'style_type',
						'heading' 	 => __( 'Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Icon Top', 'ruby' )  => '1',
							__( 'Icon Left', 'ruby' ) => '2',
						),
					),
					array(
						'param_name'  => 'border_width',
						'heading'     => __( 'Border Width', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
					),
					array(
						'param_name' => 'border_style',
						'heading'    => __( 'Border Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Solid', 'ruby' )  => 'solid',
							__( 'Dashed', 'ruby' ) => 'dashed'
						),
					),
					array(
						'param_name'  => 'border_color',
						'heading'     => __( 'Border', 'ruby' ),
						'type'        => 'colorpicker'
					),
					array(
						'param_name' => 'icon_type',
						'heading'    => __( 'Icon Type', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Icon font', 'ruby' )    => 'icon_font',
							__( 'Icon Graphic', 'ruby' ) => 'icon_graphic'
						),
					),
					array(
						'param_name'  => 'icon_font',
						'heading'     => __( 'Choose Icon', 'ruby' ),
						'type' 		 => 'k2t_icon',
						'value'      => '',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_font' ),
						),
					),
					array(
						'param_name'  => 'icon_size',
						'heading'     => __( 'Icon size', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_font' ),
						),
					),
					array(
						'param_name'  => 'icon_color',
						'heading'     => __( 'Icon Color', 'ruby' ),
						'type'        => 'colorpicker',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_font' ),
						),
					),
					array(
						'param_name'  => 'icon_background',
						'heading'     => __( 'Icon Background', 'ruby' ),
						'type'        => 'colorpicker',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_font' ),
						),
					),
					array(
						'param_name'  => 'icon_border_color',
						'heading'     => __( 'Icon Border', 'ruby' ),
						'type'        => 'colorpicker',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_font' ),
						),
					),
					array(
						'param_name' => 'icon_border_style',
						'heading'    => __( 'Icon Border Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Solid', 'ruby' )  => 'solid',
							__( 'Dashed', 'ruby' ) => 'dashed'
						),
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_font' ),
						)
					),
					array(
						'param_name'  => 'icon_border_width',
						'heading'     => __( 'Icon Border Width', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_font' ),
						),
					),
					array(
						'param_name'  => 'icon_border_radius',
						'heading'     => __( 'Icon Border Radius', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_font' ),
						),
					),
					array(
						'param_name'  => 'icon_graphic',
						'heading'     => __( 'Upload icon graphic', 'ruby' ),
						'type'        => 'attach_image',
						'holder'      => 'div',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_graphic' ),
						),
					),
					array(
						'param_name'  => 'number',
						'heading'     => __( 'Counter to number', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'number_font_size',
						'heading'     => __( 'Number font size', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'number_color',
						'heading'     => __( 'Number Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'title',
						'heading'     => __( 'Counter Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'title_font_size',
						'heading'     => __( 'Title font size', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'title_color',
						'heading'     => __( 'Title Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'speed',
						'heading'     => __( 'Animation Speed', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'delay',
						'heading'     => __( 'Animation Delay', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_counter );

			/*  [ Google Map ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_google_map = array(
				'base'            => 'google_map',
				'name'            => __( 'Google Maps', 'ruby' ),
				'icon'            => 'fa fa-map-marker',
				'category'        => __( 'Marketing', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'z',
						'heading'     => __( 'Zoom Level', 'ruby' ),
						'description' => __( 'Between 0-20', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'lat',
						'heading'     => __( 'Latitude', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'lon',
						'heading'     => __( 'Longitude', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'w',
						'heading'     => __( 'Width', 'ruby' ),
						'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'h',
						'heading'     => __( 'Height', 'ruby' ),
						'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'address',
						'heading'     => __( 'Address', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'marker',
						'heading' 	 => __( 'Marker', 'ruby' ),
						'type' 		 => 'checkbox',
						'value'      => array(
							'' => 'true'
						),
						'dependency' => array(
							'element' => 'address',
							'not_empty'   => true,
						),
					),
					array(
						'param_name'  => 'markerimage',
						'heading'     => __( 'Marker Image', 'ruby' ),
						'description' => __( 'Change default Marker.', 'ruby' ),
						'type'        => 'attach_image',
						'holder'      => 'div',
						'dependency' => array(
							'element' => 'marker',
							'value'   => array( 'true' ),
						),
					),
					array(
						'param_name' => 'traffic',
						'heading' 	 => __( 'Show Traffic', 'ruby' ),
						'type' 		 => 'checkbox',
						'value'      => array(
							'' => 'true'
						)
					),
					array(
						'param_name' => 'draggable',
						'heading' 	 => __( 'Draggable', 'ruby' ),
						'type' 		 => 'checkbox',
						'value'      => array(
							'' => 'true'
						)
					),
					array(
						'param_name' => 'infowindowdefault',
						'heading' 	 => __( 'Show Info Map', 'ruby' ),
						'type' 		 => 'checkbox',
						'value'      => array(
							'' => 'true'
						)
					),
					array(
						'param_name'  => 'infowindow',
						'heading'     => __( 'Content Info Map', 'ruby' ),
						'description' => __( 'Strong, br are accepted.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'hidecontrols',
						'heading' 	 => __( 'Hide Control', 'ruby' ),
						'type' 		 => 'checkbox',
						'value'      => array(
							'' => 'true'
						)
					),
					array(
						'param_name' => 'scrollwheel',
						'heading' 	 => __( 'Scroll wheel zooming', 'ruby' ),
						'type' 		 => 'checkbox',
						'value'      => array(
							'' => 'true'
						)
					),
					array(
						'param_name' => 'maptype',
						'heading' 	 => __( 'Map Type', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'ROADMAP', 'ruby' )   => 'ROADMAP',
							__( 'SATELLITE', 'ruby' ) => 'SATELLITE',
							__( 'HYBRID', 'ruby' )    => 'HYBRID',
							__( 'TERRAIN', 'ruby' )   => 'TERRAIN'
						),
					),
					array(
						'param_name' => 'mapstype',
						'heading' 	 => __( 'Map style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'None', 'ruby' )   => '',
							__( 'Subtle Grayscale', 'ruby' )   => 'grayscale',
							__( 'Blue water', 'ruby' ) => 'blue_water',
							__( 'Pale Dawn', 'ruby' ) => 'pale_dawn',
							__( 'Shades of Grey', 'ruby' ) => 'shades_of_grey',
						),
					),
					array(
						'param_name'  => 'color',
						'heading'     => __( 'Background Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_google_map );

			/*  [ Heading ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_heading = array(
				'base'            => 'heading',
				'name'            => __( 'Heading', 'ruby' ),
				'icon'            => 'fa fa-header',
				'category'        => __( 'Typography', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'content',
						'heading'     => __( 'Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
						'value'       => ''
					),
					array(
						'param_name'  => 'subtitle',
						'heading'     => __( 'Sub Title', 'ruby' ),
						'type'        => 'textarea',
						'holder'      => 'div',
						'value'       => ''
					),
					array(
						'param_name' => 'h',
						'heading' 	 => __( 'Heading Tag', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array( 
							__( 'H1', 'ruby' ) => 'h1', 
							__( 'H2', 'ruby' ) => 'h2', 
							__( 'H3', 'ruby' ) => 'h3', 
							__( 'H4', 'ruby' ) => 'h4', 
							__( 'H5', 'ruby' ) => 'h5', 
							__( 'H6', 'ruby' ) => 'h6', 
							__( 'Custom', 'ruby' ) => 'custom' ),
					),
					array(
						'param_name'  => 'font_size',
						'heading'     => __( 'Custom Font Size', 'ruby' ),
						'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
						'dependency' => array(
							'element' => 'h',
							'value'   => array( 'custom' )
						),
					),
					array(
						'param_name' => 'align',
						'heading' 	 => __( 'Align', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Left', 'ruby' )   => 'left',
							__( 'Center', 'ruby' ) => 'center',
							__( 'Right', 'ruby' )  => 'right'
						),
					),
					array(
						'param_name'  => 'font',
						'heading'     => __( 'Font', 'ruby' ),
						'description' => __( 'Use Google Font', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'border',
						'heading' 	 => __( 'Has border', 'ruby' ),
						'type' 		 => 'checkbox',
						'value'      => array(
							'' => 'true'
						)
					),
					array(
						'param_name' => 'border_style',
						'heading' 	 => __( 'Border Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Short Line', 'ruby' )   => 'short_line',
							__( 'Bottom Icon', 'ruby' ) => 'bottom_icon',
							__( 'Heading', 'ruby' )  => 'heading',
							__( 'Boxed Heading', 'ruby' )  => 'boxed_heading',
							__( 'Bottom Border', 'ruby' )  => 'bottom_border',
							__( 'Line Through', 'ruby' )  => 'line_through',
							__( 'Double Line', 'ruby' )  => 'double_line',
							__( 'Dotted Line', 'ruby' )  => 'three_dotted',
							__( 'Fat Line', 'ruby' )  => 'fat_line',
						),
						'dependency' => array(
							'element' => 'border',
							'value'   => array( 'true' )
						),
					),
					array(
						'param_name' => 'icon',
						'heading' 	 => __( 'Choose Icon', 'ruby' ),
						'type' 		 => 'k2t_icon',
						'value'      => '',
						'dependency' => array(
							'element' => 'border_style',
							'value'   => array( 'bottom_icon', 'boxed_heading' )
						),
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_heading );

			/*  [ Icon Box ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_icon_box = array(
				'base'            => 'iconbox',
				'name'            => __( 'Icon Box', 'ruby' ),
				'icon'            => 'fa fa-th',
				'category'        => __( 'Marketing', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name' => 'layout',
						'heading' 	 => __( 'Layout', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array( '1', '2', '3' ),
					),
					array(
						'param_name'  => 'title',
						'heading'     => __( 'Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'subtitle',
						'heading'     => __( 'Sub Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'fontsize',
						'heading'     => __( 'Title Font Size', 'ruby' ),
						'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'color',
						'heading'     => __( 'Title Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name' => 'text_transform',
						'heading' 	 => __( 'Text Transform', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Inherit', 'ruby' )    => 'inherit',
							__( 'Uppercase', 'ruby' )  => 'uppercase',
							__( 'Lowercase', 'ruby' )  => 'lowercase',
							__( 'Initial', 'ruby' )    => 'initial',
							__( 'Capitalize', 'ruby' ) => 'capitalize',
						),
					),
					array(
						'param_name' => 'icon_type',
						'heading' 	 => __( 'Icon Type', 'oneway' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							'Icon Fonts' => 'icon_fonts',
							'Graphics'   => 'graphics',
						)
					),
					array(
						'param_name' => 'graphic',
						'heading' 	 => __( 'Choose Images', 'oneway' ),
						'type' 		 => 'attach_image',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'graphics' )
						),
					),
					array(
						'param_name' => 'icon_hover',
						'heading' 	 => __( 'Enable hover effect', 'ruby' ),
						'type' 		 => 'checkbox',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_fonts' )
						),
						'value'      => array(
							'' => 'true'
						)
					),
					array(
						'param_name' => 'icon',
						'heading' 	 => __( 'Choose Icon', 'ruby' ),
						'type' 		 => 'k2t_icon',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_fonts' )
						),
						'value'      => '',
					),
					array(
						'param_name'  => 'icon_font_size',
						'heading'     => __( 'Icon size', 'ruby' ),
						'type'        => 'textfield',
						'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_fonts' )
						),
					),
					array(
						'param_name' => 'icon_color',
						'heading' 	 => __( 'Icon Color', 'oneway' ),
						'type' 		 => 'colorpicker',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_fonts' )
						),
					),
					array(
						'param_name' => 'icon_background',
						'heading' 	 => __( 'Icon Background', 'oneway' ),
						'type' 		 => 'colorpicker',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_fonts' )
						),
					),
					array(
						'param_name'  => 'icon_border_color',
						'heading'     => __( 'Icon Border Color', 'ruby' ),
						'type'        => 'colorpicker',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_fonts' ),
						),
					),
					array(
						'param_name' => 'icon_border_style',
						'heading'    => __( 'Icon Border Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Solid', 'ruby' )  => 'solid',
							__( 'Dashed', 'ruby' ) => 'dashed'
						),
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_fonts' ),
						)
					),
					array(
						'param_name'  => 'icon_border_width',
						'heading'     => __( 'Icon Border Width', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_fonts' ),
						),
					),
					array(
						'param_name'  => 'icon_border_radius',
						'heading'     => __( 'Icon Border Radius', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'icon_fonts' ),
						),
					),
					array(
						'param_name' => 'align',
						'heading' 	 => __( 'Icon Align', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Left', 'ruby' )   => 'left',
							__( 'Center', 'ruby' ) => 'center',
							__( 'Right', 'ruby' )  => 'right'
						),
					),
					array(
						'param_name'  => 'link',
						'heading'     => __( 'Link to', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'link_text',
						'heading'     => __( 'Link text', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'content',
						'heading'     => __( 'Content', 'ruby' ),
						'type'        => 'textarea_html',
						'holder'      => 'div',
						'value'       => ''
					),
					array(
						'param_name'  => 'box_background_color',
						'heading'     => __( 'Box background Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name' => 'box_border',
						'heading' 	 => __( 'Box Border', 'ruby' ),
						'type' 		 => 'checkbox',
						'value'      => array(
							'' => 'true'
						)
					),
					array(
						'param_name'  => 'box_border_color',
						'heading'     => __( 'Box Border Color', 'ruby' ),
						'type'        => 'colorpicker',
						'dependency' => array(
							'element' => 'box_border',
							'value'   => array( 'true' ),
						),
					),
					$k2t_margin_top,
					$k2t_margin_right,
					$k2t_margin_bottom,
					$k2t_margin_left,
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_icon_box );

			/*  [ Icon List ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_icon_list = array(
				'base'            => 'iconlist',
				'name'            => __( 'Icon List', 'ruby' ),
				'icon'            => 'fa fa-list',
				'category'        => __( 'Typography', 'ruby' ),
				'as_parent'       => array( 'only' => 'li' ),
				'content_element' => true,
				'js_view'         => 'VcColumnView',
				'params'          => array(
					array(
						'param_name' => 'icon',
						'heading' 	 => __( 'Choose Icon', 'ruby' ),
						'type' 		 => 'k2t_icon',
						'value'      => '',
					),
					array(
						'param_name'  => 'color',
						'heading'     => __( 'Icon Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			$k2t_icon_list_item = array(
				'base'            => 'li',
				'name'            => __( 'Icon List', 'ruby' ),
				'icon'            => 'fa fa-ellipsis-v',
				'category'        => __( 'Typography', 'ruby' ),
				'as_child'        => array( 'only' => 'iconlist' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'title',
						'heading'     => __( 'Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'icon',
						'heading' 	 => __( 'Choose Icon', 'ruby' ),
						'type' 		 => 'k2t_icon',
						'value'      => '',
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
				)
			);
			vc_map( $k2t_icon_list );
			vc_map( $k2t_icon_list_item );
			
			/*  [ Icon Box List ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_icon_box_list = array(
				'base'            => 'iconbox_list',
				'name'            => __( 'Icon Box List', 'ruby' ),
				'icon'            => 'fa fa-file-text',
				'category'        => __( 'Marketing', 'ruby' ),
				'as_parent'       => array( 'only' => 'li' ),
				'content_element' => true,
				'js_view'         => 'VcColumnView',
				'params'          => array(
					array(
						'param_name' => 'style',
						'heading' 	 => __( 'Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Style 1', 'ruby' ) => '1',
							__( 'Style 2', 'ruby' ) => '2',
							__( 'Style 3', 'ruby' ) => '3',
						)
					),
					array(
						'param_name' => 'align',
						'heading' 	 => __( 'Align', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Left', 'ruby' ) => 'left',
							__( 'Right', 'ruby' ) => 'right',
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => array( '3' )
						),
					),
					$k2t_id,
					$k2t_class
				)
			);
			$k2t_icon_box_list_item = array(
				'base'            => 'li',
				'name'            => __( 'Icon List Item', 'ruby' ),
				'icon'            => 'fa fa-ellipsis-v',
				'category'        => __( 'Typography', 'ruby' ),
				'as_child'        => array( 'only' => 'iconbox_list' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'title',
						'heading'     => __( 'Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'icon',
						'heading' 	 => __( 'Choose Icon', 'ruby' ),
						'type' 		 => 'k2t_icon',
						'value'      => '',
					),
					array(
						'param_name'  => 'content_icon_box',
						'heading'     => __( 'Content', 'ruby' ),
						'type'        => 'textarea',
						'holder'      => 'div',
						'value'       => ''
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
				)
			);
			vc_map( $k2t_icon_box_list );
			vc_map( $k2t_icon_box_list_item );

			/*  [ Member ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_member = array(
				'base'            => 'member',
				'name'            => __( 'Member', 'ruby' ),
				'icon'            => 'fa fa-user',
				'category'        => __( 'Common', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name' => 'style',
						'heading' 	 => __( 'Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Style 1', 'ruby' ) => '1',
							__( 'Style 2', 'ruby' ) => '2',
							__( 'Style 3', 'ruby' ) => '3',
							__( 'Style 4', 'ruby' ) => '4'
						),
					),
					array(
						'param_name'  => 'image',
						'heading'     => __( 'Member Avatar', 'ruby' ),
						'type'        => 'attach_image',
						'holder'      => 'div',
					),
					array(
						'param_name'  => 'name',
						'heading'     => __( 'Member Name', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'role',
						'heading'     => __( 'Role', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'facebook',
						'heading'     => __( 'Facebook URL', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'twitter',
						'heading'     => __( 'Twitter URL', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'skype',
						'heading'     => __( 'Skype', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'pinterest',
						'heading'     => __( 'Pinterest URL', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'instagram',
						'heading'     => __( 'Instagram', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'dribbble',
						'heading'     => __( 'Dribbble URL', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'google_plus',
						'heading'     => __( 'Google Plus URL', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'content',
						'heading'     => __( 'Member Info', 'ruby' ),
						'type'        => 'textarea_html',
						'holder'      => 'div',
						'value'       => ''
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_member );

			/*  [ Pie Chart ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_pie_chart = array(
				'base'            => 'piechart',
				'name'            => __( 'Pie Chart', 'ruby' ),
				'icon'            => 'fa fa-pie-chart',
				'category'        => __( 'Common', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'percent',
						'heading'     => __( 'Percent', 'ruby' ),
						'description' => __( 'Numeric value only, between 1-100.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'color',
						'heading'     => __( 'Outer Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'trackcolor',
						'heading'     => __( 'Track Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'textcolor',
						'heading'     => __( 'Text Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'textbackground',
						'heading'     => __( 'Text Background', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'title',
						'heading'     => __( 'Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'icon',
						'heading' 	 => __( 'Choose Icon', 'ruby' ),
						'type' 		 => 'k2t_icon',
						'value'      => '',
					),
					array(
						'param_name'  => 'text',
						'heading'     => __( 'Text', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'thickness',
						'heading'     => __( 'Thickness', 'ruby' ),
						'description' => __( 'Numeric value only.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'speed',
						'heading'     => __( 'Speed', 'ruby' ),
						'description' => __( 'Numeric value only, 1000 = 1second.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'delay',
						'heading'     => __( 'Delay', 'ruby' ),
						'description' => __( 'Numeric value only.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'size',
						'heading'     => __( 'Size', 'ruby' ),
						'description' => __( 'Numeric value only, size = width = height, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'linecap',
						'heading' 	 => __( 'Linecap', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Butt', 'ruby' )   => 'butt',
							__( 'Square', 'ruby' ) => 'square',
							__( 'Round', 'ruby' )  => 'round'
						),
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_pie_chart );

			/*  [ Pricing Table ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_pricing = array(
				'base'            => 'pricing',
				'name'            => __( 'Pricing Table', 'ruby' ),
				'icon'            => 'fa fa-table',
				'category'        => __( 'Marketing', 'ruby' ),
				'as_parent'       => array( 'only' => 'pricing_column' ),
				'content_element' => true,
				'js_view'         => 'VcColumnView',
				'params'          => array(
					array(
						'param_name' => 'separated',
						'heading' 	 => __( 'Separated', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'True', 'ruby' )  => 'true',
							__( 'False', 'ruby' ) => 'false',
						)
					),
					$k2t_id,
					$k2t_class
				)
			);
			$k2t_pricing_item = array(
				'base'            => 'pricing_column',
				'name'            => __( 'Pricing Columns', 'ruby' ),
				'icon'            => 'fa fa-table',
				'category'        => __( 'Marketing', 'ruby' ),
				'as_child'        => array( 'only' => 'pricing' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'title',
						'heading'     => __( 'Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'price',
						'heading'     => __( 'Price', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'old_price',
						'heading'     => __( 'Old Price', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'price_per',
						'heading'     => __( 'Price Per', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'unit',
						'heading'     => __( 'Unit', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'link',
						'heading'     => __( 'Link to', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'link_text',
						'heading'     => __( 'Link Text', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'target',
						'heading' 	 => __( 'Link Target', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Open in a new window', 'ruby' )                      => '_blank',
							__( 'Open in the same frame as it was clicked', 'ruby' )  => '_self'
						),
					),
					array(
						'param_name' => 'featured',
						'heading' 	 => __( 'Featured', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'False', 'ruby' ) => 'false',
							__( 'True', 'ruby' )  => 'true',
						)
					),
					array(
						'param_name' => 'featured_list',
						'heading' 	 => __( 'Featured List', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'False', 'ruby' ) => 'false',
							__( 'True', 'ruby' )  => 'true',	
						)
					),
					array(
						'param_name'  => 'color',
						'heading'     => __( 'Background Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'pricing_content',
						'heading'     => __( 'List Item', 'ruby' ),
						'description' => __( 'Using ul li tag.', 'ruby' ),
						'type'        => 'textarea_html',
						'holder'      => 'div',
						'value'       => ''
					),
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_pricing );
			vc_map( $k2t_pricing_item );

			/*  [ Process ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_process = array(
				'base'            => 'process',
				'name'            => __( 'Process', 'ruby' ),
				'icon'            => 'fa fa-arrows-h',
				'category'        => __( 'Common', 'ruby' ),
				'as_parent'       => array( 'only' => 'step' ),
				'content_element' => true,
				'js_view'         => 'VcColumnView',
				'params'          => array(
					array(
						'param_name'  => 'process_style',
						'heading' 	  => __( 'Process Style', 'ruby' ),
						'type' 		  => 'dropdown',
						'value'       => array(
							__( 'Line style', 'ruby' ) => 'style-line',
							__( 'Box style', 'ruby' ) => 'style-box',
						),
					),
					$k2t_id,
					$k2t_class
				)
			);
			$k2t_process_step = array(
				'base'            => 'step',
				'name'            => __( 'Step', 'ruby' ),
				'icon'            => 'fa fa-arrows-h',
				'category'        => __( 'Common', 'ruby' ),
				'as_child'        => array( 'only' => 'process' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'title',
						'heading'     => __( 'Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'icon',
						'heading' 	 => __( 'Choose Icon', 'ruby' ),
						'type' 		 => 'k2t_icon',
						'value'      => '',
					),
					array(
						'param_name'  => 'featured',
						'heading'     => __( 'Featured', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => 'true'
						)
					),
					array(
						'param_name'  => 'step_content',
						'heading'     => __( 'Text', 'ruby' ),
						'type'        => 'textarea',
						'holder'      => 'div',
						'value'       => ''
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_process );
			vc_map( $k2t_process_step );

			/*  [ Progress ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_progress = array(
				'base'            => 'progress',
				'name'            => __( 'Progress', 'ruby' ),
				'icon'            => 'fa fa-sliders',
				'category'        => __( 'Common', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'percent',
						'heading'     => __( 'Percent', 'ruby' ),
						'description' => __( 'Numeric value only, between 1-100.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'color',
						'heading'     => __( 'Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'background_color',
						'heading'     => __( 'Background Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'text_color',
						'heading'     => __( 'Text Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'title',
						'heading'     => __( 'Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'height',
						'heading'     => __( 'Height', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'striped',
						'heading'     => __( 'Striped', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => 'true'
						)
					),
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_progress );

			/*  [ Responsive Text ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_responsive_text = array(
				'base'            => 'responsive_text',
				'name'            => __( 'Responsive text', 'ruby' ),
				'icon'            => 'fa fa-arrows-alt',
				'category'        => __( 'Typography', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'compression',
						'heading'     => __( 'Compression', 'ruby' ),
						'description' => __( 'Numeric value only.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'min_size',
						'heading'     => __( 'Min Font Size', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'max_size',
						'heading'     => __( 'Max Font Size', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_responsive_text );

			/*  [ Sticky Tab ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_sticky_tab = array(
				'base'            => 'sticky_tab',
				'name'            => __( 'Sticky Tab', 'ruby' ),
				'icon'            => 'fa fa-bookmark',
				'category'        => __( 'Structure', 'ruby' ),
				'as_parent'       => array( 'only' => 'tab' ),
				'content_element' => true,
				'js_view'         => 'VcColumnView',
				'params'          => array(
					array(
						'param_name'  => 'background_color',
						'heading'     => __( 'Background Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'padding_top',
						'heading'     => __( 'Padding Top', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'padding_bottom',
						'heading'     => __( 'Padding Bottom', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_class
				)
			);
			/*  [ Tab ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_tab = array(
				'base'            => 'tab',
				'name'            => __( 'Tab item', 'ruby' ),
				'icon'            => 'fa fa-ellipsis-h',
				'category'        => __( 'Structure', 'ruby' ),
				'as_child'       => array( 'only' => 'sticky_tab' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'title',
						'heading'     => __( 'Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'content_tab',
						'heading'     => __( 'Content', 'ruby' ),
						'description' => __( 'Enter your content.', 'ruby' ),
						'type'        => 'textarea',
						'holder'      => 'div',
						'value'       => ''
					),
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_sticky_tab );
			vc_map( $k2t_tab );

			/*  [ Testimonial ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_textimonial = array(
				'base'            => 'testimonial',
				'name'            => __( 'Testimonial', 'ruby' ),
				'icon'            => 'fa fa-comments-o',
				'category'        => __( 'Marketing', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name' => 'style',
						'heading' 	 => __( 'Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Style 1', 'ruby' ) => '1',
							__( 'Style 2', 'ruby' ) => '2',
							__( 'Style 3', 'ruby' ) => '3',
						)
					),
					array(
						'param_name'  => 'image',
						'heading'     => __( 'Avatar', 'ruby' ),
						'description' => __( 'Choose avatar for testimonial author.', 'ruby' ),
						'type'        => 'attach_image',
						'holder'      => 'div',
					),
					array(
						'param_name' => 'align',
						'heading' 	 => __( 'Avatar Position', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Left', 'ruby' )  => 'left',
							__( 'Right', 'ruby' ) => 'right',
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => array( '1' )
						),
					),
					array(
						'param_name'  => 'name',
						'heading'     => __( 'Name', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'position',
						'heading'     => __( 'Position', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'link_name',
						'heading'     => __( 'Link to', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'target',
						'heading' 	 => __( 'Link Target', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Open in a new window', 'ruby' ) => '_blank',
							__( 'Open in the same frame as it was clicked', 'ruby' )  => '_self'
						),
					),
					array(
						'param_name'  => 'content',
						'heading'     => __( 'Text', 'ruby' ),
						'description' => __( 'Enter your testimonial.', 'ruby' ),
						'type'        => 'textarea_html',
						'holder'      => 'div',
						'value'       => ''
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_textimonial );

			/*  [ Blockquote ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_blockquote = array(
				'base'            => 'blockquote',
				'name'            => __( 'Blockquote', 'ruby' ),
				'icon'            => 'fa fa-quote-left',
				'category'        => __( 'Typography', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name' => 'style',
						'heading' 	 => __( 'Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Style 1', 'ruby' )   => '1',
							__( 'Style 2', 'ruby' )   => '2',
						),
					),
					array(
						'param_name' => 'align',
						'heading' 	 => __( 'Align', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Left', 'ruby' )   => 'left',
							__( 'Center', 'ruby' ) => 'center',
							__( 'Right', 'ruby' )  => 'right'
						),
					),
					array(
						'param_name'  => 'author',
						'heading'     => __( 'Author', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'link_author',
						'heading'     => __( 'Link to', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'content',
						'heading'     => __( 'Content', 'ruby' ),
						'type'        => 'textarea_html',
						'holder'      => 'div',
						'value'       => ''
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_blockquote );

			/*  [ Countdown ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_countdown = array(
				'base'            => 'countdown',
				'name'            => __( 'Countdown', 'ruby' ),
				'icon'            => 'fa fa-sort-numeric-desc',
				'category'        => __( 'Common', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'style',
						'heading'     => __( 'Countdown Style', 'ruby' ),
						'type' 		  => 'dropdown',
						'value'       => array(
							__( 'Square', 'ruby' )   			=> 'square',
							__( 'Square Fill Color', 'ruby' )   => 'square-fill',
							__( 'Circle', 'ruby' )   			=> 'circle',
							__( 'Circle Fill Color', 'ruby' )   => 'circle-fill',
							__( 'Solid', 'ruby' )  				=> 'solid',
						),
					),
					array(
						'param_name'  => 'time',
						'heading'     => __( 'Time', 'ruby' ),
						'description' => __( 'The time in this format: YYYY-MM-DD-HH-MM-SS', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'year',
						'heading'     => __( 'The word "Year(s)" in your language', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'month',
						'heading'     => __( 'The word "Month(s)" in your language', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'day',
						'heading'     => __( 'The word "Day(s)" in your language', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'hour',
						'heading'     => __( 'The word "Hour(s)" in your language', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'minute',
						'heading'     => __( 'The word "Minute(s)" in your language', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'second',
						'heading'     => __( 'The word "Second(s)" in your language', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'fontsize',
						'heading'     => __( 'Font Size', 'ruby' ),
						'description' => __( 'Numeric value only, unit is pixel', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'align',
						'heading' 	 => __( 'Align', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Left', 'ruby' )   => 'left',
							__( 'Center', 'ruby' ) => 'center',
							__( 'Right', 'ruby' )  => 'right'
						),
					),
					array(
						'param_name'  => 'background_color',
						'heading'     => __( 'Background Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'text_color',
						'heading'     => __( 'Number Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_countdown );

			/*  [ Embed ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_embed = array(
				'base'            => 'k2t_embed',
				'name'            => __( 'Embed', 'ruby' ),
				'icon'            => 'fa fa-terminal',
				'category'        => __( 'Media', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'width',
						'heading'     => __( 'Width', 'ruby' ),
						'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'content',
						'heading'     => __( 'URL or embed code', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
						'value'       => ''
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_embed );

			/*  [ K2T Slider ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_slider = array(
				'base'            => 'k2t_slider',
				'name'            => __( 'K2T Carousel', 'ruby' ),
				'icon'            => 'fa fa-exchange',
				'category'        => __( 'Content', 'ruby' ),
				'as_parent'       => array( 'only' => 'testimonial, vc_single_image, vc_raw_html, event' ),
				'js_view'         => 'VcColumnView',
				'content_element' => true,
				'params'          => array(
					array(
						'param_name' => 'style',
						'heading' 	 => __( 'Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Style 1', 'ruby' )    => 'style_1',
							__( 'Style 2', 'ruby' )    => 'style_2',
						),
					),
					array(
						'param_name'  => 'items',
						'heading'     => __( 'Slides per view', 'ruby' ),
						'description' => __( 'Numeric value only.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
					),
					array(
						'param_name'  => 'items_desktop',
						'heading'     => __( 'Slides per view on desktop', 'ruby' ),
						'description' => __( 'Item to display for desktop small (device width <= 1200px).', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
					),
					array(
						'param_name'  => 'items_tablet',
						'heading'     => __( 'Slides per view on tablet', 'ruby' ),
						'description' => __( 'Item to display for tablet (device width <= 768px).', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
					),
					array(
						'param_name'  => 'items_mobile',
						'heading'     => __( 'Slides per view on mobile', 'ruby' ),
						'description' => __( 'Item to display for mobile (device width <= 480px).', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
					),
					array(
						'param_name'  => 'single_item',
						'heading'     => __( 'Single Item', 'ruby' ),
						'description' => __( 'Display only one item.', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => true
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => array( 'style_1' ),
						),
					),
					array(
						'param_name'  => 'slide_speed',
						'heading'     => __( 'Slide speed', 'ruby' ),
						'description' => __( 'Slide speed in milliseconds.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div',
						'dependency' => array(
							'element' => 'style',
							'value'   => array( 'style_1' ),
						),
					),
					array(
						'param_name'  => 'auto_play',
						'heading'     => __( 'Auto Play', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => true
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => array( 'style_1' ),
						),
					),
					array(
						'param_name'  => 'stop_on_hover',
						'heading'     => __( 'Stop on hover', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => true
						),
						'dependency' => array(
							'element' => 'auto_play',
							'value'   => array( '1' ),
						),
					),
					array(
						'param_name'  => 'navigation',
						'heading'     => __( 'Navigation', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => true
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => array( 'style_1' ),
						),
					),
					array(
						'param_name'  => 'pagination',
						'heading'     => __( 'Pagination', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => true
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => array( 'style_1' ),
						),
					),
					array(
						'param_name' => 'pagi_pos',
						'heading' 	 => __( 'Pagination Position', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Top', 'ruby' )    => 'top',
							__( 'Bottom', 'ruby' ) => 'bottom',
							__( 'On Slider', 'ruby' ) => 'on_slider',
						),
						'dependency' => array(
							'element' => 'pagination',
							'value'   => array( '1' ),
						),
					),
					array(
						'param_name' => 'pagi_style',
						'heading' 	 => __( 'Pagination Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Style 1', 'ruby' ) => '1',
							__( 'Style 2', 'ruby' ) => '2',
						),
						'dependency' => array(
							'element' => 'pagination',
							'value'   => array( '1' ),
						),
					),
					array(
						'param_name'  => 'lazyLoad',
						'heading'     => __( 'LazyLoad', 'ruby' ),
						'description' => __( 'Delays loading of images. Images outside of viewport won\'t be loaded before user scrolls to them. Great for mobile devices to speed up page loadings.', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'value'       => array(
							'' => true
						)
					),
					$k2t_class
				)
			);
			vc_map( $k2t_slider );

			/*  [ Blog Post ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_blog_post = array(
				'base'            => 'blog_post',
				'name'            => __( 'Blog Post', 'ruby' ),
				'icon'            => 'fa fa-file-text',
				'category'        => __( 'Content', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name' => 'style',
						'heading' 	 => __( 'Choose Style', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Style 1', 'ruby' ) => '1',
							__( 'Style 2', 'ruby' ) => '2',
							__( 'Style 3', 'ruby' ) => '3',
							__( 'Style 4', 'ruby' ) => '4'
						),
					),
					array(
						'param_name'  => 'limit',
						'heading'     => __( 'Number of posts to show', 'ruby' ),
						'description' => __( 'Empty is show all posts.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'cat',
						'heading'     => __( 'Show posts associated with certain categories', 'ruby' ),
						'description' => __( 'Using category id, separate multiple categories with commas.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'slider',
						'heading' 	 => __( 'Enable Slider', 'ruby' ),
						'type' 		 => 'checkbox',
						'value'      => array(
							'' => true
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => array( '2', '4', '1' ),
						),
					),
					array(
						'param_name'  => 'items',
						'heading'     => __( 'Items', 'ruby' ),
						'description' => __( 'Numeric value only.', 'ruby' ),
						'type'        => 'textfield',
						'dependency' => array(
							'element' => 'slider',
							'value'   => array( '1' ),
						),
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'items_desktop',
						'heading'     => __( 'Items for Desktop small', 'ruby' ),
						'description' => __( 'Item to display for desktop small (device width <= 1366px).', 'ruby' ),
						'type'        => 'textfield',
						'dependency' => array(
							'element' => 'slider',
							'value'   => array( '1' ),
						),
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'items_tablet',
						'heading'     => __( 'Items for Tablet', 'ruby' ),
						'description' => __( 'Item to display for tablet (device width <= 768px).', 'ruby' ),
						'type'        => 'textfield',
						'dependency' => array(
							'element' => 'slider',
							'value'   => array( '1' ),
						),
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'items_mobile',
						'heading'     => __( 'Items for Mobile', 'ruby' ),
						'description' => __( 'Item to display for mobile (device width <= 480px).', 'ruby' ),
						'type'        => 'textfield',
						'dependency' => array(
							'element' => 'slider',
							'value'   => array( '1' ),
						),
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'navigation',
						'heading'     => __( 'Navigation', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'dependency' => array(
							'element' => 'slider',
							'value'   => array( '1' ),
						),
						'value'       => array(
							'' => 'true'
						)
					),
					array(
						'param_name'  => 'auto_play',
						'heading'     => __( 'Auto Play', 'ruby' ),
						'type'        => 'checkbox',
						'holder'      => 'div',
						'dependency' => array(
							'element' => 'slider',
							'value'   => array( '1' ),
						),
						'value'       => array(
							'' => 'true'
						)
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_blog_post );

			/*  [ Event ]
			- - - - - - - - - - - - - - - - - - - */
			$k2t_event = array(
				'base'            => 'event',
				'name'            => __( 'Events', 'ruby' ),
				'icon'            => 'fa fa-list-alt',
				'category'        => __( 'Content', 'ruby' ),
				'content_element' => true,
				'params'          => array(
					array(
						'param_name'  => 'event_title',
						'heading'     => __( 'Event Title', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'date',
						'heading'     => __( 'Event Date', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'month',
						'heading'     => __( 'Event Month', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'start_time',
						'heading'     => __( 'Start Time', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'end_time',
						'heading'     => __( 'End Time', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'location',
						'heading'     => __( 'Location', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'content',
						'heading'     => __( 'Content', 'ruby' ),
						'type'        => 'textarea_html',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'link',
						'heading'     => __( 'Link To', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name' => 'target',
						'heading' 	 => __( 'Link Target', 'ruby' ),
						'type' 		 => 'dropdown',
						'value'      => array(
							__( 'Open in a new window', 'ruby' )                      => '_blank',
							__( 'Open in the same frame as it was clicked', 'ruby' )  => '_self'
						),
					),
					array(
						'param_name'  => 'background_color',
						'heading'     => __( 'Event Background Color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'border_top_width',
						'heading'     => __( 'Border top width', 'ruby' ),
						'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'border_top_color',
						'heading'     => __( 'Border top color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					array(
						'param_name'  => 'border_bottom_width',
						'heading'     => __( 'Border bottom width', 'ruby' ),
						'description' => __( 'Numeric value only, Unit is Pixel.', 'ruby' ),
						'type'        => 'textfield',
						'holder'      => 'div'
					),
					array(
						'param_name'  => 'border_bottom_color',
						'heading'     => __( 'Border bottom color', 'ruby' ),
						'type'        => 'colorpicker',
					),
					$k2t_animation,
					$k2t_animation_name,
					$k2t_animation_delay,
					$k2t_id,
					$k2t_class
				)
			);
			vc_map( $k2t_event );
		}

		add_action( 'admin_init', 'k2t_vc_map_shortcodes' );

		/*  [ Extend container class (parents) ]
		- - - - - - - - - - - - - - - - - - - - - - - - - */
		class WPBakeryShortCode_Accordion extends WPBakeryShortCodesContainer {}
		class WPBakeryShortCode_Brands extends WPBakeryShortCodesContainer {}
		class WPBakeryShortCode_Iconlist extends WPBakeryShortCodesContainer {}
		class WPBakeryShortCode_Iconbox_List extends WPBakeryShortCodesContainer {}
		class WPBakeryShortCode_Pricing extends WPBakeryShortCodesContainer {}
		class WPBakeryShortCode_Process extends WPBakeryShortCodesContainer {}
		class WPBakeryShortCode_Sticky_Tab extends WPBakeryShortCodesContainer {}
		class WPBakeryShortCode_K2t_Slider extends WPBakeryShortCodesContainer {}

		/*  [ Extend shortcode class (children) ]
		- - - - - - - - - - - - - - - - - - - - - - - - - */
		class WPBakeryShortCode_Toggle extends WPBakeryShortCode {}
		class WPBakeryShortCode_Brand extends WPBakeryShortCode {}
		class WPBakeryShortCode_Li extends WPBakeryShortCode {}
		class WPBakeryShortCode_Pricing_Column extends WPBakeryShortCode {}
		class WPBakeryShortCode_Step extends WPBakeryShortCode {}
		class WPBakeryShortCode_Tab extends WPBakeryShortCode {}

	endif;
endif;

/*-------------------------------------------------------------------
	Remove Default Visual Composer Shortcode.
--------------------------------------------------------------------*/
if ( class_exists( 'Vc_Manager' ) ) :
	if ( ! function_exists( 'k2t_remove_default_shortcodes' ) ) :

		function k2t_remove_default_shortcodes() {
			vc_remove_element( 'vc_accordion' );
			vc_remove_element( 'vc_pie' );
			vc_remove_element( 'vc_posts_slider' );
			vc_remove_element( 'vc_widget_sidebar' );
			vc_remove_element( 'vc_button' );
			vc_remove_element( 'vc_carousel' );
			vc_remove_element( 'vc_button2' );
			vc_remove_element( 'vc_cta_button' );
			vc_remove_element( 'vc_cta_button2' );
			vc_remove_element( 'vc_progress_bar' );
			vc_remove_element( 'vc_gmaps' );
			vc_remove_element( 'vc_flickr' );
		}
		add_action( 'admin_init', 'k2t_remove_default_shortcodes' );

	endif;
endif;

/*-------------------------------------------------------------------
	Remove Teaser Metabox.
--------------------------------------------------------------------*/
if ( class_exists( 'Vc_Manager' ) ) :
	if ( is_admin() ) :
		if ( ! function_exists( 'k2t_vc_remove_teaser_metabox' ) ) :

		function k2t_vc_remove_teaser_metabox() {
			$post_types = get_post_types( '', 'names' ); 
			foreach ( $post_types as $post_type ) {
				remove_meta_box( 'vc_teaser',  $post_type, 'side' );
			}
		}

		add_action( 'do_meta_boxes', 'k2t_vc_remove_teaser_metabox' );

		endif;
	endif;
endif;

/*-------------------------------------------------------------------
	Incremental ID Counter for Templates.
--------------------------------------------------------------------*/
if ( class_exists( 'Vc_Manager' ) ) :
	if ( ! function_exists( 'k2t_vc_templates_id_increment' ) ) :

		function k2t_vc_templates_id_increment() {
			static $count = 0; $count++;
			return $count;
		}

	endif;
endif;

