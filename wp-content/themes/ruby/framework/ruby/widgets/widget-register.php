<?php
/**
 * Register widget.
 *
 * @package Ruby
 * @author  KingKongThemes
 * @link http://www.kingkongthemes.com
 */

include_once get_template_directory() . '/framework/ruby/widgets/recent-post.php';
include_once get_template_directory() . '/framework/ruby/widgets/facebook.php';
include_once get_template_directory() . '/framework/ruby/widgets/google-plus.php';
?>
