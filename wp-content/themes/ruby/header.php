<?php
/**
 * The header for theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Ruby
 * @author  KingKongThemes
 * @link	http://www.kingkongthemes.com
 */

// Get theme options
global $smof_data;

$classes = array();

// Vertical menu class
if ( ! empty( $smof_data['vertical-menu'] ) ) {
	$classes[] = 'vertical-menu';
}
// Fixed header
if ( ! empty( $smof_data['fixed-header'] ) && ( ! $smof_data['vertical-menu'] ) ) {
	$classes[] = 'fixed';
}
// Full width header
if ( ! empty( $smof_data['full-header'] ) && ( ! $smof_data['vertical-menu'] ) ) {
	$classes[] = 'full';
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php esc_url( bloginfo( 'pingback_url' ) ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php if ( ! empty( $smof_data['pageloader'] ) ) : ?>
	<div id="loader-wrapper">
		<div class="loader"></div>
	</div>
<?php endif; ?>

<div class="k2t-container">

	<header class="k2t-header <?php echo esc_attr( implode( ' ', $classes ) ); ?>" role="banner">
		
		<?php
			// Include top header layout
			if ( ! $smof_data['vertical-menu'] ) {
				if ( ! empty( $smof_data['use-top-header'] ) ) {
					include_once RUBY_TEMPLATE_TMPL . '/header/top.php';
				}
			}

			// Include middle header layout
			if ( ! empty( $smof_data['use-mid-header'] ) ) {
				include_once RUBY_TEMPLATE_TMPL . '/header/mid.php';
			}

			// Include bottom header layout
			if ( ! $smof_data['vertical-menu'] ) {
				if ( ! empty( $smof_data['use-bot-header'] ) ) {
					include_once RUBY_TEMPLATE_TMPL . '/header/bot.php';
				}
			}

			include_once RUBY_TEMPLATE_TMPL . '/header/responsive.php';
		?>

	</header><!-- .k2t-header -->

	<div class="k2t-body">

		<?php include_once RUBY_TEMPLATE_TMPL . '/titlebar/title-bar.php'; ?>