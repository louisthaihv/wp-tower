<?php
/**
 * The main template file.
 *
 * @package Ruby
 * @author  KingKongThemes
 * @link	http://www.kingkongthemes.com
 */

// Get theme options
global $smof_data;

$classes = array();

// Get blog layout
$blog_layout = $smof_data['blog-sidebar-position'];

// Get blog style
$blog_style = $smof_data['blog-style'];

if ( 'right_sidebar' == $blog_layout ) {
	$classes[] = 'right-sidebar';
} elseif ( 'left_sidebar' == $blog_layout ) {
	$classes[] = 'left-sidebar';
} elseif ( 'two_sidebars' == $blog_layout ) {
	$classes[] = 'two-sidebars';
} else {
	$classes[] = 'no-sidebar';
}

if ( $blog_style ) {
	$classes[] = 'b-' . $blog_style;
}

// Get columns of blog masonry
$columns = $smof_data['blog-masonry-column'];

// Blog masonry full width
$fullwidth = ( isset ( $smof_data['blog-masonry-full-width'] ) && $smof_data['blog-masonry-full-width'] ) ? ' fullwidth' : '';

get_header(); ?>

	<div class="k2t-content <?php echo esc_attr( implode( ' ', $classes ) . $fullwidth ); ?>">

		<div class="k2t-wrap">

			<?php
				if ( 'two_sidebars' == $blog_layout ) {
					echo '<div class="k2t-btc">';

					if ( 'small' == $blog_style ) {
						$featured = new WP_Query( 'category_name=featured&posts_per_page=1' );

						while ( $featured->have_posts() ) : $featured->the_post();				
							include_once RUBY_TEMPLATE_TMPL . '/blog/content-featured.php';
						endwhile;

						// Reset global query object
						wp_reset_postdata();
					}

					// Get secondary sidebar for blog 3 columns
					get_sidebar( 'sub' );
				}
			?>
			<main class="k2t-blog" role="main">
				
				<?php
					if ( 'masonry' == $blog_style ) {
						echo '<div class="masonry-layout ' . esc_attr( $columns ) . '">';
						echo '<div class="grid-sizer"></div>';
					}

					if ( 'timeline' == $blog_style ) {
						echo '<div class="k2t-blog-timeline">';
						if ( ! $smof_data['blog-timeline-image'] ) {
							echo '<div class="tl-dot"></div>';
						} else {
							echo '<img class="tl-avatar" src="' . esc_url( $smof_data['blog-timeline-image'] ) . '" alt="Timeline Image" />';
						}
						echo '<div class="tl-bar"></div>';
					}
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							
							if ( 'large' == $blog_style ) {
								include RUBY_TEMPLATE_TMPL . '/blog/content-large.php';
							} elseif ( 'medium' == $blog_style ) {
								include RUBY_TEMPLATE_TMPL . '/blog/content-medium.php';
							} elseif ( 'small' == $blog_style ) {
								include RUBY_TEMPLATE_TMPL . '/blog/content-small.php';
							} elseif ( 'timeline' == $blog_style ) {
								include RUBY_TEMPLATE_TMPL . '/blog/content-timeline.php';
							} elseif ( 'masonry' == $blog_style ) {
								include RUBY_TEMPLATE_TMPL . '/blog/content-masonry.php';
							}
						
						endwhile;
					else :
						get_template_part( 'content', 'none' );
					endif;

					if ( 'timeline' == $blog_style || 'masonry' == $blog_style ) {
						echo '</div>';
					}

					include_once RUBY_TEMPLATE_TMPL . '/navigation.php';

				?>

			</main><!-- .k2t-main -->

			<?php
				if ( 'two_sidebars' == $blog_layout ) {
					echo '</div>';
				}
				if ( 'no_sidebar' != $blog_layout ) {
					get_sidebar();
				}
			?>

		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-content -->

<?php get_footer(); ?>
