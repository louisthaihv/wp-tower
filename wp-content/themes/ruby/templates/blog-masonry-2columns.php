<?php
/**
 * The blog template file.
 *
 * @package Ruby
 * @author  KingKongThemes
 * @link	http://www.kingkongthemes.com
 * Template Name: Blog masonry 2 Columns
 */

// Get theme options
global $smof_data;

$classes = array();

// Get blog layout
$blog_layout = $smof_data['blog-sidebar-position'];

get_header(); ?>

	<div class="k2t-content no-sidebar b-masonry" style="padding-top: 70px;">

		<div class="k2t-wrap">

			<main class="k2t-blog" role="main">
				
				<div class="masonry-layout column-2">
					<div class="grid-sizer"></div>
					<?php
					$args = array(
						'post_type'      => 'post',
						'posts_per_page' => -1,
					);
					$blog = new WP_query( $args );
					while ( $blog->have_posts() ) : $blog->the_post();
					// Get post format
					$post_format = get_post_format();
					$link        = ( function_exists( 'get_field' ) ) ? get_field( 'link_format_url', get_the_ID() ) : '';
					$large = ( function_exists( 'get_field' ) ) ? get_field( 'post_large', get_the_ID() ) : '';
					?>

						<article id="post-<?php the_ID(); ?>" <?php post_class( $large ); ?>>
	
							<?php
								include get_template_directory() . '/framework/ruby/tmpl/blog/post-format.php';

								if ( 'quote' != $post_format ) :
							?>
							
							<div class="k2t-text">

								<div class="k2t-meta">
									<?php
									if ( 'link' == $post_format ) {
										the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( $link ) ), '</a></h2>' );
									} else {
										if ( $smof_data['blog-post-link'] ) {
											the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
										}
									}

									if ( $smof_data['blog-author'] ) { ?>
										<div class="post-author">
											<?php echo sprintf( __( '<span>%s</span> / ', 'ruby' ), get_the_author_link() );?>
										</div>
									<?php } 

									if ( $smof_data['blog-date'] ) { ?>
										<div class="posted-on">
											<?php the_time( 'j M Y' ); ?>
										</div>
									<?php } ?>
								</div><!-- .k2t-meta -->

								<div class="k2t-entry">
									<?php
										if ( 'excerpts' == $smof_data['blog-display'] ) {
											echo $trimmed_content = wp_trim_words( get_the_content(), $smof_data['excerpt-length'], '<a class="more-link" href="'. esc_url( get_permalink() ) .'">Read More</a>' );
										} else {
											if ( $smof_data['blog-readmore'] ) {
												the_content( sprintf( __( 'Read more', 'ruby' ) ) );
											} else {
												the_content( sprintf( __( ' ', 'ruby' ) ) );
											}	
										}
									?>
								</div><!-- .k2t-entry -->

							</div>

							<?php endif; ?>

						</article><!-- #post-## -->
						
					<?php endwhile;
					include_once get_template_directory() . '/framework/ruby/tmpl/navigation.php';
					?>

			</main><!-- .k2t-main -->

		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-content -->

<?php get_footer(); ?>
<script>
(function($) {
	"use strict";

	$(window).load(function() {
		var container = document.querySelector('.b-masonry .masonry-layout');
		var msnry = new Masonry( container, {
			itemSelector: '.hentry',
			columnWidth: container.querySelector('.grid-sizer')
		});
	});
})(jQuery);
</script>