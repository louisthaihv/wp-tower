<?php
/**
 * The blog template file.
 *
 * @package Ruby
 * @author  KingKongThemes
 * @link	http://www.kingkongthemes.com
 * Template Name: Blog small left sidebar
 */

// Get theme options
global $smof_data;
// Get post format
$post_format = get_post_format();
$link        = ( function_exists( 'get_field' ) ) ? get_field( 'link_format_url', get_the_ID() ) : '';
get_header(); ?>

	<div class="k2t-content b-small left-sidebar" style="padding-top: 60px;">

			<div class="k2t-wrap">	
				<div class="k2t-btc">

					<?php
						$featured = new WP_Query( 'category_name=featured&posts_per_page=1' );

						while ( $featured->have_posts() ) : $featured->the_post(); ?>

							<div id="post-<?php the_ID(); ?>" <?php post_class( 'k2t-featured-post' ); ?>>
								<div class="k2t-thumb">
									<?php
										$thumbnail = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );
										$image     = aq_resize( $thumbnail, 380, 380, true );
										if ( has_post_thumbnail() ) :
											echo '<img src="' . esc_url( $image ) . '" alt="' . get_the_title() . '" />';
										else :
											echo '<img src="' . esc_url( get_template_directory_uri() . '/assets/img/placeholder/380x380.png' ) .'" alt="' . get_the_title() . '" />';
										endif;
									?>
									<div class="mask"><a href="<?php the_permalink(); ?>"><i class="fa fa-link"></i></a></div>
								</div><!-- .k2t-thumb -->
								<div class="k2t-entry">
									<span class="ribbon"><?php _e( 'Featured', 'ruby' ); ?></span>
									<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

									<div class="k2t-meta">
										<div class="post-author">
											<?php echo sprintf( __( 'Posted by <span>%s</span>', 'ruby' ), get_the_author_link() );?>
										</div>
										<div class="posted-on">
											<i class="fa fa-clock-o"></i><?php the_time( 'j M Y'); ?>
										</div>
										<?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
											<div class="post-comment">
												<a href="<?php comments_link(); ?>"><i class="fa fa-comments"></i><?php comments_number( '0 Comment', '1 Comment', '% Comments' ); ?></a>
											</div>
										<?php endif; ?>
									</div><!-- .k2t-meta -->

									<?php the_content( 'Read More' ); ?>

									<div class="other-post">
										<h2><?php _e( 'Other Posts', 'ruby' ); ?></h2>
										<?php
										// Build arguments to query for related posts
										$args = array(
											'category_name'    => 'featured',
											'post__not_in'     => array( $post->ID ),
											'posts_per_page'   => 3,
											'orderby'          => 'rand',
										);
										$related = new WP_Query( $args );
										echo '<ul>';
											while ( $related->have_posts() ) : $related->the_post();
												the_title( sprintf( '<li><i class="fa fa-caret-right"></i><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></li>' );
											endwhile;
											// Reset global query object
											wp_reset_postdata();
										echo '</ul>';
										?>
									</div><!-- .other-post -->
								</div><!-- .k2t-entry -->
							</div><!-- .k2t-featured-post -->
						<?php
						endwhile;

						// Reset global query object
						wp_reset_postdata();
					get_sidebar();
					?>
					<main class="k2t-blog" role="main">

						<?php
							$args = array(
								'post_type'      => 'post',
								'posts_per_page' => get_option('posts_per_page '),
							);
							$blog = new WP_query( $args );
							if ( $blog->have_posts() ) :
								while ( $blog->have_posts() ) : $blog->the_post(); ?>
									
								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<div class="k2t-thumb" style="max-width: 250px;">
										<?php
					
											$thumbnail = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );
											$image     = aq_resize( $thumbnail, 250, 250, true );
											if ( has_post_thumbnail() ) :
												echo '<img src="' . esc_url( $image ) . '" alt="' . get_the_title() . '" />';
											else :
												echo '<img src="' . esc_url( get_template_directory_uri() . '/assets/img/placeholder/250x250.png' ) .'" alt="' . get_the_title() . '" />';
											endif;
										?>
										<div class="mask"><a href="<?php the_permalink(); ?>"><i class="fa fa-link"></i></a></div>
									</div>
									<?php

										if ( 'quote' != $post_format ) :
									?>

										<div class="k2t-entry">
											<div class="k2t-meta">
												<?php if ( $smof_data['blog-author'] ) { ?>
													<div class="post-author">
														<?php echo sprintf( __( 'Posted by <span>%s</span>', 'ruby' ), get_the_author_link() );?>
													</div>
												<?php }

												if ( $smof_data['blog-date'] ) { ?>
													<div class="posted-on">
														<i class="fa fa-clock-o"></i><?php the_time( 'j M Y' ); ?>
													</div>
												<?php }

												if ( $smof_data['blog-number-comment'] ) {
													if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
														<div class="post-comment">
															<a href="<?php comments_link(); ?>"><i class="fa fa-comments"></i><?php comments_number( '0 Comment', '1 Comment', '% Comments' ); ?></a>
														</div>
												<?php
													endif;
												}

												?>
											</div><!-- .k2t-meta -->

											<?php
												the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
												the_content( 'Read More' );
											?>
										</div><!-- .k2t-entry -->
									<?php endif; ?>

								</article><!-- #post-## -->
									
								
							<?php
								endwhile;
							endif;
						?>

					</main><!-- .k2t-main -->
				</div>

		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-content -->

<?php get_footer(); ?>
