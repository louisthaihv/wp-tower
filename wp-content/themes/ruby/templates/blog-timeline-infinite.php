<?php
/**
 * The blog timeline template file.
 *
 * @package Ruby
 * @author  KingKongThemes
 * @link	http://www.kingkongthemes.com
 * Template Name: Blog Timeline infinite scroll
 */

// Get theme options
global $smof_data, $post;

// Get post format
$post_format = get_post_format();
$link        = ( function_exists( 'get_field' ) ) ? get_field( 'link_format_url', get_the_ID() ) : '';
get_header(); ?>

	<div class="k2t-content no-sidebar b-timeline">

		<div class="k2t-wrap">
			<main class="k2t-blog" role="main">

				<?php
					echo '<div class="k2t-blog-timeline">';
					echo '<div class="tl-dot"></div>';
					echo '<div class="tl-bar"></div>';
					$temp = $wp_query;
					$wp_query= null;
					$wp_query = new WP_Query();
					$wp_query->query( 'showposts=5' . '&paged=' . $paged );

					if ( $wp_query->have_posts() ) :
						while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
								<?php
								if ( 'quote' != $post_format ) : ?>

									<div class="tl-author">
										<div class="author-avatar">
											<?php echo get_avatar( get_the_author_meta( 'ID' ), 50 ); ?>
										</div>
										<div class="k2t-meta">
											<?php if ( $smof_data['blog-author'] ) { ?>
												<div class="post-author">
													<?php echo sprintf( __( 'Posted By <span>%s</span>', 'ruby' ), get_the_author_link() );?>
												</div>
											<?php }
											
											if ( $smof_data['blog-date'] ) { ?>
												<div class="posted-on">
													<i class="fa fa-clock-o"></i><span><?php the_time( 'j M Y' ); ?></span>
												</div>
											<?php }

											if ( $smof_data['blog-number-comment'] ) {

												if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
												<div class="post-comment">
													<a href="<?php comments_link(); ?>"><i class="fa fa-comments-o"></i><?php comments_number( '0 Comment', '1 Comment', '% Comments' ); ?></a>
												</div>
											<?php
												endif;
											}
											?>
										</div><!-- .k2t-meta -->
									</div><!-- .tl-author -->

									<?php include get_template_directory() . '/framework/ruby/tmpl/blog/post-format.php'; ?>

									<div class="k2t-entry">
										<?php
											if ( $smof_data['blog-post-link'] ) {
												the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
											}

											the_content( 'Read More' );
										?>
									</div><!-- .k2t-entry -->
								<?php endif; ?>

							</article><!-- #post-## -->
						<?php 
						endwhile;
					endif;
				?>
				</div>

				<div class="nav-seemore">
					<div class="nav-seemore-inner">
						<?php echo next_posts_link( __( 'Load More', 'ruby' ) ); ?>
					</div>
				</div>
				<?php
				$wp_query = null; $wp_query = $temp;
				?>
			</main><!-- .k2t-main -->

		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-content -->
<?php wp_enqueue_script( 'k2t-owlcarousel' ); wp_enqueue_script( 'infinitescroll-script' ); wp_enqueue_script( 'jquery-imageloaded-script' ); ?>
<script>
	jQuery(window).load(function($) {
		var $ = jQuery;
		function timeline_indicator() {
			var post = $( ".b-timeline" ).find( ".hentry" );
			$.each( post, function( i,obj ) {           
				var posLeft = $( obj ).css( "left" );
				if( posLeft == "0px" ) {
					$(obj).addClass( "post-left" );
				} else {
					$(obj).addClass( "post-right" );
				}
			});
		}

		// Pagination load more
		function timeline_pagination() {
			var $container = $( ".k2t-blog-timeline" );
			$container.imagesLoaded(function(){
				$container.masonry({
					itemSelector: '.hentry'
				});
			});
			$container.isotope({
				itemSelector : ".hentry"
			}); 
			$container.infinitescroll({
				navSelector: ".nav-seemore-inner", // selector for the paged navigation
				nextSelector: ".nav-seemore-inner a", // selector for the NEXT link (to page 2)
				itemSelector: ".hentry", // selector for all items you"ll retrieve
				loading: {
					finishedMsg: "No more pages to load.",
					img: "http://i.imgur.com/qkKy8.gif"
				}
			},
				function( newElements ) {
					// hide new items while they are loading
					var $newElems = $( newElements ).css({ opacity: 0 });
					// ensure that images load before adding to masonry layout
					$newElems.imagesLoaded(function(){
						// show elems now they're ready
						$newElems.animate({ opacity: 1 });
						$container.isotope( "appended", $( newElements ) );
					});

					// Carousel for post format gallery
					if ( $().owlCarousel ) {
						console.log($( ".k2t-thumb-gallery" ).attr('class'));
						$( ".k2t-thumb-gallery" ).each(function(){
							$(this).owlCarousel({
								singleItem 		: true,
								navigation 		: true,
								pagination 		: false,
								navigationText	: [
									'<i class="fa fa-angle-left"></i>',
									'<i class="fa fa-angle-right"></i>'
								],
							});
						});
					}

					timeline_indicator();
				} 
			);
		}

		var container = document.querySelector(".k2t-blog-timeline");
		var msnry = new Masonry( container, {
			itemSelector: ".hentry",
			columnWidth: container.querySelector(".hentry")
		});

		msnry.on( "layoutComplete", function() {
			timeline_indicator();
			timeline_pagination();
		});
		// manually trigger initial layout
		msnry.layout();
	});
</script>
<?php get_footer(); ?>
