<?php
/**
 * The blog timeline right sidebar template file.
 *
 * @package Ruby
 * @author  KingKongThemes
 * @link	http://www.kingkongthemes.com
 * Template Name: Blog Timeline right sidebar
 */

// Get theme options
global $smof_data;
// Get post format
$post_format = get_post_format();
$link        = ( function_exists( 'get_field' ) ) ? get_field( 'link_format_url', get_the_ID() ) : '';
get_header(); ?>

	<div class="k2t-content right-sidebar b-timeline">

		<div class="k2t-wrap">
			<main class="k2t-blog" role="main">

				<?php
					echo '<div class="k2t-blog-timeline">';
					echo '<div class="tl-dot"></div>';
					echo '<div class="tl-bar"></div>';
					$args = array(
			            'post_type'      => 'post',
			            'posts_per_page' => -1,
			        );
			        $blog = new WP_query( $args );
					if ( $blog->have_posts() ) :
						while ( $blog->have_posts() ) : $blog->the_post(); ?>
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
								<?php
								if ( 'quote' != $post_format ) : ?>

									<div class="tl-author">
										<div class="author-avatar">
											<?php echo get_avatar( get_the_author_meta( 'ID' ), 50 ); ?>
										</div>
										<div class="k2t-meta">
											<?php if ( $smof_data['blog-author'] ) { ?>
												<div class="post-author">
													<?php echo sprintf( __( 'Posted By <span>%s</span>', 'ruby' ), get_the_author_link() );?>
												</div>
											<?php }
											
											if ( $smof_data['blog-date'] ) { ?>
												<div class="posted-on">
													<i class="fa fa-clock-o"></i><span><?php the_time( 'j M Y' ); ?></span>
												</div>
											<?php }

											if ( $smof_data['blog-number-comment'] ) {

												if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
												<div class="post-comment">
													<a href="<?php comments_link(); ?>"><i class="fa fa-comments-o"></i><?php comments_number( '0 Comment', '1 Comment', '% Comments' ); ?></a>
												</div>
											<?php
												endif;
											}
											?>
										</div><!-- .k2t-meta -->
									</div><!-- .tl-author -->

									<?php include get_template_directory() . '/framework/ruby/tmpl/blog/post-format.php'; ?>

									<div class="k2t-entry">
										<?php
											if ( $smof_data['blog-post-link'] ) {
												the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
											}

											the_content( 'Read More' );
										?>
									</div><!-- .k2t-entry -->
								<?php endif; ?>

							</article><!-- #post-## -->
						<?php 
						endwhile;
					endif;
				?>
				</div>
			</main><!-- .k2t-main -->

			<?php get_sidebar(); ?>

		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-content -->

<script>
	jQuery(window).load(function($) {
		var $ = jQuery;
		function timeline_indicator() {
			var post = $( ".k2t-blog-timeline" ).find( ".hentry" );
			$.each( post, function( i,obj ) {           
				var posLeft = $( obj ).css( "left" );
				if( posLeft == "0px" ) {
					$(obj).addClass( "post-left" );
				} else {
					$(obj).addClass( "post-right" );
				}
			});
		}

		var container = document.querySelector(".k2t-blog-timeline");
		var msnry = new Masonry( container, {
			itemSelector: ".hentry",
			columnWidth: container.querySelector(".hentry")
		});

		msnry.on( "layoutComplete", function() {
			timeline_indicator();
		});
		// manually trigger initial layout
		msnry.layout();
	});
</script>
<?php get_footer(); ?>
