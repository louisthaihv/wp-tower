<?php
/**
 * The template for displaying footer version 1.
 *
 * @package Ruby
 * @author  KingKongThemes
 * @link	http://www.kingkongthemes.com
 * Template Name: Footer Version 1
 */


get_header(); ?>

	<div class="k2t-content no-sidebar">

		<main class="k2t-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
		
	</div><!-- .k2t-content -->

<?php get_footer(); ?>