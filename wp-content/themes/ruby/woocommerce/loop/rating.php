<?php
/**
 * Loop Rating
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product;

if ( get_option( 'woocommerce_enable_review_rating' ) === 'no' )
	return;

echo '<div class="p-top">';
	if ( $rating_html = $product->get_rating_html() ) :
		echo $rating_html;
	endif;

	if ( class_exists( 'YITH_WCWL_UI' ) ) :
		echo k2t_template_woo::k2t_wishlist_button();
	endif;
echo '</div>';