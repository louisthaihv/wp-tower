<?php
/**
 * The main template file.
 *
 * @package Tower
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

get_header(); ?>

	<section class="k2t-content b-large">
		<div class="k2t-wrap">
			<main class="k2t-main" role="main">

				<?php
					if ( have_posts() ) :

						while ( have_posts() ) : the_post();
							include RUBY_TEMPLATE_TMPL . '/blog/content-large.php';
						endwhile;

					else :
						get_template_part( 'content', 'none' );
					endif;
				?>

			</main><!-- #main -->

			<?php get_sidebar(); ?>

		</div><!-- .k2t-wrap -->
	</section><!-- .k2t-content -->

<?php get_footer(); ?>