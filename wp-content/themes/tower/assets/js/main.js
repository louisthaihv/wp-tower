/**
 * Custom script for Furion.
 *
 * @package Furion
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */
 
(function($) {
	"use strict";

	$(document).ready(function() {

		/*  [ Detecting Mobile Devices ]
		- - - - - - - - - - - - - - - - - - - - */
		var isMobile = {
			Android: function() {
				return navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function() {
				return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
			},
			Desktop: function() {
				return window.innerWidth <= 960;
			},
			any: function() {
				return ( isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows() || isMobile.Desktop() );
			}
		}

		/*  [ Sticky menu trigger ]
		- - - - - - - - - - - - - - - - - - - - */
		var header_sticky = '';
		if ( mainParams.fixed_header != 1 ) {
			if ( mainParams.sticky_menu == 'sticky_top' ) {
				var nav = $(".k2t-header-top");
				var waypoint_offset = 20;
			} else if ( mainParams.sticky_menu == 'sticky_mid' ) {
				var nav = $(".k2t-header-mid");
				var waypoint_offset = 50;
			} else if ( mainParams.sticky_menu == 'sticky_bot' ) {
				var nav = $(".k2t-header-bot");
				var waypoint_offset = 30;
			}
			
			var container = $( '.k2t-header' );
			var top_spacing = 0;
			$(window).on('scroll', function(){
				scrollFunc();
				var container = $( '.k2t-header' );
				if ( mainParams.smart_sticky == 1 )	{// smart sticky menu 
					if ( mainParams.sticky_menu == 'sticky_top' || mainParams.sticky_menu == 'sticky_mid' || mainParams.sticky_menu == 'sticky_bot' ) {
						if ( __k2t_check_updown > 0 && !$('body').hasClass('header-sticky') ) {
							container.css({
								'height': nav.outerHeight()
							});
							nav.stop().addClass('sticky').css('top', - nav.outerHeight() ).animate({
								'top': top_spacing
							});
							$('body').addClass('header-sticky');
						} else if ( __k2t_check_updown < 0 || __k2t_check_updown == 0 ) {
							container.css({
								'height': 'auto'
							});
							nav.stop().removeClass('sticky').css('top', nav.outerHeight() + waypoint_offset).animate({
								'top': ''
							});
							$('body').removeClass('header-sticky');
						}
					}
				}
				else // normal sticky menu 
				{
					if ( mainParams.sticky_menu == 'sticky_top' || mainParams.sticky_menu == 'sticky_mid' || mainParams.sticky_menu == 'sticky_bot' ) {
						if ( __k2t_check_updown < 0 && !$('body').hasClass('header-sticky') ) {
							container.css({
								'height': nav.outerHeight()
							});
							nav.stop().addClass('sticky').css('top', - nav.outerHeight() ).animate({
								'top': top_spacing
							});
							$('body').addClass('header-sticky');
						} else if ( __k2t_check_updown == 0 ) {
							container.css({
								'height': 'auto'
							});
							nav.stop().removeClass('sticky').css('top', nav.outerHeight() + waypoint_offset).animate({
								'top': ''
							});
							$('body').removeClass('header-sticky');
						}
					};
				}
			});
		}

		/*  [ Vertical header ]
		- - - - - - - - - - - - - - - - - - - - */
		$('#showPushMenu').on('click', function() {
			if ( mainParams.vertical_menu == '1' ){
				$('body').toggleClass('vertical-close');
			}
			return false;
		});

		if ( isMobile.iOS() ) {
			$('.k2t-header').addClass('ios-divice');
			$('body').addClass('body-ios-divice');
		}
		/*  [ Custom RTL Menu ]
		- - - - - - - - - - - - - - - - - - - - */
		if ( ! isMobile.any() ) {
			$( '.sub-menu li' ).on( 'hover', function () {
				var sub_menu = $( this ).find( ' > .sub-menu' );
				if ( sub_menu.length ) {
					if ( sub_menu.outerWidth() > ( $( window ).outerWidth() - sub_menu.offset().left ) ) {
						$( this ).addClass( 'menu-rtl' );
					}
				}
			});
		}

		/*  [ Back to top ]
		- - - - - - - - - - - - - - - - - - - - */
		$( '.k2t-btt' ).on("click", function () {
			$("html, body").animate({
				scrollTop: 0
			}, 500);
			return false;
		});

		/*  [ Scroll to row start ]
		- - - - - - - - - - - - - - - - - - - - */
		$( '.chevron-down' ).on("click", function () {
			$('html, body').animate({
			    scrollTop: ($('#row-start').offset().top)
			},1000);
		});

		/*  [ Offcanvas Sidebar ]
		- - - - - - - - - - - - - - - - - - - - */
		$( '.open-sidebar' ).on( 'click', function() {
			//if ( mainParams.offcanvas_turnon == '1' ){
				$( 'body' ).toggleClass( 'offcanvas-open' );
				$( '.offcanvas-sidebar' ).toggleClass( 'is-open' );
				$(this).toggleClass( 'close-sidebar' );
			//}
			//return false;
		});
		$( '.offcanvas-sidebar .btn-close' ).on( 'click', function(e) {
			$( 'body' ).removeClass( 'offcanvas-open' );
			$( '.offcanvas-sidebar' ).removeClass( 'is-open' );
			$( '.open-sidebar' ).removeClass( 'close-sidebar' );
		});
		$( '.k2t-container' ).on( 'click', function(e) {
			if ($(e.target).hasClass( 'open-sidebar' ) || $(e.target).closest( '.open-sidebar' ).length > 0 ) {
				return;
			}
			$( 'body' ).removeClass( 'offcanvas-open' );
			$( '.offcanvas-sidebar' ).removeClass( 'is-open' );
			$( '.open-sidebar' ).removeClass( 'close-sidebar' );
		});

		$('.offcanvas-sidebar .k2t-sidebar ul li').on("click", function(){
			if ($(this).find('ul') && $(this).find('ul').hasClass('k2t-active')){
				$(this).find('ul').removeClass('k2t-active');
				$(this).removeClass('k2t-active');
			}else {
				$(this).find('ul').addClass('k2t-active');
				$(this).addClass('k2t-active');
			}
		});

		/*  [ Search Box ]
		- - - - - - - - - - - - - - - - - - - - */
		$('.search-box span').on( 'click', function(){  
			// $('.k2t-searchbox #s').focus();
			$('body').addClass('mode-search');
		});
		$('.k2t-searchbox-close span').on( 'click', function(){  
			$('body').removeClass('mode-search');
		});

		$('.k2t-searchbox .mark').on( 'click', function(){  
			$('body').removeClass('mode-search');
		});
		/*  [ VC Alert close ]
		- - - - - - - - - - - - - - - - - - - - */
		$( '.wpb_alert .close' ).on("click", function (){
			var parent = $(this).parent();
			parent.css({"opacity":"0", "height":"0", "padding":"0", "margin":"0"});
		});

		/*  [ Menu Responsive ]
		- - - - - - - - - - - - - - - - - - - - */
		jQuery('.mobile-menu-toggle').on("click", function(e) {
	        jQuery('body').toggleClass('enable-mobile-menu');
			jQuery('body').removeClass('scroll-mobile-menu');
	    });

		// Isotope
		if ( $().isotope && $().imagesLoaded ) {
			$('.b-masonry .masonry-layout').each( function() {
				var $this = $(this);
				$this.imagesLoaded( function() {
					var container = document.querySelector('.b-masonry .masonry-layout');
					var msnry = new Masonry( container, {
						itemSelector: '.post-item',
						columnWidth: container.querySelector('.grid-sizer'),
						gutter: 0
					});
					if ( $( '.style-blog-time' ).length > 0 ) {
						$this.find('article').each( function() {
							if ( $(this).css('left').replace(/[^-\d\.]/g, '') > 0 ) 
								$(this).addClass('item-right');
							else
								$(this).addClass('item-left');
						})
					}
				});
			});
				
			$( '.k2t-isotope-wrapper' ).each( function() {

				var $this = $(this);
				var $container = $this.find('.k2t-isotope-container');
				
				// initialize Isotope + Masonry after all images have loaded  
				$this.imagesLoaded( function() {

					$container.addClass('loaded').find('.isotope-selector').find('.article-inner');
					var isotope_args = {
						itemSelector: '.isotope-selector',
						transitionDuration	: '.55s',
						masonry: {
							gutter	: '.gutter-sizer',
							//columnWidth: 
						},
					};
					if ($this.hasClass('isotope-grid')) {
						isotope_args['layoutMode'] = 'fitRows';
					}
					if ($this.hasClass('isotope-no-padding')) {
						delete isotope_args.masonry.gutter; //true
					}
					if ($this.hasClass('isotope-free')) {
						isotope_args.masonry['columnWidth'] = '.width-1';
					}
					var $grid = $container.isotope(isotope_args);
					
					// animation
					var animation = $grid.data('animation');
					if (animation = true) {
						$container.find('.isotope-selector').find('.article-inner').each(function(){
							var $this=$(this);
							$this.parent().one('inview', function(event, isInView, visiblePartX, visiblePartY) {
								if (isInView) {
									$this.addClass('run_animation');
								} // inview						  
							});// bind
						}); // each
							
					} // endif animation
					
				}); // imagesLoaded
				
			}); // each .k2t-isotope-wrapper
		} // if isotope
		
		/*  [ Performs a smooth page scroll to an anchor ]
		- - - - - - - - - - - - - - - - - - - - */
		$('.scroll').on("click", function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash),
				headerH = $('.k2t-header').outerHeight();

				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - 170 + "px"
					}, 1200);
					return false;
			   }
		   }
		});

		var $logoImg = $('.k2t-logo img');
		if ( $logoImg.css( 'min-height' ) == '1px' ) {
			$logoImg.attr( 'src', $logoImg.attr( 'src' ).replace( 'logo.png', 'logo@2x.png' ) );
		}

		/*  [ Call owl carousel ]
		- - - - - - - - - - - - - - - - - - - - */
		
		if ( $().owlCarousel ) {
			if( typeof( related_num_slider ) === 'undefined'){
			    var related_num_slider = 2;
			};
			$('.k2t-testimonial .owl-carousel').owlCarousel({
				nav: false,
				dots: true,
				items: 1,
				navText: false
			});
			jQuery('.related-post-wrap').owlCarousel({
				items: related_num_slider,
				autoPlay: true,
				margin: 30,
				loop: false,
				nav: false,
				navText: [
					"<i class='zmdi zmdi-arrow-left'></i>",
					"<i class='zmdi zmdi-arrow-right'></i>"
				],
				dots: false,
				responsive: {
					320: {
						items: 1,
					},
					480: {
						items: 1,
					},
					768: {
						items: 2,
					},
					992: {
						items: related_num_slider,
					},
					1200: {
						items: related_num_slider,
					}
				},
			});
			var owl = jQuery(".owl-carousel");

			owl.each(function(){
				var items 			= $(this).attr('data-items'),
					autoPlay 		= $(this).attr('data-autoPlay'),
					margin 			= $(this).attr('data-margin'),
					loop 			= $(this).attr('data-loop'),
					nav 			= $(this).attr('data-nav'),
					dots 			= $(this).attr('data-dots'),
					mobile 			= $(this).attr('data-mobile'),
					tablet 			= $(this).attr('data-tablet'),
					desktop 		= $(this).attr('data-desktop'),
					URLhashListener = $(this).attr('data-URLhashListener');

				if ( $(this).hasClass( 'syn-owl' ) ) {

					var sync1 = $(".syn-main");
					var sync2 = $(".syn-nav-owl");
					var flag = false;
					var slides = sync1.owlCarousel({
						items:1,
						loop:false,
						margin:10,
						autoplay:false,
						autoplayTimeout:6000,
						autoplayHoverPause:false,
						nav: false,
						dots: false,
					});
					var items_nav = $('.syn-nav-owl').attr('data-items');
					var thumbs = sync2.owlCarousel({
						items: items_nav,
						loop:false,
						margin:10,
						autoplay:false,
						nav: false,
						dots: false
					}).on('click', '.item', function(e) {
				        e.preventDefault();	
				        sync1.trigger('to.owl.carousel', [$(e.target).parents('.owl-item').index(), 300, true]);
					}).on('change.owl.carousel', function(e) {
		                if (e.namespace && e.property.name === 'position' && !flag) {
				    }
					}).data('owl.carousel');
					
					$('.syn-nav-owl a').each(function(index){
						$(this).on('click', function(e){
							e.preventDefault(); 
						    $(this).addClass('clicked');
							$(this).parent().siblings().children('a').removeClass('clicked');
						});
					});

				} else {
					$(this).owlCarousel({
						items: items,
						autoPlay: autoPlay == "true" ? true : false,
						margin: parseInt( margin ),
						loop: false,
						nav: nav == "true" ? true : false,
						navText: [
							'<span aria-hidden="true" class="arrow_left"></span>',
							'<span aria-hidden="true" class="arrow_right"></span>'
						],
						dots: dots == "true" ? true : false,
						responsive: {
							320: {
								items: mobile
							},
							480: {
								items: mobile
							},
							768: {
								items: tablet
							},
							992: {
								items: desktop
							},
							1200: {
								items: items
							}
						},
						URLhashListener: URLhashListener == "true" ? true : false,
					});
				};
				
			});
		}

	});

	$(window).load(function() {

		/*  [ Check scroll ]
		- - - - - - - - - - - - - - - - - - - - */
		i();
		$(document).mousewheel(function(event, delta) {
            if ($(this).scrollTop() > 50) {
				$('.k2t-btt').fadeIn('slow');
				$('body').addClass('scroll-dow');
			} else {
				$('.k2t-btt').fadeOut('slow');
				$('body').removeClass('scroll-dow');
			}
			var scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop();
			if (scrollBottom < 50) {
				$('body').addClass('scroll-bottom');
			} else {
				$('body').removeClass('scroll-bottom');
			}
			i();
			set_current_menu_for_scroll();
        });

		/*  [ Page loader effect ]
		- - - - - - - - - - - - - - - - - - - - */
		$( '#loader' ).delay(600).fadeOut();
		$( '#loader-wrapper' ).delay(600).fadeOut( 'slow' );
		setTimeout(function(){
			$( '#loader-wrapper' ).remove();
		}, 800);

		
		/*  [ Menu One Page ]
		- - - - - - - - - - - - - - - - - - - - */
		var headerH = $(".k2t-header-mid").height();
		var adminbar = $("#wpadminbar").height();
		if (!adminbar) adminbar = 0;
		function i() {
			var e = "";
			var t = "";
			$(".k2t-header .k2t-menu > li").each(function(e) {
				var n = $(this).find("a").attr("href");
				var r = $(this).find("a").attr("data-target");
				if ($(r).length > 0 && $(r).position().top - headerH <= $(document).scrollTop()) {
					t = r
				}
			});
		}
		function set_current_menu_for_scroll(){
			var menu_arr = [];
			var i =  0;
			$(".k2t-header .k2t-menu > li").each(function(e) {
				var n = $(this).find("a").attr("href");
				if (n.charAt(0) == "#" && n.length > 2) {
					menu_arr[i] = n.substr(1, n.length - 1);
					i++;
				}
			});
			if (menu_arr.length > 0){
				jQuery.each( menu_arr, function(){
					var offset = $("#" + this).offset();
					var posY = offset.top - $(window).scrollTop();
					var posX = offset.left - $(window).scrollLeft(); 
					if(posY > 0){
						var new_active = "#" + this;
						if( jQuery(".k2t-header .k2t-menu > li.active > a").attr("href") == new_active  )
						{}else{
							jQuery(".k2t-header .k2t-menu > li.active").removeClass("active");
							jQuery("[href=#" + this + "]").parent("li").addClass("active");
						}
						return false;
					}
				});
			}
		}
		var n = 1e3;
		var r = "#" + $(".k2t-content").attr("id");
		$("body").on("click", ".k2t-header .k2t-menu > li > a", function() {
			var e = $(this).attr("href");
			var i = $(this).attr("data-target");

			$(".k2t-header .k2t-menu > li").each(function(){
				$(this).removeClass("active");
			});
			$(this).parent("li").addClass("active");
			if (e.charAt(0) == "#") {
				i = e
			}
			if ($(i).length > 0) {
				if (e == r) {
					$("html,body").animate({
						scrollTop: 0
					}, n, "easeInOutQuart")
				} else {
					$("html,body").animate({
						scrollTop: $(i).offset().top - headerH - adminbar
					}, n, "easeInOutQuart")
				}
				return false
			}
		});
	});

	var __k2t_check_updown = 0;
	function scrollFunc(e) {
		if ( typeof scrollFunc.x == 'undefined' ) {
			scrollFunc.x=window.pageXOffset;
			scrollFunc.y=window.pageYOffset;
		}
		var diffX=scrollFunc.x-window.pageXOffset;
		var diffY=scrollFunc.y-window.pageYOffset;

		if( diffX<0 ) {
			// Scroll right
		} else if( diffX>0 ) {
			// Scroll left
		} else if( diffY<0 ) {
			// Scroll down
			__k2t_check_updown = -1;
		} else if ( window.pageYOffset == 0 ) {
			__k2t_check_updown = 0;
		}else if( diffY>0 ) {
			// Scroll up
			__k2t_check_updown = 1;
		} else {
			__k2t_check_updown = 0;
			// First scroll event
		}
		scrollFunc.x=window.pageXOffset;
		scrollFunc.y=window.pageYOffset;
	}
	
	// set title bar lenght

	var $title = $( '.k2t-title-bar .main-title ');
	var title_lenght = $.trim($title.text()).length;
	if ( title_lenght < 11 )
		$title.addClass('ex-short-title');
	else if ( title_lenght < 15  ) $title.addClass('medium-title');
	else if ( title_lenght > 15  ) $title.addClass('length-title');

	// widget custom menu 

	if ( $( '.widget_nav_menu' ).length > 0 ) {
		$( '.widget_nav_menu li.menu-item-has-children' ).on('click',function( event ){
			$(this).addClass('active');
			$(this).children('ul').slideToggle();
			event.stopPropagation();
		});
		$( '.widget_nav_menu a').on('click',function( event ){
			event.stopPropagation();
		});
	}
	// set title bar lenght
	if ( $('.nav-item-shortcode').length > 0 ) {
		$('.nav-item-shortcode').each(function(){
			var data_href = $(this).attr('href');
			$( data_href ).each( function(){
				$(this).on('inview', function(event, isInView, visiblePartX, visiblePartY) {
					setTimeout(function(){            
						var inview_id = event.target.id ;
						var screen_bottom = window.pageYOffset + window.innerHeight;
						var el_offset_top = $('#' + inview_id).offset().top;
						var el_height = $('#' + inview_id).height();
						var height;
						var iv_height = screen_bottom - el_offset_top;
						if ( el_height > window.innerHeight ) height = window.innerHeight;
						else height = el_height;
						if ( iv_height / height > 0.5 ) {
							$('.pni-active').removeClass('pni-active');
							$( '#pni-' + inview_id ).addClass('pni-active');
						}
						if ( visiblePartY == 'bottom' ) {
							$('.pni-active').removeClass('pni-active');
							$('#pni-' + inview_id ).addClass('pni-active');
						};
					}, 0);
				}); // inview
			});
			$(this).on('click',function(){
				$("html, body").animate({
					scrollTop: $( $.attr(this, 'href') ).offset().top
				}, 500);
				return false;
			});
		});
	}
})(jQuery);

