<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package Tower
 * @author  Tower
 * @link	http://www.lunartheme.com
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php // You can start editing here -- including this comment! ?>

	<?php if ( have_comments() ) : ?>
		<h3 class="comments-title">
			<?php
				printf( _nx( 'One comment', '%1$s comments', get_comments_number(), 'comments title', 'Tower' ),
					number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>
		</h3>
		
		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style'      => 'ol',
					'short_ping' => true,
					'callback'   => 'k2t_tower_comments',
				) );
			?>
		</ol><!-- .comment-list -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below" class="navigation" role="navigation">
			<h1 class="assistive-text section-heading"><?php esc_html_e( 'Comment navigation', 'Tower' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( esc_html__( '&larr; Older Comments', 'Tower' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments &rarr;', 'Tower' ) ); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>

	<?php endif; // have_comments() ?>

	<?php 
		$comments_args = array(
	        'label_submit'	=> esc_html__( 'Submit Comment', 'k2t'),
	        'title_reply'	=> esc_html__( 'Post a comment', 'k2t'),
	        'comment_field' => '<p class="comment-form-comment"><textarea id="comment" name="comment" aria-required="true" class="required" placeholder="Comment" rows="8" cols="37" wrap="hard"></textarea></p>',
			'fields' => apply_filters( 'comment_form_default_fields', array(

		    'author' =>
		      '<p class="comment-form-author">' .
		      '<input id="author" placeholder="Name *" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
		      '" size="30" /></p>',

		    'email' =>
		      '<p class="comment-form-email">' .
		      '<input id="email" class="required" placeholder="E-mail *" name="email" class="required" aria-required="true" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
		      '" size="30" /></p>',
		      'url' =>
			    '<p class="comment-form-url">' .
			    '<input id="url" name="url" type="text" placeholder="Your Website" value="' . esc_attr( $commenter['comment_author_url'] ) .
			    '" size="30" /></p>',
		    )
		  ),
		);

		comment_form($comments_args);
	?>

</div><!-- #comments -->
