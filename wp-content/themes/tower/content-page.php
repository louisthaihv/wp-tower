<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Tower
 * @author  Tower
 * @link	http://www.lunartheme.com
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="page-entry">
		<div class="content-page">
		<?php
			the_content();
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Tower' ),
				'after'  => '</div>',
			) );
		?>
		</div><!-- .content-page -->
	</div><!-- .page-entry -->
	
</article><!-- #post-## -->
