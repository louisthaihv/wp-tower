<?php
/**
 * Register widget.
 *
 * @package Furion
 * @author  LunarTheme
 * @link http://www.lunartheme.com
 */

include_once K2T_FRAMEWORK_PATH . 'extensions/widgets/recent-post.php';
include_once K2T_FRAMEWORK_PATH . 'extensions/widgets/facebook.php';
include_once K2T_FRAMEWORK_PATH . 'extensions/widgets/google-plus.php';
include_once K2T_FRAMEWORK_PATH . 'extensions/widgets/social.php';
