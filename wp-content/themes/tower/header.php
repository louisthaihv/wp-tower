<?php
/**
 * The header for theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Furion
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get theme options
global $smof_data;

$classes = array();
$header_style    = ( function_exists( 'get_field' ) ) ? get_field( 'page_header_style', get_the_ID() ) : '';
if ( empty( $header_style ) )
	$header_style = $smof_data['header-style'];

$classes[] = $header_style;
// Fixed header
if ( $smof_data[ $header_style . 'fixed-header'] == '1' ) {
	$classes[] = 'fixed';
}
// Full width header
if ( ! empty( $smof_data[ $header_style . 'full-header'] ) ) {
	$classes[] = 'full-width';
}
// Full width header
if ( ! empty( $smof_data[ $header_style . 'absolute'] ) ) {
	$classes[] = 'header-absolute';
}

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php if ( ! empty( $smof_data['pageloader'] ) ) : ?>
	<div id="loader-wrapper">
		<svg class="circular" height="50" width="50">
		  <circle class="path" cx="25" cy="25.2" r="19.9" fill="none" stroke-width="6" stroke-miterlimit="10" />
		</svg>
	</div>
<?php endif; ?>
	
<div class="k2t-container">

	<?php if ( ! is_404() ) : ?>
	<header class="k2t-header <?php echo esc_attr( implode( ' ', $classes ) ); ?>">
		<?php
		
			// Include top header layout
			if ( ! empty( $smof_data[ $header_style . 'use-top-header'] ) ) {
				include_once K2T_TEMPLATE_PATH . 'header/top.php';
			}
			// Include middle header layout
			if ( ! empty( $smof_data[ $header_style . 'use-mid-header'] ) ) {
				include_once K2T_TEMPLATE_PATH . 'header/mid.php';
			}

			// Include bottom header layout
			if ( ! empty( $smof_data[ $header_style . 'use-bot-header'] ) ) {
				include_once K2T_TEMPLATE_PATH . 'header/bot.php';
			}

			include_once K2T_TEMPLATE_PATH . 'header/responsive.php';
		?>

	</header><!-- .k2t-header -->
	<?php endif;?>

	<div class="k2t-body">
