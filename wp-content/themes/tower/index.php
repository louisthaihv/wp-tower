<?php
/**
 * The main template file.
 *
 * @package Furion
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get theme options
global $smof_data;

$classes 				= array();
$pre 					= 'blog-';
$style = $smof_data['blog-style'];
$layout = $smof_data['blog-layout'];

if ( 'right_sidebar' == $layout ) {
	$classes[] = 'right-sidebar';
} elseif ( 'left_sidebar' == $layout ) {
	$classes[] = 'left-sidebar';
} else {
	$classes[] = 'no-sidebar';
}
if ($smof_data['pagination-type'] == 'pagination_lite') {
	$classes[] = 'pagination_lite';
}
if ( $style ) {
	$classes[] = 'b-' . $style;
}
// Blog masonry full width
$fullwidth = ( isset ( $smof_data['blog-masonry-full-width'] ) && $smof_data['blog-masonry-full-width'] ) ? ' fullwidth' : '';

get_header(); ?>

	<div class="k2t-content <?php echo esc_attr( implode( ' ', $classes ) ) . $fullwidth ?>">

		<div class="k2t-wrap test tsdsds">

			<main class="k2t-blog" role="main">
				
				<?php
					if ( 'masonry' == $style ) {
						echo '<div class="masonry-layout ' . $smof_data['blog-masonry-column'] . ' ">';
						echo '<div class="grid-sizer"></div>';
					}
					if ( 'grid' == $style ) {
						echo '<div class="grid-layout clearfix ' . $smof_data['blog-grid-column'] . ' ">';
					}
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							
							if ( $style == 'large'  ) {
								include K2T_TEMPLATE_PATH . 'blog/content-large.php';
							} elseif ( 'medium' == $style ) {
								include K2T_TEMPLATE_PATH . 'blog/content-medium.php';
							} elseif ( $style == 'grid'  || $style == 'masonry' ) {
								include K2T_TEMPLATE_PATH . 'blog/content-grid.php';
							} 
						endwhile;
					else :
						get_template_part( 'content', 'none' );
					endif;

					if ( 'masonry' == $style ) {
						echo '</div>';
					}
					if ( 'grid' == $style ) {
						echo '</div>';
					}

					include_once K2T_TEMPLATE_PATH . 'navigation.php';

				?>

			</main><!-- .k2t-main -->

			<?php
				if ( $smof_data['blog-layout'] != 'no_sidebar' ) {
					get_sidebar();
				}
			?>

		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-content -->

<?php get_footer(); ?>
