<?php
/**
 * Search form.
 *
 * @package Furion
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */
?>
<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="form-group">
		<input type="text" value="<?php echo get_search_query(); ?>" name="s" placeholder="<?php esc_html_e( 'Type & Hit Enter...', 'furion' ); ?>" />
		<button type="submit" ><?php esc_html_e( 'Search', 'furion' ); ?></button>
	</div>
</form>