<?php
/**
 * The template for displaying all single posts.
 *
 * @package Furion
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */


global $smof_data;

// Setup layout

$classes = array();
$layout = ( function_exists( 'get_field' ) ) ? get_field( 'post_layout', get_the_ID(), true ) : '';
if ( empty( $layout ) || $layout == 'default' )
	$layout = $smof_data['single-layout'];
switch ( $layout ) :
	case 'right_sidebar':
		$classes[] = 'right-sidebar';
		break;
	case 'left_sidebar':
		$classes[] = 'left-sidebar';
		break;
	case 'no_sidebar':
		$classes[] = 'no-sidebar';
		break;
endswitch;

get_header(); ?>

	<div  class="k2t-content <?php echo implode( ' ', $classes ) ?>">
		<div class="k2t-wrap">
			<main class="k2t-blog" role="main">
				<?php
				
				while ( have_posts() ) : the_post();
					get_template_part( 'content', 'single' );

					if ( $smof_data['single-commnet-form'] ) :
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
					endif;
	
				endwhile;
				?>
			</main><!-- .k2t-blog -->

			<?php
				if ( $layout != 'no_sidebar' ) {
					get_sidebar();
				}
			?>
		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-content -->

<?php get_footer(); ?>
