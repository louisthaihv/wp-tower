<?php
/**
 * The template for displaying menu responsive.
 *
 * @package Furion
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get theme options
global $smof_data;

$header_style    = ( function_exists( 'get_field' ) ) ? get_field( 'page_header_style', get_the_ID() ) : '';
if ( !isset($header_style) )
	$header_style = $smof_data['header-style'];

// Get logo type
$logo = isset ( $smof_data[ $header_style . 'logo'] ) ? trim( $smof_data[ $header_style . 'logo' ] ) : '';
?>
<div class="k2t-header-m">
	<div class="k2t-menu-m">
		<a class="m-trigger mobile-menu-toggle"><span></span></a>
		<div class="mobile-menu-wrap dark-div">
			<a href="#" class="mobile-menu-toggle"><i class="fa fa-times-circle"></i></a>
			<ul class="mobile-menu">
				<?php
					wp_nav_menu(array(
						'theme_location'  => 'mobile',
						'container' => false,
						'items_wrap' => '%3$s',
					)); 
				?>
			</ul>
		</div>
	</div>

	<div class="k2t-logo-m">
		<?php 
		if ( $logo == '' || ( isset( $smof_data[ $header_style . 'text-logo'] ) && $smof_data[ $header_style . 'use-text-logo'] ) ) : ?>
			<h1 class="logo-text">
				<a class="k2t-logo" rel="home" href="<?php echo esc_url( home_url( "/" ) ); ?>">
					<?php
						if ( ! $smof_data[ $header_style . 'text-logo'] ) {
							echo esc_html( bloginfo( 'name' ) );
						} else {
							echo esc_html( $smof_data[ $header_style . 'text-logo'] );
						}
					?>
				</a><!-- .k2t-logo -->
			</h1><!-- .logo-text -->
		<?php else : ?>
			<a class="k2t-logo" rel="home" href="<?php echo esc_url( home_url( "/" ) ); ?>">
				<img src="<?php echo esc_url( $logo );?>" alt="<?php esc_attr( bloginfo( 'name' ) );?>" />
			</a><!-- .k2t-logo -->
		<?php endif; ?>
	</div><!-- .k2t-logo-m -->

	<div class="k2t-right-m">
		<div class="search-box">
			<i class="fa fa-search"></i>
		</div><!-- .search-box -->
		<div class="canvas-sidebar">
			<a onclick="javascript:return false;" class="open-sidebar" href="#"><span class="inner"></span></a>
		</div>
	</div><!-- .k2t-right-m -->
</div><!-- .k2t-header-m -->