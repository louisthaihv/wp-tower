<?php
/**
 * The template for displaying title and breadcrumb of event.
 *
 * @package Furion
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get theme options
global $smof_data, $post;

// Get post or page id
if ( is_home() ) {
	$id = get_option( 'page_for_posts' );
} elseif ( function_exists( 'is_shop' ) && is_shop() ) {
	$id = get_option( 'woocommerce_shop_page_id' );
} else {
	$id = get_the_ID();
}
$classes = $css = $html = array();
// Check pre
$pre 		= 'page-';
$single_pre = 'page_';
if ( is_single() ) {
	if ( is_singular( 'post-portfolio' ) ) {
		$pre = 'portfolio-';
		$single_pre = 'portfolio_';
	} elseif ( is_singular( 'product' ) ) {
		$pre = 'product-';
		$single_pre = 'product_';
	} else {
		$pre = 'single-';
		$single_pre = 'single_';
	}
} elseif ( function_exists( 'is_shop' ) && is_shop() ) {
	$pre = 'shop-';
	$single_pre = 'shop_';
}elseif ( is_archive() || is_author() || is_category() || is_home() || is_tag() ) {
	$pre = 'blog-';
	$single_pre = 'blog_';
}

// Get metadata of event in single
$arr_titlebar_meta_val  = array();
$arr_titlebar_meta 		= array( 
	'layout'									=> '',
	'display_titlebar' 							=> 'show', 
	'titlebar_font_size' 						=> '', 
	'titlebar_color' 							=> '', 
	'pading_top' 								=> '',
	'pading_bottom'								=> '', 
	'background_color' 							=> '', 
	'background_image' 							=> '', 
	'background_position' 						=> '', 
	'background_size' 							=> '', 
	'background_repeat' 						=> '', 
	'background_parallax' 						=> '', 
	'titlebar_overlay_opacity' 					=> '', 
	'titlebar_clipmask_opacity' 				=> '',
	'titlebar_custom_content'  					=> ''
);


extract( shortcode_atts( $arr_titlebar_meta, $arr_titlebar_meta_val ) );

if ( is_singular( 'product' ) ) {
	if ( 'right_sidebar' == $layout ) {
		$classes[] = 'right-sidebar';
	} elseif ( 'left_sidebar' == $layout ) {
		$classes[] = 'left-sidebar';
	} elseif ( 'no_sidebar' == $layout ) {
		$classes[] = 'no-sidebar';
	}
}

// Title bar font size
if ( $titlebar_font_size ) {
	if ( is_numeric( $titlebar_font_size ) ) {
		$titlebar_font_size = ! empty( $titlebar_font_size ) ? 'font-size:' . $titlebar_font_size . 'px;' : '';
	} else {
		$titlebar_font_size = ! empty( $titlebar_font_size ) ? 'font-size:' . $titlebar_font_size . ';' : '';
	}
}

// Title bar color
if ( $titlebar_color ) {
	$titlebar_color = ! empty( $titlebar_color ) ? 'color:' . $titlebar_color . ';' : '';
}

// Padding for title bar
if ( $pading_top ) {
	if ( is_numeric( $pading_top ) ) {
		$css[] = ! empty( $pading_top ) ? 'padding-top:' . $pading_top . 'px;' : '';
	} else {
		$css[] = ! empty( $pading_top ) ? 'padding-top:' . $pading_top . ';' : '';
	}
}
if ( $pading_bottom ) {
	if ( is_numeric( $pading_bottom ) ) {
		$css[] = ! empty( $pading_bottom ) ? 'padding-bottom:' . $pading_bottom . 'px;' : '';
	} else {
		$css[] = ! empty( $pading_bottom ) ? 'padding-bottom:' . $pading_bottom . ';' : '';
	}
}

// Background color
if ( $background_color ) {
	$css[] = ! empty( $background_color ) ? 'background-color: ' . $background_color . ';' : '';
}

// Background image
if ( $background_image ) {
	if ( is_numeric( $background_image ) ) {
		$background_image = wp_get_attachment_image_src( $background_image, 'full' );
		$background_image = $background_image[0];
	}
	$css[] = ! empty( $background_image ) ? 'background-image: url(' . $background_image . ');' : '';
	$css[] = ! empty( $background_position ) ? 'background-position: ' . $background_position . ';' : '';
	$css[] = ! empty( $background_repeat ) ? 'background-repeat: ' . $background_repeat . ';' : '';
	if ( 'full' == $background_size ) {
		$css[] = ! empty( $background_size ) ? 'background-size: 100%;' : '';
	} else {
		$css[] = ! empty( $background_size ) ? 'background-size: ' . $background_size . ';' : '';
	}
}

// Background parallax
$inline_attr = '';

if ( 'show' == $display_titlebar ) :
?>

	<div class="k2t-title-bar <?php echo esc_attr( implode( ' ', $classes ) ); ?>" style="<?php echo esc_attr( implode( '', $css ) ); ?>" <?php echo esc_attr($inline_attr); ?>>
		<?php echo implode( ' ', $html ); ?>
		<div>
			<div class="container k2t-wrap">
			slider
			</div>
		</div>
	</div><!-- .k2t-title-bar -->

<?php endif;